<?php

namespace DoctrineORMModule\Proxy\__CG__\MLReview\Entity;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ORM. DO NOT EDIT THIS FILE.
 */
class Place extends \MLReview\Entity\Place implements \Doctrine\ORM\Proxy\Proxy
{
    private $_entityPersister;
    private $_identifier;
    public $__isInitialized__ = false;
    public function __construct($entityPersister, $identifier)
    {
        $this->_entityPersister = $entityPersister;
        $this->_identifier = $identifier;
    }
    /** @private */
    public function __load()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;

            if (method_exists($this, "__wakeup")) {
                // call this after __isInitialized__to avoid infinite recursion
                // but before loading to emulate what ClassMetadata::newInstance()
                // provides.
                $this->__wakeup();
            }

            if ($this->_entityPersister->load($this->_identifier, $this) === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            unset($this->_entityPersister, $this->_identifier);
        }
    }

    /** @private */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    
    public function setPlaceName($placeName)
    {
        $this->__load();
        return parent::setPlaceName($placeName);
    }

    public function getPlaceName()
    {
        $this->__load();
        return parent::getPlaceName();
    }

    public function setSrc($src)
    {
        $this->__load();
        return parent::setSrc($src);
    }

    public function getSrc()
    {
        $this->__load();
        return parent::getSrc();
    }

    public function setGpsShape(\MLReview\Entity\PolygonVO $gpsShape)
    {
        $this->__load();
        return parent::setGpsShape($gpsShape);
    }

    public function getGpsShape()
    {
        $this->__load();
        return parent::getGpsShape();
    }

    public function setDescription($description)
    {
        $this->__load();
        return parent::setDescription($description);
    }

    public function getDescription()
    {
        $this->__load();
        return parent::getDescription();
    }

    public function setAddress($address)
    {
        $this->__load();
        return parent::setAddress($address);
    }

    public function getAddress()
    {
        $this->__load();
        return parent::getAddress();
    }

    public function getPlaceId()
    {
        if ($this->__isInitialized__ === false) {
            return (int) $this->_identifier["placeId"];
        }
        $this->__load();
        return parent::getPlaceId();
    }

    public function addActivities(\Doctrine\Common\Collections\Collection $activities)
    {
        $this->__load();
        return parent::addActivities($activities);
    }

    public function removeActivities(\Doctrine\Common\Collections\Collection $activities)
    {
        $this->__load();
        return parent::removeActivities($activities);
    }

    public function getActivities()
    {
        $this->__load();
        return parent::getActivities();
    }


    public function __sleep()
    {
        return array('__isInitialized__', 'placeName', 'src', 'gpsShape', 'description', 'address', 'placeId', 'activities');
    }

    public function __clone()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;
            $class = $this->_entityPersister->getClassMetadata();
            $original = $this->_entityPersister->load($this->_identifier);
            if ($original === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            foreach ($class->reflFields as $field => $reflProperty) {
                $reflProperty->setValue($this, $reflProperty->getValue($original));
            }
            unset($this->_entityPersister, $this->_identifier);
        }
        
    }
}