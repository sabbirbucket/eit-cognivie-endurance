<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */
/**
 * Local Configuration Override
 *
 * This configuration override file is for overriding environment-specific and
 * security-sensitive configuration information. Copy this file without the
 * .dist extension at the end and populate values as needed.
 *
 * @NOTE: This file is ignored from Git by default with the .gitignore included
 * in ZendSkeletonApplication. This is a good practice, as it prevents sensitive
 * credentials from accidentally being committed into version control.
 */
return array(
    // Whether or not to enable a configuration cache.
    // If enabled, the merged configuration will be cached and used in
    // subsequent requests.
    //'config_cache_enabled' => false,
    // The key used to create the configuration cache file name.
    //'config_cache_key' => 'module_config_cache',
    // The path in which to cache merged configuration.
    //'cache_dir' =>  './data/cache',
    // ...
    /* Database authentication */
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host' => 'localhost',
                    'port' => '3306',
                    'user' => 'root',
                    'password' => '',
                    'dbname' => 'MLReview',
                    'charset' => 'utf8' //IMPORTANT
                )
            )
        )
    ),
    /*
     * Sorting methods to call, and in what order to call them
     * Putting them here in local because their addresses can wary between dev, prod etc.
     */
    'sortingMethods' => array(
        'MLCluster' => array(
            'adapterName' => 'MLCluster', //This is the adapter service name!
            'route' => 'mlcluster', //Route defined by this Module
            'params' => array(), //For eaxmple what action to call
            'queryOptions' => array(//query options for this Module
                'known_places_source' => '/data/logsync/GPS_places.xml',
                'known_bt_source' => '/data/logsync/BT_persons.xml',
                'gps_source' => '/data/logsync/Logs/GPSlogTemp.xml',
                'bt_source' => '/data/logsync/Logs/BTlogTemp.xml',
                'mediaimage_source' => '/data/logsync/Logs/imagesTemp.xml'
            ),
            'postTo' => 'http://mlreview.dev',
        ),
        'MLCluster2' => array(
            'adapterName' => 'MLCluster',
            'route' => 'mlcluster',
            'params' => array(),
            'queryOptions' => array(
                'known_places_source' => '/data/logsync/GPS_places.xml',
                'known_bt_source' => '/data/logsync/BT_persons.xml',
                'gps_source' => '/data/logsync/Logs/GPSlogTemp2.xml',
                'bt_source' => '/data/logsync/Logs/BTlogTemp2.xml',
                'mediaimage_source' => '/data/logsync/Logs/imagesTemp2.xml'
            ),
            'filters' => array(
                'NumberOfMediaFilter' => array(//The filter service name!
                    'NrOfMedia' => 3, //Must match the setter of the filter
                ),
            ),
            'postTo' => 'http://mlreview.dev',
        ),
        'MLCluster3' => array(
            'adapterName' => 'MLCluster',
            'route' => 'mlcluster',
            'params' => array(),
            'queryOptions' => array(
                'known_places_source' => '/data/logsync/GPS_places.xml',
                'known_bt_source' => '/data/logsync/BT_persons.xml',
                'gps_source' => '/data/logsync/Logs/GPSlogTemp3.xml',
                'bt_source' => '/data/logsync/Logs/BTlogTemp3.xml',
                'mediaimage_source' => '/data/logsync/Logs/imagesTemp3.xml'
            ),
            'filters' => array(
                'NumberOfMediaFilter' => array(//The filter service name!
                    'nrOfMedia' => 3, //Must match the setter of the filter
                ),
                'MinTimeActivityFilter' => array(//The filter service name!
                    'minutes' => 40, //Must match the setter of the filter
                ),
            ),
            'postTo' => 'http://mlreview.dev',
        ),
    ),
);
