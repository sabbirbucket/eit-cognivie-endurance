<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    /*
     * Default global variables used by the MLReview client.
     * IMPORTANT: If you change these you might need to change some column data
     * in the database!
     */
    'default' => array(
        'person_image' => '/images/unknown_person.jpg',
        'place_image' => '/images/unknown_place.jpg',
        'activity_image' => '/images/unknown_activity.jpg',
        'set_place_image' => '/images/house-question.jpg',
        'add_persons_image' => '/images/person-question.jpg',
        'base_upload_path' => '/data/uploaded/',
        'public_path' => __DIR__ . '/../../public',
        'logsync_media_path' => '/data/logsync/',
    ),
    /* Navigation configuration for review client.
     * Label is the name of the page.
     * Route is taken from module.config.php
     */
    'navigation' => array(
        'default' => array(
            array(
                'label' => \MLReview\Util\Translator::translate('Calendar'),
                'route' => 'calendar',
            ),
            array(
                'label' => MLReview\Util\Translator::translate('Overview'),
                'route' => 'home',
            ),
            array(
                'label' => MLReview\Util\Translator::translate('Activity'),
                'route' => 'activity',
            ),
            array(
                'label' => MLReview\Util\Translator::translate('Life Story'),
                'route' => 'lifestory',
            ), array(
                'label' => MLReview\Util\Translator::translate('Places'),
                'route' => 'place',
            ),
            array(
                'label' => MLReview\Util\Translator::translate('Persons'),
                'route' => 'person',
            ),
            array(
                'label' => MLReview\Util\Translator::translate('Upload'),
                'route' => 'uploadxmldata',
            )

        )
    ),
);
