<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * FieldSet for file uploading. Later used in Forms.
 */
class FileUploadFieldset extends Fieldset implements InputFilterProviderInterface {

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->addElements();
    }

    /**
     * Just adds some elements to this Fieldset
     */
    public function addElements() {

        $this->add(array(
            'type' => 'Zend\Form\Element\File',
            'name' => 'file',
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('File to upload')
            ),
            'attributes' => array(
                'onchange' => 'special_submit(this.form);',
                'class' => 'file',
                'id' => 'selectedFile'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Button',
            'name' => 'upload-button',
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Upload image')
            ),
            'attributes' => array(
                'class' => 'button-primary',
                'onclick' => 'document.getElementById("selectedFile").click();'
            ),
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function getInputFilterSpecification() {

        return array(
            'file' => array(
                'required' => false,
            )
        );
    }

}

?>
