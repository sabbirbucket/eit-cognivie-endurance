<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form;

use MLReview\Entity\Place;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * MVC framework. FieldSet for the place. Later used in Forms.
 *
 */
class PlaceFieldset extends Fieldset implements InputFilterProviderInterface {

    /**
     * Constructor
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager) {
        parent::__construct('place');
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        $this->setHydrator(new DoctrineHydrator($entityManager, 'MLReview\Entity\Place'))
                ->setObject(new Place());

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'placeId',
            'attributes' => array(
                'id' => 'placeId',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'src'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'gpsShape'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'placeName',
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Name*')
            ),
            'attributes' => array(
                'class' => 'keyboardInput',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\TextArea',
            'name' => 'description',
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Description')
            ),
            'attributes' => array(
                'class' => 'keyboardInput',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\TextArea',
            'name' => 'address',
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Address')
            ),
            'attributes' => array(
                'class' => 'keyboardInput',
            ),
        ));

        /*
          $activityFieldset = new ActivityFieldset($serviceManager);
          $this->add(array(
          'type' => 'Zend\Form\Element\Collection',
          'name' => 'activities',
          'options' => array(
          'count' => 2,
          'target_element' => $activityFieldset
          )
          ));
         */
    }

    /**
     * {@inheritDoc}
     */
    public function getInputFilterSpecification() {
        return array(
            'placeId' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'Int'),
                )
            ),
            'src' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            'gpsShape' => array(
                'required' => false
            ),
            'placeName' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            'description' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 255,
                        ),
                    ),
                ),
            ),
            'address' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 255,
                        ),
                    ),
                ),
            ),
        );
    }

}

?>
