<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\ServiceManager\ServiceManager;
use Zend\Form\Form;

/**
 * Form for adding and editing places
 *
 */
class PlaceForm extends Form {

    /**
     * Constructor
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager) {
        parent::__construct('place-form');
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        // The form will hydrate an object of type "Place"
        $this->setHydrator(new DoctrineHydrator($entityManager, 'MLReview\Entity\Place'));

        // Add the place fieldset, and set it as the base fieldset
        $placeFieldset = new PlaceFieldset($serviceManager);

        $placeFieldset->setUseAsBaseFieldset(true);
        $placeFieldset->setLabel('Add a new place');
        $this->add($placeFieldset);

        //Able to upload image
        $uploadFieldSet = new FileUploadFieldset('upload');
        $this->add($uploadFieldSet);

        //Security to avoid cross-site request forgery
        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));

        $this->add(array(
            'name' => 'submit-yes',
            'attributes' => array(
                'type' => 'submit',
                'value' => \MLReview\Util\Translator::translate('Yes'),
                'class' => 'button-primary',
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Save this place?')
            )
        ));

        $this->add(array(
            'name' => 'submit-no',
            'attributes' => array(
                'type' => 'submit',
                'value' => \MLReview\Util\Translator::translate('No'),
                'class' => 'button-primary',
                'onclick' => 'window.close();'
            ),
        ));

        //Special flag, used for different things in different controllers
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'specialFlag'
        ));

        //The image to view
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'tempImage'
        ));
    }

}

?>
