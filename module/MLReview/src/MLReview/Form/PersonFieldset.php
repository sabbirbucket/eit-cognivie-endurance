<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form;

use MLReview\Entity\Person;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * MVC framework. FieldSet for the person. Later used in Forms.
 *
 */
class PersonFieldset extends Fieldset implements InputFilterProviderInterface {

    /**
     * Constructor
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager) {
        parent::__construct('person');
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        $this->setHydrator(new DoctrineHydrator($entityManager, 'MLReview\Entity\Person'))
                ->setObject(new Person());

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'personId'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'src'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'firstName',
            'attributes' => array(
                'class' => 'keyboardInput',
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('First name*')
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'lastName',
            'attributes' => array(
                'class' => 'keyboardInput',
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Last name')
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\TextArea',
            'name' => 'personalInfo',
            'attributes' => array(
                'class' => 'keyboardInput',
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Personal Information')
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'phone',
            'attributes' => array(
                'class' => 'keyboardInput',
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Phone number')
            )
        ));

        /*
          $personDataFieldset = new PersonDataFieldset($serviceManager);
          $this->add(array(
          'type' => 'Zend\Form\Element\Collection',
          'name' => 'personDatas',
          'options' => array(
          'count' => 2,
          'target_element' => $personDataFieldset
          )
          ));
         */

        /*
          $activityFieldset = new ActivityFieldset($serviceManager);
          $this->add(array(
          'type' => 'Zend\Form\Element\Collection',
          'name' => 'activities',
          'options' => array(
          'target_element' => $activityFieldset
          )
          ));
         */
    }

    /**
     * {@inheritDoc}
     */
    public function getInputFilterSpecification() {
        return array(
            'personId' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'Int'),
                )
            ),
            'src' => array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            'firstName' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            'lastName' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ),
            'personalInfo' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 255,
                        ),
                    ),
                ),
            ),
            'phone' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 40,
                        ),
                    ),
                ),
            )
        );
    }

}

?>
