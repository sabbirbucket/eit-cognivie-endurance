<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form;

use Zend\ServiceManager\ServiceManager;
use Zend\Form\Form;

/**
 * Form for uploading media. One at a time.
 *
 */
class MediaUploadForm extends Form {

    /**
     * Constructor
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager) {
        parent::__construct('mediaform');

        $this->addElements();
    }

    /**
     * Adds some elements.
     */
    public function addElements() {

        //Able to upload image
        $uploadFieldSet = new FileUploadFieldset('upload');
        $file = $uploadFieldSet->get('file');
        //We are setting this in the view instead
        $file->setAttribute('onchange', 'post_media(this.form);');
        $this->add($uploadFieldSet);
    }

}

?>
