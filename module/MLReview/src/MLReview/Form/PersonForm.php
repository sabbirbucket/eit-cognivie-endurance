<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\ServiceManager\ServiceManager;
use Zend\Form\Form;

/**
 * Form for adding and editing persons
 *
 */
class PersonForm extends Form {

    /**
     * Constructor
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager) {
        parent::__construct('person-form');
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        // The form will hydrate an object of type "Person"
        $this->setHydrator(new DoctrineHydrator($entityManager, 'MLReview\Entity\Person'));

        // Add the person fieldset, and set it as the base fieldset
        $personFieldset = new PersonFieldset($serviceManager);

        //We dont want to validate or change the activities or personDatas
        //$personFieldset->remove('activities');
        //$personFieldset->remove('personDatas');
        //$personFieldset->get('media')->remove('personDatas');

        $personFieldset->setUseAsBaseFieldset(true);
        $personFieldset->setLabel('Add a new person');
        $this->add($personFieldset);

        //Able to upload image
        $uploadFieldSet = new FileUploadFieldset('upload');
        $this->add($uploadFieldSet);

        //Security to avoid cross-site request forgery
        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));

        $this->add(array(
            'name' => 'submit-yes',
            'attributes' => array(
                'type' => 'submit',
                'value' => \MLReview\Util\Translator::translate('Yes'),
                'class' => 'button-primary',
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Save this person?')
            )
        ));

        $this->add(array(
            'name' => 'submit-no',
            'attributes' => array(
                'type' => 'submit',
                'value' => \MLReview\Util\Translator::translate('No'),
                'class' => 'button-primary',
                'onclick' => 'window.close();'
            ),
        ));

        //Special flag, used for different things in different controllers
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'specialFlag'
        ));

        //The image to view
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'tempImage'
        ));
    }

}

?>
