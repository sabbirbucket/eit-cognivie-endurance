<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form;

use MLReview\Entity\Activity;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * MVC framework. FieldSet for the activity. Later used in Forms.
 */
class ActivityFieldset extends Fieldset implements InputFilterProviderInterface {

    /**
     * Constructor
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     * @return type
     */
    public function __construct(ServiceManager $serviceManager) {
        parent::__construct('activity');
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        //$this->setHydrator(new DoctrineHydrator($entityManager, 'MLReview\Entity\Activity'))
        $hydrator = new DoctrineHydrator($entityManager, 'MLReview\Entity\Activity');
        $hydrator->addStrategy('startTime', new \MLReview\Form\Hydrator\Strategy\DateTimeStrategy());
        $hydrator->addStrategy('endTime', new \MLReview\Form\Hydrator\Strategy\DateTimeStrategy());
        $this->setHydrator($hydrator)->setObject(new Activity());

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'activityId'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'src',
            'attributes' => array(
                'id' => 'activity_src_hidden',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'reviewed',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'subject',
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Subject*')
            ),
            'attributes' => array(
                'class' => 'keyboardInput',
                'id' => 'subject',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'startTime',
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Start date')
            ),
            'attributes' => array(
                'id' => 'datetimepicker_start',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'endTime',
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('End date')
            ),
            'attributes' => array(
                'id' => 'datetimepicker_end',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\TextArea',
            'name' => 'description',
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Description')
            ),
            'attributes' => array(
                'class' => 'keyboardInput',
            ),
        ));

        $mediaFieldset = new MediaFieldset($serviceManager);
        $this->add(array(
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'medias',
            'options' => array(
                'id' => 'medias',
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'template_placeholder' => '__placeholder__',
                'target_element' => $mediaFieldset
            )
        ));

        $placeFieldset = new PlaceFieldset($serviceManager);
        $place_options = array(
            'label' => \MLReview\Util\Translator::translate('Set place'));
        $placeFieldset->setOptions($place_options);
        $this->add($placeFieldset);

        $this->add(array(
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'name' => 'storyCategory',
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Life Story'),
                'object_manager' => $serviceManager->get('Doctrine\ORM\EntityManager'),
                'target_class' => 'MLReview\Entity\StoryCategory',
                'property' => 'name',
                'empty_option' => \MLReview\Util\Translator::translate('- No Life Story -'),
                'label_generator' => function($targetEntity) {
                    return \MLReview\Util\Translator::translate($targetEntity->getName());
                },
            ),
        ));

        /*
          $storyCategoryFieldset = new StoryCategoryFieldset($serviceManager);
          $story_options = array(
          'label' => \MLReview\Util\Translator::translate('Life story'));
          $storyCategoryFieldset->setOptions($story_options);
          $this->add($storyCategoryFieldset);
         */

        $personFieldset = new PersonFieldset($serviceManager);
        $this->add(array(
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'persons',
            'options' => array(
                'count' => 0,
                'should_create_template' => true,
                'allow_add' => true,
                'allow_remove' => true,
                'target_element' => $personFieldset,
            )
        ));
    }

    /**
     * Sets all the images representing a media to the same as the src value
     */
    public function setMediaImageToSrc() {
        $medias = $this->get('medias');
        foreach ($medias as $media):
            $media->setMediaImageToSrc();
        endforeach;
    }

    /**
     * {@inheritDoc}
     */
    public function getInputFilterSpecification() {
        return array(
            'activityId' => array(
                'required' => false
            ),
            'src' => array(
                'required' => false
            ),
            'subject' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 150,
                        ),
                    ),
                ),
            ),
            'description' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 255,
                        ),
                    ),
                ),
            ),
            'storyCategory' => array(
                'required' => false,
                'allow_empty' => true,
            ),
            'startTime' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Date',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'format' => 'Y-m-d H:i',
                            'messages' => array(
                                'dateFalseFormat' =>
                                \MLReview\Util\Translator::translate('Invalid date, must be yyyy-mm-dd HH:MM'),
                                'dateInvalidDate' =>
                                \MLReview\Util\Translator::translate('Invalid date, must be yyyy-mm-dd HH:MM')
                            ),
                        ),
                    ),
                ),
            ),
            'endTime' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Date',
                        'break_chain_on_failure' => true,
                        'options' => array(
                            'format' => 'Y-m-d H:i',
                            'messages' => array(
                                'dateFalseFormat' =>
                                \MLReview\Util\Translator::translate('Invalid date, must be yyyy-mm-dd HH:MM'),
                                'dateInvalidDate' =>
                                \MLReview\Util\Translator::translate('Invalid date, must be yyyy-mm-dd HH:MM')
                            ),
                        ),
                    ),
                ),
            ),
            'place' => array(
                'required' => false,
                'allow_empty' => true,
            ),
            'persons' => array(
                'required' => false,
                'allow_empty' => true,
            ),
            'medias' => array(
                'required' => false,
                'allow_empty' => true,
            ),
        );
    }

}

?>
