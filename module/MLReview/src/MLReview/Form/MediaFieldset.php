<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form;

use MLReview\Entity\Media;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * MVC framework. FieldSet for the media. Later used in Forms.
 *
 */
class MediaFieldset extends Fieldset implements InputFilterProviderInterface {

    /**
     * Constructor
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager) {
        parent::__construct('media');
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        /*
          $this->setHydrator(new DoctrineHydrator($entityManager, 'MLReview\Entity\Media'))
          ->setObject(new Media());
         */

        $hydrator = new DoctrineHydrator($entityManager, 'MLReview\Entity\Media');
        $hydrator->addStrategy('mediaTime', new \MLReview\Form\Hydrator\Strategy\DateTimeStrategy());
        $hydrator->addStrategy('gpsPos', new \MLReview\Form\Hydrator\Strategy\GPSStrategy());
        $this->setHydrator($hydrator)->setObject(new Media());

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'mediaId'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'src',
            'attributes' => array(
                'value' => '__src_to_be_changed__'
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'mediaTime'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'gpsPos'
        ));

        //Image elements
        $mediaImage = new \Zend\Form\Element\Image('image');
        //$image_options = array('label' => \MLReview\Util\Translator::translate('Media image'));
        //$mediaImage->setOptions($image_options);
        $image_attributes = array(
            'class' => 'preview medium selectedMedia',
            'src' => '__src_to_be_changed__',
            'onclick' => '__script_to_add__;return false;'
        );
        $mediaImage->setAttributes($image_attributes);
        $this->add($mediaImage);

        /*
          $personDataFieldset = new PersonDataFieldset($serviceManager);
          $this->add(array(
          'type' => 'Zend\Form\Element\Collection',
          'name' => 'personDatas',
          'options' => array(
          'count' => 2,
          'target_element' => $personDataFieldset
          )
          ));
         */

        /*
          $activityFieldset = new ActivityFieldset($serviceManager);
          $this->add(array(
          'type' => 'Application\Form\ActivityFieldset',
          'name' => 'activity',
          'options' => array(
          'label' => 'Activity place',
          'target_element' => $activityFieldset
          )
          ));
         */
    }

    /**
     * Sets the image representing this media to the same as the src value
     */
    public function setMediaImageToSrc() {
        $src = $this->get('src')->getValue();
        $this->get('image')->setAttribute('src', $src);
    }

    /**
     * {@inheritDoc}
     */
    public function getInputFilterSpecification() {
        return array(
            'mediaId' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'Int'),
                )
            ),
            'src' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'ISO-8859-1',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ),
        );
    }

}

?>
