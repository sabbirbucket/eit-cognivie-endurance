<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form;

use MLReview\Entity\PersonData;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * MVC framework. FieldSet for the personData. Later used in Forms.
 */
class PersonDataFieldset extends Fieldset implements InputFilterProviderInterface {

    /**
     * Constructor
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager) {
        parent::__construct('PersonData');
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        $this->setHydrator(new DoctrineHydrator($entityManager, 'MLReview\Entity\PersonData'))
                ->setObject(new PersonData());

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'personDataId'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'btName'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'btUid'
        ));

        $mediaFieldset = new MediaFieldset($serviceManager);
        $this->add(array(
            'type' => 'Zend\Form\Element\Collection',
            'name' => 'medias',
            'options' => array(
                'count' => 2,
                'target_element' => $mediaFieldset
            )
        ));

        $personFieldset = new PersonFieldset($serviceManager);
        $this->add(array(
            'type' => 'Application\Form\PersonFieldset',
            'name' => 'place',
            'options' => array(
                'label' => 'Activity place',
                'target_element' => $personFieldset
            )
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function getInputFilterSpecification() {
        return array(
            'personDataId' => array(
                'required' => false
            )
        );
    }

}

?>
