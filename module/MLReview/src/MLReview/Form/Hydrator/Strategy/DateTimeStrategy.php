<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form\Hydrator\Strategy;

use Zend\Stdlib\Hydrator\Strategy\StrategyInterface;
use MLReview\Util\Translator;

/**
 * Handles the conversion between the database entities (doctrine) and the Zend form elements.
 */
class DateTimeStrategy implements StrategyInterface {

    /**
     * Converts from Database entities to Form elements
     * @param \DateTime $value
     * @return string
     */
    public function extract($value) {
        $str = Translator::dateToString($value);
        if (!empty($str)) {
            return $str;
        }
        return $value;
    }

    /**
     * Converts from Form elements to Database entities
     * @param string $value
     * @return null|\DateTime
     */
    public function hydrate($value) {
        if (is_string($value) && "" === $value) {
            return null;
        }
        $date = Translator::stringToDate($value);
        if (!empty($date)) {
            return $date;
        }
        return $value;
    }

}

?>
