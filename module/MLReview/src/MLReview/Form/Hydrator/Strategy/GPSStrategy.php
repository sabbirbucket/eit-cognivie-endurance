<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form\Hydrator\Strategy;

use MLReview\Entity\PointVO;
use Zend\Stdlib\Hydrator\Strategy\StrategyInterface;

/**
 * Handles the conversion between the database entities (doctrine) and the Zend form elements.
 * This class handles the GPS point residing in the Media entity.
 */
class GPSStrategy implements StrategyInterface {

    /**
     * Converts from Database entities to Form elements
     * @param \MLReview\Entity\PointVO $value
     * @return string|\MLReview\Entity\PointVO
     */
    public function extract($value) {
        if (empty($value)) {
            return "";
        }
        if ($value instanceof PointVO) {
            if ($value->getX() == 0 && $value->getY() == 0) {
                return "";
            }
            return '(' . $value->toString() . ')';
        }
        return $value;
    }

    /**
     * Converts from Form elements to Database entities
     * @param string $value
     * @return null|\MLReview\Entity\PointVO
     */
    public function hydrate($value) {
        if (empty($value)) {
            return null;
        }
        if (is_string($value)) {
            list($x, $y) = sscanf($value, '(%f %f)');
            return new PointVO($x, $y);
        }
        return $value;
    }

}

?>
