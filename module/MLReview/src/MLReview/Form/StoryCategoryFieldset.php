<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form;

use MLReview\Entity\StoryCategory;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * FieldSet for the story category. Later used in Forms.
 *
 */
class StoryCategoryFieldset extends Fieldset implements InputFilterProviderInterface {

    /**
     * Constructor
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager) {
        parent::__construct('storyCategory');
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        $this->setHydrator(new DoctrineHydrator($entityManager, 'MLReview\Entity\StoryCategory'))
                ->setObject(new StoryCategory());

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'storyCategoryId'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'description'
        ));

        $this->add(array(
            'name' => 'name',
            'options' => array(
                'label' => 'Name of the category'
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function getInputFilterSpecification() {
        return array(
            'storyCategoryId' => array(
                'required' => false
            ),
            'name' => array(
                'required' => true
            ),
            'description' => array(
                'required' => false
            ),
        );
    }

}

?>
