<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Form;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\ServiceManager\ServiceManager;
use Zend\Form\Form;

/**
 * Form for adding and editing activities
 */
class ActivityForm extends Form {

    /**
     * Constructor
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager) {
        parent::__construct('activity-form');
        $entityManager = $serviceManager->get('Doctrine\ORM\EntityManager');

        // The form will hydrate an object of type "Activity"
        $hydrator = new DoctrineHydrator($entityManager, 'MLReview\Entity\Activity');
        $this->setHydrator($hydrator);

        // Add the activity fieldset, and set it as the base fieldset
        $activityFieldset = new ActivityFieldset($serviceManager);

        //Change some sizes
        $activityFieldset->get('subject')->setAttribute('size', '100');

        $activityFieldset->setUseAsBaseFieldset(true);
        $activityFieldset->setLabel('Add a new activity');
        $this->add($activityFieldset);

        //Able to upload image
        $uploadFieldSet = new FileUploadFieldset('upload');
        $this->add($uploadFieldSet);

        //Security to avoid cross-site request forgery
        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Button',
            'name' => 'submit-yes',
            'attributes' => array(
                'class' => 'button-primary',
                'onclick' => 'this.form.submit();'
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Save')
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Button',
            'name' => 'submit-no',
            'attributes' => array(
                'class' => 'button-primary',
                'onclick' => 'window.location.href=window.location.href;'
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Clear')
            )
        ));

        //Special flag, used for different things in different controllers
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'specialFlag'
        ));

        //Image elements
        $config = $serviceManager->get('Config');
        $activity_image = $config['default']['activity_image'];
        $set_place_image = $config['default']['set_place_image'];
        $add_persons_image = $config['default']['add_persons_image'];

        $this->add(array(
            'type' => 'Image',
            'name' => 'representative',
            'attributes' => array(
                'src' => $activity_image,
                'id' => 'representative_image',
                'class' => 'preview medium'
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Representative')
            )
        ));

        $this->add(array(
            'type' => 'Image',
            'name' => 'set_place',
            'attributes' => array(
                'src' => $set_place_image,
                'id' => 'set_place_image',
                'class' => 'preview small',
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Set Place'),
            )
        ));

        $this->add(array(
            'type' => 'Image',
            'name' => 'add_persons',
            'attributes' => array(
                'src' => $add_persons_image,
                'id' => 'add_persons_image',
                'class' => 'preview small',
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Add persons')
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Button',
            'name' => 'select_all_button',
            'attributes' => array(
                'class' => 'button-primary',
                'onclick' => 'select_all();'
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Select All')
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Button',
            'name' => 'deselect_all_button',
            'attributes' => array(
                'class' => 'button-primary',
                'onclick' => 'deselect_all();'
            ),
            'options' => array(
                'label' => \MLReview\Util\Translator::translate('Deselect All')
            )
        ));
    }

}

?>
