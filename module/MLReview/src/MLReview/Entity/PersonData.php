<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * PersonData
 *
 * @ORM\Table(name="PersonData")
 * @ORM\Entity
 */
class PersonData {

    /**
     * @var string
     *
     * @ORM\Column(name="BTName", type="string", length=50, nullable=true)
     */
    private $btName;

    /**
     * @var string
     *
     * @ORM\Column(name="BTuid", type="string", length=50, nullable=true)
     */
    private $btUid;

    /**
     * @var integer
     *
     * @ORM\Column(name="personDataId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personDataId;

    /**
     * @var \Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="personDatas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="personId", referencedColumnName="personId")
     * })
     */
    private $person;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Media", inversedBy="personDatas")
     * @ORM\JoinTable(name="PersonDataMedia",
     *   joinColumns={
     *     @ORM\JoinColumn(name="personDataId", referencedColumnName="personDataId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="mediaId", referencedColumnName="mediaId")
     *   }
     * )
     */
    private $medias;

    /**
     * Constructor
     */
    public function __construct() {
        $this->medias = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set btName
     *
     * @param string $btName
     * @return PersonData
     */
    public function setBtName($btName) {
        $this->btName = $btName;

        return $this;
    }

    /**
     * Get btName
     *
     * @return string
     */
    public function getBtName() {
        return $this->btName;
    }

    /**
     * Set btUid
     *
     * @param string $btUid
     * @return PersonData
     */
    public function setBtUid($btUid) {
        $this->btUid = $btUid;

        return $this;
    }

    /**
     * Get btUid
     *
     * @return string
     */
    public function getBtUid() {
        return $this->btUid;
    }

    /**
     * Get personDataId
     *
     * @return integer
     */
    public function getPersonDataId() {
        return $this->personDataId;
    }

    /**
     * Set person
     *
     * @param \MLReview\Entity\Person $person
     * @return PersonData
     */
    public function setPerson(\MLReview\Entity\Person $person = null) {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \MLReview\Entity\Person
     */
    public function getPerson() {
        return $this->person;
    }

    /**
     * Add medias
     *
     * @param \MLReview\Entity\Media $medias
     * @return \MLReview\Entity\PersonData
     */
    public function addMedias(Collection $medias) {
        foreach ($medias as $media) {
            $this->medias->add($media);
        }

        return $this;
    }

    /**
     * Remove medias
     *
     * @param \Doctrine\Common\Collections\Collection $medias
     */
    public function removeMedia(Collection $medias) {
        foreach ($medias as $media) {
            $this->medias->removeElement($media);
        }
    }

    /**
     * Get medias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedias() {
        return $this->medias;
    }

}

