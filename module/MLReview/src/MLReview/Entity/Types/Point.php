<?php

/*
 * This file is part of the Memory Lane Review Client.
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use MLReview\Entity\PointVO;

/**
 * Custom datatype Point for Doctrine.
 * Point is a geometry extension for MySQL.
 * Check example at
 * @link http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/basic-mapping.html#custom-mapping-types
 */
class Point extends Type {

    const POINT = 'point'; // modify to match your type name

    /**
     *
     * @param array $fieldDeclaration
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
     * @return string
     */

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
        return 'POINT';
    }

    /**
     *
     * @param \MLReview\Entity\Types\PointVO $value
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
     * @return \MLReview\Entity\PointVO
     */
    public function convertToPHPValue($value, AbstractPlatform $platform) {
        list($x, $y) = sscanf($value, 'POINT(%f %f)');
        return new PointVO($x, $y);
    }

    /**
     *
     * @param \MLReview\Entity\PointVO $value
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
     * @return type
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        if ($value instanceof PointVO) {
            $value = sprintf('POINT(%f)', $value->toString());
        }
        return $value;
    }

    /**
     *
     * @return type
     */
    public function getName() {
        return self::POINT;
    }

    /**
     *
     * @return boolean
     */
    public function canRequireSQLConversion() {
        return true;
    }

    /**
     *
     * @param type $sqlExpr
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
     * @return type
     */
    public function convertToPHPValueSQL($sqlExpr, AbstractPlatform $platform) {
        return sprintf('AsText(%s)', $sqlExpr);
    }

    /**
     *
     * @param type $sqlExpr
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
     * @return type
     */
    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform) {
        return sprintf('PointFromText(%s)', $sqlExpr);
    }

}

?>
