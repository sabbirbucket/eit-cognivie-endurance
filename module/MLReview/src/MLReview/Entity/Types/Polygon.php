<?php

/*
 * This file is part of the Memory Lane Review Client.
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use MLReview\Entity\PolygonVO;
use MLReview\Entity\PointVO;

/**
 * Custom datatype Polygon for Doctrine.
 * Polygon is a geometry extension for MySQL.
 * Check example at
 * @link http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/basic-mapping.html#custom-mapping-types
 */
class Polygon extends Type {

    const POLYGON = 'polygon'; // modify to match your type name

    /**
     *
     * @param array $fieldDeclaration
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
     * @return string
     */

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
        return 'POLYGON';
    }

    /**
     *
     * @param type $value
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
     * @return \MLReview\Entity\PolygonVO
     */
    public function convertToPHPValue($value, AbstractPlatform $platform) {
        $pointArray = array();
        $pointValues = sscanf($value, 'POLYGON((%f))');
        $listOfPoints = explode(",", $pointValues);
        $i = 0;
        foreach ($listOfPoints as $thisPoint) {
            list($x, $y) = sscanf($value, '%f %f');
            $thisPoint = new PointVO($x, $y);
            $pointArray[$i] = $thisPoint;
            $i++;
        }
        return new PolygonVO($pointArray);
    }

    /**
     *
     * @param $value
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
     * @return type
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        if ($value instanceof PolygonVO) {
            $value = sprintf('POLYGON((%f))', $value->toString());
        }
        return $value;
    }

    public function getName() {
        return self::POLYGON;
    }

    public function canRequireSQLConversion() {
        return true;
    }

    public function convertToPHPValueSQL($sqlExpr, AbstractPlatform $platform) {
        return sprintf('AsText(%s)', $sqlExpr);
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform) {
        return sprintf('PolygonFromText(%s)', $sqlExpr);
    }

}

?>
