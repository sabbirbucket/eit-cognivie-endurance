<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use MLReview\Entity\PolygonVO;

/**
 * Place
 *
 * @ORM\Table(name="Place")
 * @ORM\Entity
 */
class Place {

    /**
     * @var string
     *
     * @ORM\Column(name="placeName", type="string", length=100, nullable=false)
     */
    private $placeName;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=100, nullable=true)
     */
    private $src;

    /**
     * @var polygon
     *
     * @ORM\Column(name="GPSShape", type="polygon", nullable=true)
     */
    private $gpsShape;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="placeId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $placeId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Activity", mappedBy="place")
     */
    private $activities;

    /**
     * Constructor
     */
    public function __construct() {
        $this->activities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set placeName
     *
     * @param string $placeName
     * @return Place
     */
    public function setPlaceName($placeName) {
        $this->placeName = $placeName;

        return $this;
    }

    /**
     * Get placeName
     *
     * @return string
     */
    public function getPlaceName() {
        return $this->placeName;
    }

    /**
     * Set src
     *
     * @param string $src
     * @return Place
     */
    public function setSrc($src) {
        $this->src = $src;

        return $this;
    }

    /**
     * Get placeName
     *
     * @return string
     */
    public function getSrc() {
        return $this->src;
    }

    /**
     * Set gpsShape
     *
     * @param \MLReview\Entity\PolygonVO $gpsShape
     * @return \MLReview\Entity\Place
     */
    public function setGpsShape(PolygonVO $gpsShape) {
        $this->gpsShape = $gpsShape;

        return $this;
    }

    /**
     * Get gpsShape
     *
     * @return polygon
     */
    public function getGpsShape() {
        return $this->gpsShape;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Place
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Place
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Get placeId
     *
     * @return integer
     */
    public function getPlaceId() {
        return $this->placeId;
    }

    /**
     * Add activities
     *
     * @param \Doctrine\Common\Collections\Collection $activities
     * @return \MLReview\Entity\Place
     */
    public function addActivities(Collection $activities) {
        foreach ($activities as $act) {
            $act->setPlace($this);
            $this->activities->add($act);
        }

        return $this;
    }

    /**
     * Remove activities
     *
     * @param \Doctrine\Common\Collections\Collection $activities
     */
    public function removeActivities(Collection $activities) {
        foreach ($activities as $act) {
            $act->setPlace(null);
            $this->activities->removeElement($act);
        }
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities() {
        return $this->activities;
    }

}

