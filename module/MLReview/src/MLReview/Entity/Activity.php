<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Activity
 *
 * @ORM\Table(name="Activity")
 * @ORM\Entity (repositoryClass="MLReview\Entity\Repository\ActivityRepository")
 */
class Activity {

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=100, nullable=false)
     */
    private $src;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startTime", type="datetime", nullable=true)
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endTime", type="datetime", nullable=true)
     */
    private $endTime;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=150, nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="reviewed", type="boolean", nullable=false)
     */
    private $reviewed;

    /**
     * @var integer
     *
     * @ORM\Column(name="activityId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $activityId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Media", mappedBy="activity", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $medias;

    /**
     * @var \Place
     *
     * @ORM\ManyToOne(targetEntity="Place", inversedBy="activities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="placeId", referencedColumnName="placeId")
     * })
     */
    private $place;

    /**
     * @var \StoryCategory
     *
     * @ORM\ManyToOne(targetEntity="StoryCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="storyCategoryId", referencedColumnName="storyCategoryId")
     * })
     */
    private $storyCategory;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Person", inversedBy="activities")
     * @ORM\JoinTable(name="ActivityPerson",
     *   joinColumns={
     *     @ORM\JoinColumn(name="activityId", referencedColumnName="activityId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="personId", referencedColumnName="personId")
     *   }
     * )
     */
    private $persons;

    /**
     * Subfolder where files associated with this activity are stored.
     * @var string
     */
    protected $fileSubfolder = "activity";

    /**
     * Constructor
     */
    public function __construct() {
        $this->medias = new ArrayCollection();
        $this->persons = new ArrayCollection();
    }

    /**
     * Set src
     *
     * @param string $src
     * @return Media
     */
    public function setSrc($src) {
        $this->src = $src;

        return $this;
    }

    /**
     * Get src
     *
     * @return string
     */
    public function getSrc() {
        return $this->src;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     * @return Activity
     */
    public function setStartTime($startTime) {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime() {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     * @return Activity
     */
    public function setEndTime($endTime) {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime() {
        return $this->endTime;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Activity
     */
    public function setSubject($subject) {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject() {
        return $this->subject;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Activity
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set reviewed
     *
     * @param boolean $reviewed
     * @return Activity
     */
    public function setReviewed($reviewed) {
        $this->reviewed = $reviewed;

        return $this;
    }

    /**
     * Get reviewdd
     *
     * @return boolean
     */
    public function getReviewed() {
        return $this->reviewed;
    }

    /**
     * Get activityId
     *
     * @return integer
     */
    public function getActivityId() {
        return $this->activityId;
    }

    /**
     * Add medias
     *
     * @param \Doctrine\Common\Collections\Collection $medias
     * @return \MLReview\Entity\Activity
     */
    public function addMedias(Collection $medias) {
        foreach ($medias as $media) {
            $media->setActivity($this);
            $this->medias->add($media);
        }

        return $this;
    }

    /**
     * Remove medias
     *
     * @param \Doctrine\Common\Collections\Collection $medias
     */
    public function removeMedias(Collection $medias) {
        foreach ($medias as $media) {
            $media->setActivity(null);
            $this->medias->removeElement($media);
        }
    }

    /**
     * Get medias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedias() {
        return $this->medias;
    }

    /**
     * Set place
     *
     * @param \MLReview\Entity\Place $place
     * @return Activity
     */
    public function setPlace(\MLReview\Entity\Place $place = null) {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return \MLReview\Entity\Place
     */
    public function getPlace() {
        return $this->place;
    }

    /**
     * Set storyCategory
     *
     * @param \MLReview\Entity\StoryCategory $storyCategory
     * @return Activity
     */
    public function setStoryCategory(\MLReview\Entity\StoryCategory $storyCategory = null) {
        $this->storyCategory = $storyCategory;

        return $this;
    }

    /**
     * Get storyCategory
     *
     * @return \MLReview\Entity\StoryCategory
     */
    public function getStoryCategory() {
        return $this->storyCategory;
    }

    /**
     * Add persons
     *
     * @param \Doctrine\Common\Collections\Collection $persons
     * @return \MLReview\Entity\Activity
     */
    public function addPersons(Collection $persons) {
        foreach ($persons as $person) {
            $this->persons->add($person);
        }

        return $this;
    }

    /**
     * Remove persons
     *
     * @param \Doctrine\Common\Collections\Collection $persons
     */
    public function removePersons(Collection $persons) {
        foreach ($persons as $person) {
            $this->persons->removeElement($person);
        }
    }

    /**
     * Get persons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersons() {
        return $this->persons;
    }

    /**
     * Get subfolder for files
     * @return string
     */
    public function getFileSubfolder() {
        return $this->fileSubfolder;
    }

    /**
     * Set subfolder for files
     * @param string $fileSubfolder
     */
    public function setFileSubfolder($fileSubfolder) {
        $this->fileSubfolder = $fileSubfolder;
    }

}

