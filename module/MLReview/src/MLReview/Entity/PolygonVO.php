<?php

/*
 * This file is part of the Memory Lane Review Client.
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity;

/**
 * Value object for the database type Polygon
 */
class PolygonVO {

    /**
     *
     * @param array $array
     */
    public function __construct(array $array) {
        $this->points = $array;
    }

    /**
     * @return array
     */
    public function getPoints() {
        return $this->points;
    }

    /**
     * @return string
     */
    public function toString() {
        $arrayPointsVO = $this->getPoints();
        $returnString = "";
        foreach ($arrayPointsVO as $pointVO) {
            $returnString = $returnString . $pointVO->toString() . ', ';
        }
        $returnString = rtrim($returnString, ', ');
        return $returnString;
    }

}

?>
