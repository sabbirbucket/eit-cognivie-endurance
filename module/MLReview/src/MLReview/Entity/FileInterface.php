<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity;

/**
 * Interface to tell the FileHandlerListener that this is an Entity handling files.
 */
interface FileInterface {

    public function getSrc();

    public function setSrc($src);

    public function getFileSubfolder();

    public function setFileSubfolder($subfolder);
}

?>
