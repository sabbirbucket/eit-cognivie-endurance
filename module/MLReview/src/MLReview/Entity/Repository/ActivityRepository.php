<?php

/*
 * This file is part of the Memory Lane Review Client.
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * This repository class is used by the Doctrine ORM
 * and specifies custom finders/SQL for an entity.
 */
class ActivityRepository extends EntityRepository {

    /**
     * Get ALL the activities a given date
     * From 00:00:01 to 23:59:59
     * @param string|\DateTime $date String or DateTime representing a date
     * @return array Activitiy Entities in array
     */
    public function findActivitiesOnDate($date) {
        //Check the whole date from 00:00:01 to 23:59:59
        if ($date instanceof \DateTime) {
            $dateTimestamp = $date->setTime(00, 00, 00)->getTimestamp();
        } else if (is_string($date)) {
            $dateTimestamp = strtotime($date);
        }
        $endDateTimestamp = strtotime("+1 day", $dateTimestamp);
        $endDateTimestamp = strtotime("-1 second", $endDateTimestamp);
        $endDateToSearch = new \DateTime();
        $endDateToSearch->setTimestamp($endDateTimestamp);
        $startDateTimestamp = strtotime("+1 second", $dateTimestamp);
        $startDateToSearch = new \DateTime();
        $startDateToSearch->setTimestamp($startDateTimestamp);

        //Get activities with this date
        $qb = $this->_em->createQueryBuilder();
        $query = $qb
                ->select("a")
                ->from("MLReview\Entity\Activity", "a")
                ->andwhere($qb->expr()->lte('a.startTime', ':endTime'))
                ->andwhere($qb->expr()->gte('a.startTime', ':startTime'))
                ->setParameter('startTime', $startDateToSearch)
                ->setParameter('endTime', $endDateToSearch)
                ->orderBy('a.startTime', 'ASC')
                ->getQuery();
        $result = $query->getResult();
        return $result;
    }

    /**
     * Get all the activity dates from the activity table
     * @return array on the form result[i][1] = "2013-03-01"
     */
    public function findOldActivityDates() {

        $qb = $this->_em->createQueryBuilder();
        $result = $qb
                //Silly hack to get just the date and not time
                //Reason is that doctrine does not have a date function for this...
                ->select('DISTINCT SUBSTRING(a.startTime,1,10)')
                ->from('MLReview\Entity\Activity', 'a')
                ->getQuery()
                ->getResult();
        return $result;
    }

    /**
     * Finds an activity with a certain starttime.
     * Used to check if an activity already exists when no id is available.
     * @param string|\DateTime $startTime
     * @return array|null A single array['activityId']
     */
    public function findActivityWithStartTime($startTime) {
        if ($startTime instanceof \DateTime) {
            //change format
            $dateToCompare = $startTime->format("Y-m-d H:i:s");
        } else if (is_String($startTime)) {
            //create a datetime object
            $dateToCompare = new \DateTime($startTime);
            $dateToCompare->format("Y-m-d H:i:s");
        } else {
            return null;
        }
        $qb = $this->_em->createQueryBuilder();
        $query = $qb
                ->select("a.activityId")
                ->from("MLReview\Entity\Activity", "a")
                ->andwhere($qb->expr()->eq('a.startTime', ':startTime'))
                ->setParameter('startTime', $dateToCompare)
                ->getQuery();
        $result = $query->getOneOrNullResult();
        return $result;
    }

    /**
     * Just a complicated EXAMPLE. Do not use this.
     * @param type $account
     * @param type $distance
     * @return type
     */
    /*
      public function getNearbyAccountsExample($account, $distance) {

      $rsm = new ResultSetMapping;

      $rsm->addEntityResult('Entities\Account', 'a');
      $rsm->addFieldResult('a', 'id', 'id');
      $rsm->addFieldResult('a', 'username', 'username');
      $rsm->addFieldResult('a', 'zip', 'zip');
      $rsm->addFieldResult('a', 'latitude', 'latitude');
      $rsm->addFieldResult('a', 'longitude', 'longitude');
      $rsm->addFieldResult('a', 'confirmed', 'confirmed');

      $query = $this->_em->createNativeQuery(
      "SELECT a.id, a.username, a.zip, a.latitude, a.longitude, a.confirmed,
      ( 3959 * acos( cos( radians(?) ) * cos( radians( a.latitude ) ) *
      cos( radians( a.longitude ) - radians(?) ) + sin( radians(?) ) *
      sin( radians( a.latitude ) ) ) ) AS distance
      FROM accounts a
      WHERE a.confirmed = 1
      GROUP BY a.id HAVING distance < ?
      ORDER BY distance", $rsm
      );

      $query->setParameter(1, $account->getLatitude());
      $query->setParameter(2, $account->getLongitude());
      $query->setParameter(3, $account->getLatitude());
      $query->setParameter(4, $distance, Type::INTEGER);

      return $query->getResult();
      }
     */
}

?>
