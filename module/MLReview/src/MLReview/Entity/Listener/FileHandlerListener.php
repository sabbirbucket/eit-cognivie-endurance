<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use MLReview\Entity\FileInterface;

/**
 * Handles what happens with uploaded files connected to entities
 * when entities are updated or deleted. This is where to handle copy and
 * delete of asset files.
 */
class FileHandlerListener {

    /**
     * Default value just in case
     * @var string
     */
    private $base_path = '/data/uploaded/';

    /**
     * Default value just in case
     * @var string
     */
    private $public_path = '/../../public';

    public function __construct($base_path, $public_path) {
        $this->base_path = $base_path;
        $this->public_path = $public_path;
    }

    /**
     * Happens BEFORE a persistance
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof FileInterface) {
            $this->moveFileToPermanent($entity);
        }
    }

    /**
     * Happens BEFORE an update
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof FileInterface) {
            $this->moveFileToPermanent($entity, $args);
        }
    }

    /**
     * Happens AFTER a removal
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
     */
    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof FileInterface) {
            $this->removeFile($entity);
        }
    }

    /**
     * Moves a file to permanent storage in the uploaded/subfolder part of public
     * @param object $entity
     * @param LifecycleEventArgs needed if update
     * @return type
     */
    private function moveFileToPermanent($entity, LifecycleEventArgs $args = null) {
        $src = $entity->getSrc();
        if (null === $src || !$this->isUploadedFile($src)) {
            //Can not move or delete this, just return
            return;
        }
        $subfolder = $entity->getFileSubfolder();
        $public_path = $this->public_path;
        $base_category_path = $this->base_path . $subfolder;

        $source = $public_path . $src;

        //Move to permanent storage
        $new_path = $base_category_path . '/' . basename($source);
        $destination = $public_path . $new_path;
        if ($source !== $destination) {
            $result = copy($source, $destination);
            //Rename does  not work for some reason, but copy/delete does...
            $result = unlink($source);
        }
        //The new path of this entity src
        $entity->setSrc($new_path);

        //If updating use this
        if ($args != null && $args->hasChangedField('src')) {
            $args->setNewValue('src', $new_path);
        }
    }

    /**
     * Removes a file when entities are deleted.
     * @param object $entity
     * @return type
     */
    private function removeFile($entity) {
        $src = $entity->getSrc();
        if (null === $src) {
            //Do nothing
            return;
        }
        $full_path = $this->public_path . $src;
        if (file_exists($full_path) && $this->isUploadedFile($full_path)) {
            //A file exists and we are allowed to remove it
            $result = unlink($full_path);
        }
    }

    /**
     * Check if a filename is uploaded.
     * Only files uploaded by the user should be deleted.
     * @param string $filename
     * @return boolean
     */
    private function isUploadedFile($filename) {
        return (strpos($filename, $this->base_path) !== false);
    }

}

?>
