<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Media
 *
 * @ORM\Table(name="Media")
 * @ORM\Entity
 */
class Media implements FileInterface {

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=100, nullable=false)
     */
    private $src;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mediaTime", type="datetime", nullable=true)
     */
    private $mediaTime;

    /**
     * @var PointVO
     *
     * @ORM\Column(name="GPSPos", type="point", nullable=true)
     */
    private $gpsPos;

    /**
     * @var integer
     *
     * @ORM\Column(name="mediaId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $mediaId;

    /**
     * @var \Activity
     *
     * @ORM\ManyToOne(targetEntity="Activity", inversedBy="medias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="activityId", referencedColumnName="activityId")
     * })
     */
    private $activity;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="PersonData", mappedBy="medias")
     */
    private $personDatas;

    /**
     * Subfolder where files associated with this activity are stored.
     * @var string
     */
    protected $fileSubfolder = "activity";

    /**
     * Constructor
     */
    public function __construct() {
        $this->personDatas = new ArrayCollection();
    }

    /**
     * Set src
     *
     * @param string $src
     * @return Media
     */
    public function setSrc($src) {
        $this->src = $src;

        return $this;
    }

    /**
     * Get src
     *
     * @return string
     */
    public function getSrc() {
        return $this->src;
    }

    /**
     * Set mediaTime
     *
     * @param \DateTime $mediaTime
     * @return Media
     */
    public function setMediaTime($mediaTime = null) {
        $this->mediaTime = $mediaTime;

        return $this;
    }

    /**
     * Get mediaTime
     *
     * @return \DateTime
     */
    public function getMediaTime() {
        return $this->mediaTime;
    }

    /**
     * Set gpsPos
     *
     * @param \MLReview\Entity\PointVO $gpsPos
     * @return \MLReview\Entity\Media
     */
    public function setGpsPos(PointVO $gpsPos = null) {
        $this->gpsPos = $gpsPos;

        return $this;
    }

    /**
     * Get gpsPos
     *
     * @return PointVO
     */
    public function getGpsPos() {
        return $this->gpsPos;
    }

    /**
     * Get mediaId
     *
     * @return integer
     */
    public function getMediaId() {
        return $this->mediaId;
    }

    /**
     * Set activity
     *
     * @param \MLReview\Entity\Activity $activity
     * @return Media
     */
    public function setActivity(\MLReview\Entity\Activity $activity = null) {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \MLReview\Entity\Activity
     */
    public function getActivity() {
        return $this->activity;
    }

    /**
     * Add personDatas
     *
     * @param \Doctrine\Common\Collections\Collection $personDatas
     * @return \MLReview\Entity\Media
     */
    public function addPersonDatas(Collection $personDatas) {
        foreach ($personDatas as $data) {
            $this->personDatas->add($data);
        }

        return $this;
    }

    /**
     * Remove personDatas
     *
     * @param \Doctrine\Common\Collections\Collection $personDatas
     */
    public function removePersonData(Collection $personDatas) {
        foreach ($personDatas as $data) {
            $this->medias->removeElement($data);
        }
    }

    /**
     * Get personDatas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonDatas() {
        return $this->personDatas;
    }

    /**
     * Get subfolder for files
     * @return string
     */
    public function getFileSubfolder() {
        return $this->fileSubfolder;
    }

    /**
     * Set subfolder for files
     * @param string $fileSubfolder
     */
    public function setFileSubfolder($fileSubfolder) {
        $this->fileSubfolder = $fileSubfolder;
    }

}

