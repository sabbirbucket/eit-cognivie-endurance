<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * Person
 *
 * @ORM\Table(name="Person")
 * @ORM\Entity
 */
class Person {

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=100, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=100, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=100, nullable=true)
     */
    private $src;

    /**
     * @var string
     *
     * @ORM\Column(name="personalInfo", type="string", length=255, nullable=true)
     */
    private $personalInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=40, nullable=true)
     */
    private $phone;

    /**
     * @var integer
     *
     * @ORM\Column(name="personId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="PersonData", mappedBy="person")
     */
    private $personDatas;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Activity", mappedBy="persons")
     */
    private $activities;

    /**
     * Constructor
     */
    public function __construct() {
        $this->personDatas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->activities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Person
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Person
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * Set src
     *
     * @param string $src
     * @return Person
     */
    public function setSrc($src) {
        $this->src = $src;

        return $this;
    }

    /**
     * Get src
     *
     * @return string
     */
    public function getSrc() {
        return $this->src;
    }

    /**
     * Set personalInfo
     *
     * @param string $personalInfo
     * @return Person
     */
    public function setPersonalInfo($personalInfo) {
        $this->personalInfo = $personalInfo;

        return $this;
    }

    /**
     * Get personalInfo
     *
     * @return string
     */
    public function getPersonalInfo() {
        return $this->personalInfo;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Person
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Get personid
     *
     * @return integer
     */
    public function getPersonId() {
        return $this->personId;
    }

    /**
     * Add personDatas
     *
     * @param \Doctrine\Common\Collections\Collection $personDatas
     * @return \MLReview\Entity\Person
     */
    public function addPersonDatas(Collection $personDatas) {
        foreach ($personDatas as $data) {
            $data->setPerson($this);
            $this->personDatas->add($data);
        }

        return $this;
    }

    /**
     * Remove personDatas
     *
     * @param \Doctrine\Common\Collections\Collection $personDatas
     */
    public function removePersonDatas(Collection $personDatas) {
        foreach ($personDatas as $data) {
            $data->setPerson(null);
            $this->personDatas->removeElement($data);
        }
    }

    /**
     * Get personDatas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonDatas() {
        return $this->personDatas;
    }

    /**
     * Add activities
     *
     * @param \MLReview\Entity\Activity $activities
     * @return Person
     */
    public function addActivities(Collection $activities) {
        foreach ($activities as $act) {
            $this->activities->add($act);
        }

        return $this;
    }

    /**
     * Remove activities
     *
     * @param \Doctrine\Common\Collections\Collection $activities
     */
    public function removeActivities(Collection $activities) {
        foreach ($activities as $act) {
            $this->activities->removeElement($act);
        }
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities() {
        return $this->activities;
    }

}

