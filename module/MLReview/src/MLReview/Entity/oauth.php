<?php

class oauth
{
    protected $callbackUrl;
    protected $siteUrl;
    protected $consumerKey;
    protected $consumerSecret;
    protected $oauth_token;
    protected $tokenSecret;
    
    public function __construct()
    {
        
    }
    
    public function set_oauth_token_secret($oauth_token,$tokenSecret)
    {
        $this->oauth_token = $oauth_token;
        $this->tokenSecret = $tokenSecret;
    }
    public function get_config()
    {
        return array(
                         'callbackUrl' => $this->callbackUrl,
                         'siteUrl' => $this->siteUrl,
                         'consumerKey' => $this->consumerKey,
                         'consumerSecret' => $this->consumerSecret       
                     );
    }
}
?>
