<?php

/*
 * This file is part of the Memory Lane Review Client.
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Storycategory
 *
 * @ORM\Table(name="StoryCategory")
 * @ORM\Entity
 */
class StoryCategory {

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="storyCategoryId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $storyCategoryId;

    /**
     * Set name
     *
     * @param string $name
     * @return StoryCategory
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return StoryCategory
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Get storyCategoryId
     *
     * @return integer
     */
    public function getStoryCategoryId() {
        return $this->storyCategoryId;
    }

}

