<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Util;

use Doctrine\ORM\EntityManager;

class DisplayDateGenerator {

    /**
     *
     * @var \Doctrine\ORM\EntityManage
     */
    protected $entityManager;

    /**
     * Todays date
     * @var string|\DateTime
     */
    protected $today;

    /**
     *
     * @param \Doctrine\ORM\EntityManager $em
     * @param string|\DateTime $today
     */
    public function __construct(EntityManager $em, $today) {
        $this->entityManager = $em;
        $this->today = $today;
    }

    /**
     * Creates an array holding all the Activity entities for today,
     * yesterday and the day before that.
     * @return array Dates and collections with Activity entities.
     */
    public function generate() {
        $today_date = $this->today;
        if ($today_date instanceof \DateTime) {
            $today_date = $today_date->format('Y-m-d');
        }
        //Find all activites today
        $today_result = $this->entityManager
                ->getRepository('MLReview\Entity\Activity')
                ->findActivitiesOnDate($today_date);
        //Find all activities yesterday
        $yesterday = strtotime("-1 day", strtotime($today_date));
        $yesterday_date = strftime("%Y-%m-%d", $yesterday);
        $yesterday_result = $this->entityManager
                ->getRepository('MLReview\Entity\Activity')
                ->findActivitiesOnDate($yesterday_date);
        //Find all aktivities daybefore
        $daybefore = strtotime("-2 day", strtotime($today_date));
        $daybefore_date = strftime("%Y-%m-%d", $daybefore);
        $daybefore_result = $this->entityManager
                ->getRepository('MLReview\Entity\Activity')
                ->findActivitiesOnDate($daybefore_date);

        return array(
            'today_date' => $today_date,
            'today_result' => $today_result,
            'yesterday_date' => $yesterday_date,
            'yesterday_result' => $yesterday_result,
            'daybefore_date' => $daybefore_date,
            'daybefore_result' => $daybefore_result
        );
    }

    /**
     *
     * @return string|\DateTime
     */
    public function getToday() {
        return $this->today;
    }

    /**
     *
     * @param string|\DateTime $today
     */
    public function setToday($today) {
        $this->today = $today;
    }

}

?>
