<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Util;

/**
 * Handles the translation of the webpages together with poedit.
 */
class Translator {

    /**
     * A hack to get poedit to find strings inside Config-files, Forms and Labels.
     * This is just a dummy function! But needed for poedit.
     * @param string $str
     * @return string
     */
    public static function translate($str) {
        return $str;
    }

    /**
     * Formats a datetime object into a string.
     * @param \DateTime $dateObj
     * @return string formatted string
     */
    public static function dateToString($dateObj) {
        if ($dateObj == null) {
            return "";
        }
        if ($dateObj instanceof \DateTime) {
            return $dateObj->format(self::getDateFormat());
        }
        return "";
    }

    /**
     * Formats a string into a datetime object
     * @param string $str
     * @return \DateTime object
     */
    public static function stringToDate($str) {
        if ($str === null || $str === "") {
            return null;
        }
        if (is_string($str)) {
            $validator = new \Zend\Validator\Date(array('format' => self::getDateFormat()));
            if ($validator->isValid($str)) {
                return new \DateTime(date(self::getDateFormat(), strtotime($str)));
            }
        }
        //Something went wrong. Returning null.
        return null;
    }

    /**
     * Get the format of our date
     * @todo Should be different for different locales
     * @return string
     */
    public static function getDateFormat() {
        return 'Y-m-d H:i';
    }

    /**
     * Translated stuff that does not fit anywhere else.
     * The translate is needed by poedit though.
     * @return array Array with translated stuff
     */
    public static function getTranslatedArray() {
        return array(
            /* Translation for the story categories */
            'story_category_translation' => array(
                'Child Life' => MLReview\Util\Translator::translate('Child Life'),
                'Adult Life' => MLReview\Util\Translator::translate('Adult Life'),
                'Older Life' => MLReview\Util\Translator::translate('Older Life'),
            )
        );
    }

}

?>