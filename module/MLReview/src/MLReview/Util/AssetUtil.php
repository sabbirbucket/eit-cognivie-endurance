<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Util;

use Zend\Stdlib\ErrorHandler;
use Zend\Mvc\Exception;
use Zend\Filter\File\RenameUpload;

/*
 * Class to handle the different assets/images in the program
 * @todo Mostly needed when multiple users are introduced
 */

class AssetUtil {

    /**
     * Base upload path for files
     * @var string
     */
    protected $base_upload_path = '/data/uploaded/';

    /**
     * Public path of any files viewable by users
     * @var string
     */
    protected $public_path = '/../../public';

    /**
     * The full path of uploaded files
     * @var string
     */
    protected $full_upload_path;

    /**
     * Constructor
     * @param string $base_upload_path
     * @param string $public_path
     */
    public function __construct($base_upload_path, $public_path) {
        $this->base_upload_path = $base_upload_path;
        $this->public_path = $public_path;
        $this->init();
    }

    /**
     * Called everytime a path changes to update the full upload path
     */
    private function init() {
        //Set paths
        $this->full_upload_path = $this->public_path . $this->base_upload_path;
    }

    /**
     * Check if a filename is uploaded
     * @param string $filename
     * @return boolean
     */
    public function isUploadedFile($filename) {
        return (strpos($filename, $this->base_upload_path) !== false);
    }

    /**
     * Check if a filename is temporary uploaded
     * @param string $filename
     * @return boolean
     */
    public function isTempFile($filename) {
        return (strpos($filename, $this->getTempBaseUploadPath()) !== false);
    }

    /**
     * The default public path
     * @return string
     */
    public function getPublicPath() {
        return $this->public_path;
    }

    /**
     * Get a base upload path
     * @param string $subfolder ex. 'temp' or 'person'
     * @return string
     */
    public function getBaseUploadPath($subfolder = "") {
        return $this->base_upload_path . $subfolder;
    }

    /**
     * Get a full upload path
     * @param string $category
     * @return string
     */
    public function getFullUploadPath($category = "") {
        return $this->full_upload_path . $category;
    }

    /**
     * Get the temp upload path
     * @return string
     */
    public function getTempBaseUploadPath() {
        return $this->getBaseUploadPath('temp');
    }

    /**
     * Get the full temp upload path
     * @return string
     */
    public function getTempFullUploadPath() {
        return $this->getFullUploadPath('temp');
    }

    /**
     * Apply a random string to a filename
     * @param  string $filename
     * @return string
     */
    public function applyRandomToFilename($filename) {
        $info = pathinfo($filename);
        $filename = $info['filename'] . uniqid('_');
        if (isset($info['extension'])) {
            $filename .= '.' . $info['extension'];
        }
        return $filename;
    }

    /**
     * Copies a file. Wrapper to handle exception
     * IMPORTANT: Check permissions on the disk!
     * @param string $source Full source of the file
     * @param string $destination Full destination of the file
     * @throws Exception\RuntimeException
     */
    public static function copyFile($source, $destination) {
        ErrorHandler::start();
        $result = copy($source, $destination);
        $warningException = ErrorHandler::stop();
        if (!$result || null !== $warningException) {
            throw new Exception\RuntimeException(
            sprintf("File '%s' could not be copied. An error occurred while processing the file.", $source), 0, $warningException
            );
        }
    }

    /**
     * Renames a file. Wrapper to handle exception.
     * IMPORTANT: Check permissions on the disk!
     * @param string $source Full source of the file
     * @param string $destination Full destination of the file
     * @throws Exception\RuntimeException
     */
    public static function renameFile($source, $destination) {
        ErrorHandler::start();
        $result = rename($source, $destination);
        $warningException = ErrorHandler::stop();
        if (!$result || null !== $warningException) {
            throw new Exception\RuntimeException(
            sprintf("File '%s' could not be renamed. An error occurred while processing the file.", $source), 0, $warningException
            );
        }
    }

    /**
     * Deletes a file. Wrapper to handle exception.
     * IMPORTANT: Check permissions on the disk!
     * @param string $source Full source of the file
     * @throws Exception\RuntimeException
     */
    public static function deleteFile($source) {
        ErrorHandler::start();
        $result = unlink($source);
        $warningException = ErrorHandler::stop();
        if (!$result || null !== $warningException) {
            throw new Exception\RuntimeException(
            sprintf("File '%s' could not be deleted. An error occurred while processing the file.", $source), 0, $warningException
            );
        }
    }

    /**
     * Upload and move a temporary file to public temp storage
     * This way the file can be viewed by the user
     * @param mixed $file Full path of file to change or $_FILES data array
     * @param string $old_file_to_delete The old temp file to be deleted
     * @return string The base path of the new temp file
     */
    public function uploadTempImage($file, $old_file_to_delete = null) {
        $base_temp_path = $this->getTempBaseUploadPath();
        $full_temp_path = $this->getTempFullUploadPath();

        //Move the image to viewable public place
        $filter = new RenameUpload(array(
            "randomize" => true,
            'use_upload_name' => true,
            "overwrite" => true
        ));
        $filter->setTarget($full_temp_path);
        $returnFile = $filter->filter($file);
        try {
            if ($this->isTempFile($old_file_to_delete)) {
                //Only delete if uploaded temp file
                AssetUtil::deleteFile($old_file_to_delete);
            }
        } catch (\Exception $e) {
            //TODO: Could not delete file. Log this and continue.
        }
        $return_path = $base_temp_path . '/' . basename($returnFile['tmp_name']);

        //We have uploaded AND moved the temp file to this location
        return $return_path;
    }

    /**
     * Handles the ins and outs of copying a image file to the server
     * Used by the person and place controllers
     * @param \Zend\Http\Request $request
     * @param \Zend\Form\Form $form
     * @param string $category
     * @return boolean If the form is valid and ready to be persisted
     */
    public function imageFormHelper(\Zend\Http\Request $request, \Zend\Form\Form $form, $category) {
        $src_element = $form->get($category)->get('src');

        // Make certain to merge the files info!
        $post = array_merge_recursive(
                $request->getPost()->toArray(), $request->getFiles()->toArray()
        );

        $form->setData($post);

        $temp_image = $form->get('tempImage')->getValue();
        //$src_element = $form->get($category)->get('src');
        //Special flag means only image uploading in this case
        $special = $form->get('specialFlag')->getValue();
        if (!empty($special)) {
            //Image is uploaded. Set this image to the temporary image src
            $fileErrors = $form->get('upload')->get('file')->getMessages();
            if (empty($fileErrors)) {
                $file = $form->get('upload')->get('file')->getValue();
                //Upload the temp file to public storage
                //This enables users to actually see the file in the webpages
                $temp_image = $this->uploadTempImage($file, $temp_image);
                //Set the form value as well
                $form->get('tempImage')->setValue($temp_image);
                $form->get('specialFlag')->setValue(null);
            }
        } else if ($form->isValid()) {
            //Move file to permanent storage
            $new_path = $this->moveFileToPermanent($temp_image, $category, $src_element->getValue());
            //Set src for the database in later persist
            $src_element->setValue($new_path);
            $form->get('tempImage')->setValue($new_path);

            //Form is valid
            return true;
        }

        //Form is not valid tell so
        return false;
    }

    /**
     * Move a file (often a temporary) to permanent storage
     * @param string $file_to_move Base path of file to move
     * @param string $subfolder Category ex 'person' or 'activity'
     * @param string $old_file_to_delete Base path. Will try to delete this old file
     * @return string The file's new permantent base path
     */
    public function moveFileToPermanent($file_to_move, $subfolder, $old_file_to_delete = null) {
        $public_path = $this->getPublicPath();
        $base_category_path = $this->getBaseUploadPath($subfolder);

        //Check if this file is not a default file.
        //If default file, just return
        if (!$this->isUploadedFile($file_to_move)) {
            //Can not move this, just return
            return $file_to_move;
        } else {
            //The file is an uploaded file, not just a default file
            $source = $public_path . $file_to_move;

            //Move to permanent storage
            $new_path = $base_category_path . '/' . basename($source);
            $full_new_path = $public_path . $new_path;
            if ($source !== $full_new_path) {
                AssetUtil::copyFile($source, $full_new_path);
                //Rename does  not work for some reason, but delete does...
                //usleep(500000); //delay 0.5 sec
                if ($this->isUploadedFile($source)) {
                    AssetUtil::deleteFile($source);
                }
            }
            //Delete old src
            if (!empty($old_file_to_delete)) {
                $full_old_src = $public_path . $old_file_to_delete;
                if (($old_file_to_delete !== $new_path) && file_exists($full_old_src) && $this->isUploadedFile($full_old_src)) {
                    AssetUtil::deleteFile($full_old_src);
                }
            }

            //Return our new base path
            return $new_path;
        }
    }

    /**
     * Set the paths for this class
     * @param string $base_path
     * @param string $public_path
     */
    public function setPaths($base_path, $public_path) {
        $this->base_upload_path = $base_path;
        $this->public_path = $public_path;
        $this->init();
    }

    /**
     *
     * @param string $base_upload_path
     */
    public function setBaseUploadPath($base_upload_path) {
        $this->base_upload_path = $base_upload_path;
        $this->init();
    }

    /**
     *
     * @param string $public_path
     */
    public function setPublicPath($public_path) {
        $this->public_path = $public_path;
        $this->init();
    }

}
?>


