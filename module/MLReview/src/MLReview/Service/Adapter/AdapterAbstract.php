<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Service\Adapter;

use MLReview\Service\Adapter\Filter\FilterInterface;

/**
 * Abstract class that all adapters should extend.
 * Handles filters.
 */
abstract class AdapterAbstract {

    /**
     * Just a simple array for now
     * @todo upgrade to interchangable collection
     * @var array
     */
    protected $filters = array();

    /**
     * Called when we are ready to convert data in this adapter.
     */
    abstract public function convert();

    /**
     * Adds one more filter
     * @param \MLReview\Service\Adapter\Filter\FilterInterface $filter
     */
    public function addFilter(FilterInterface $filter) {
        $this->filters[] = $filter;
    }

    /**
     * Get all the filters
     * @return array
     */
    public function getFilters() {
        return $this->filters;
    }

    /**
     * Apply all the registered filters, one by one
     * @param Doctrine\Common\Collections\Collection $activityList
     * @return Doctrine\Common\Collections\Collection
     */
    public function applyFilters($activityList) {
        $filters = $this->getFilters();
        for ($i = 0; $i < \count($filters); $i++) {
            $filters[$i]->filter($activityList);
        }
        return $activityList;
    }

}

?>
