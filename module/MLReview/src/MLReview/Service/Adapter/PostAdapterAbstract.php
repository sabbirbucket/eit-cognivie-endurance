<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Service\Adapter;

/**
 * Abstract class for adapters that get POSTs of data from activity recognition modules.
 */
abstract class PostAdapterAbstract extends AdapterAbstract {

    /**
     * The received post
     * @var array
     */
    protected $post;

    /**
     * Activities that the activity recognition module thinks are old.
     * Probably older than 3 days.
     * @var Doctrine\Common\Collections\Collection
     */
    protected $oldActivities;

    /**
     * Activities extracted from the activity recognition module.
     * @var Doctrine\Common\Collections\Collection
     */
    protected $activities;

    /**
     * Get posted data
     * @return array
     */
    public function getPost() {
        return $this->post;
    }

    /**
     * Set posted data
     * @param type $post
     */
    public function setPost($post) {
        $this->post = $post;
    }

    /**
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getOldActivities() {
        return $this->applyFilters($this->oldActivities);
    }

    /**
     *
     * @param Doctrine\Common\Collections\Collection $oldActivities
     */
    public function setOldActivities($oldActivities) {
        $this->oldActivities = $oldActivities;
    }

    /**
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getActivities() {
        return $this->applyFilters($this->activities);
    }

    /**
     *
     * @param Doctrine\Common\Collections\Collection $activities
     */
    public function setActivities($activities) {
        $this->activities = $activities;
    }

}

?>
