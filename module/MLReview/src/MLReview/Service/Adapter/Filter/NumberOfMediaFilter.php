<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Service\Adapter\Filter;

/**
 * Filters activites based on how many media the activity has.
 */
class NumberOfMediaFilter extends FilterAbstract implements FilterInterface {

    /**
     * Number of media break point
     * @var int
     */
    protected $nrOfMedia = 0;

    /**
     * Constructor
     * @param int $numberOfMedia
     */
    public function __construct($numberOfMedia) {
        $this->nrOfMedia = $numberOfMedia;
    }

    /**
     * The actual filtering
     * @param Doctrine\Common\Collections\Collection $activityList
     */
    public function filter(&$activityList) {
        if (empty($activityList)) {
            return;
        }
        foreach ($activityList as $key => $value):
            $medias = $value->getMedias();
            $act_nr_of_medias = $medias->count();
            if ($act_nr_of_medias < $this->nrOfMedia) {
                //Too few medias!
                //Simply remove this activity from the list
                $activityList->remove($key);
            }
        endforeach;
    }

    /**
     * Get nr of media
     * @return int
     */
    public function getNrOfMedia() {
        return $this->nrOfMedia;
    }

    /**
     * Set number of media
     * @param int $numberOfMedia
     */
    public function setNrOfMedia($numberOfMedia) {
        $this->nrOfMedia = $numberOfMedia;
    }

}

?>
