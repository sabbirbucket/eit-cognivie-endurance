<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Service\Adapter\Filter;

/**
 * Abstract class that handles the options for filters.
 * Automatic calls setters for these options.
 */
abstract class FilterAbstract {

    /**
     * Array of options for this filter
     * @var array
     */
    protected $options = array();

    /**
     *
     * @param array|\Traversable $options
     * @return \MLReview\Service\Adapter\Filter\FilterAbstract
     * @throws Exception\InvalidArgumentException
     */
    public function setOptions($options) {

        if (!is_array($options) && !$options instanceof \Traversable) {
            throw new Exception\InvalidArgumentException(sprintf(
                    '"%s" expects an array or Traversable; received "%s"', __METHOD__, (is_object($options) ? get_class($options) : gettype($options))
            ));
        }

        foreach ($options as $key => $value) {
            $setter = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
            if (method_exists($this, $setter)) {
                $this->{$setter}($value);
            } elseif (array_key_exists($key, $this->options)) {
                $this->options[$key] = $value;
            } else {
                throw new Exception\InvalidArgumentException(sprintf(
                        'The option "%s" does not have a matching %s setter method or options[%s] array key', $key, $setter, $key
                ));
            }
        }
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getOptions() {
        return $this->options;
    }

}

?>
