<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Service\Adapter\Filter;

/**
 * Filters the activities by minimum time between startTime and endTime.
 *
 */
class MinTimeActivityFilter extends FilterAbstract implements FilterInterface {

    /**
     * Number of minutes that is the breakpoint.
     * @var int
     */
    protected $minutes = 0;

    /**
     * Constructor
     * @param int $minutes
     */
    public function __construct($minutes) {
        $this->minutes = $minutes;
    }

    /**
     * The actual filtering
     * @param Doctrine\Common\Collections\Collection $activityList
     */
    public function filter(&$activityList) {
        if (empty($activityList)) {
            return;
        }
        foreach ($activityList as $key => $value):
            $startTime = $value->getStartTime();
            $endTime = $value->getEndTime();
            $diff = $startTime->diff($endTime); // $diff is an Instance of DateInterval
            $mins = ($diff->days * 60 * 24) + ($diff->h * 60) + $diff->i;
            if ($mins < $this->minutes) {
                //Too shot activity!
                //Simply remove this activity from the list
                $activityList->remove($key);
            }
        endforeach;
    }

    /**
     * Get minutes
     * @return int
     */
    public function getMinutes() {
        return $this->minutes;
    }

    /**
     * Set minutes
     * @param int $minutes
     */
    public function setMinutes($minutes) {
        $this->minutes = $minutes;
    }

}

?>
