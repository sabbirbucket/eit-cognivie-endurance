<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Service\Adapter\Filter;

/**
 * Interface for different filters.
 * All filters must filter activity lists.
 */
interface FilterInterface {

    /**
     *
     * @param Doctrine\Common\Collections\Collection $activityList
     */
    public function filter(&$activityList);
}

?>
