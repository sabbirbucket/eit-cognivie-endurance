<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Service\Adapter;

use MLReview\Entity\Activity;
use MLReview\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

/*
 * Class to translate values between the MLCluster module and our MLReview client.
 * This class is a rewrite from the OLD version of MemoryLane Review Client,
 * hence the different structure.
 */

class MLClusterAdapter extends PostAdapterAbstract {

    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * Where the logsync media can be found.
     * Default value just in case.
     * @var string
     */
    protected $mediaLocation = '/data/logsync/';

    /**
     *
     * @param \Doctrine\ORM\EntityManager $em
     * @param string $media_location
     */
    public function __construct(EntityManager $em, $media_location) {
        $this->entityManager = $em;
        $this->mediaLocation = $media_location;
    }

    /**
     * Converts the posted array from MLCluster to Entities of our MLReview module.
     * @return
     */
    public function convert() {
        if (empty($this->post)) {
            //Can not do anything
            return;
        }
        /* These are the posts sent by the MLCluster Algorithm
         * Found in the file indexScript.php in the module MLCluster
         * If only old activites found:
         * window.document.detect.the_clustering_activity_old_save.value = string_to_be_sent_to_server_old;
         * window.document.detect.the_clustering_index_old_Not_save.value = clustering_index; //Not needed here
         * window.document.detect.the_clustering_index_old_save.value = clustering_index_old;
         * //Flag to check if there are only old activities (more than 3 days old)
         * window.document.detect.only_old.value = true;
         *
         * If old AND new activites found:
         * window.document.detect.the_clustering_activity_old.value = string_to_be_sent_to_server_old;
         * window.document.detect.the_clustering_index_old.value = clustering_index_old;
         * window.document.detect.the_clustering_activity.value = string_to_be_sent_to_server;
         * window.document.detect.the_clustering_index.value = clustering_index;
         * window.document.detect.image_source.value = image_source; //Not needed here
         * //Flag to check if there are only old activities (more than 3 days old)
         * window.document.detect.only_old.value = false;
         */
        if ($this->post['only_old'] !== "true") {
            //Old AND new activities to collect
            $old_activities = $this->collectActivities(
                    $this->post['the_clustering_activity_old'], $this->post['the_clustering_index_old']
            );
            $this->setOldActivities($old_activities);
            $new_activities = $this->collectActivities(
                    $this->post['the_clustering_activity'], $this->post['the_clustering_index']
            );
            $this->setActivities($new_activities);
        } else {
            //Old activities to collect
            $old_activities = $this->collectActivities(
                    $this->post['the_clustering_activity_old_save'], $this->post['the_clustering_index_old_save']
            );
            $this->setOldActivities($old_activities);
        }
    }

    /**
     * Collects all the activities from the posted data.
     * This code is taken from the old file save_old.php.
     * (part of the old Memorylane Review Client)
     * @param array $activities Array with all the activities
     * @param int|string $index The start index for the activities in this array
     * @return null|\Doctrine\Common\Collections\ArrayCollection
     */
    private function collectActivities($activities, $index) {
        if (empty($activities) || $index === null) {
            return null;
        }
        //Our activity storage
        $activity_collection = new ArrayCollection();
        /////////////////////////////
        ///////////////////////////////
        //////////////////////////////
        $clustering_index = $index;
        $my_array = explode(",", $activities);
        $my_array = str_replace("~", ",", $my_array);

        $multiArr = array();
        $start = 0;
        $len = 2500;
        for ($i = 0; $i < 50; $i++) {
            $multiArr[$i] = array_slice($my_array, $start, $len);
            $start += 2500;
        }
        //if there are some old activities, we need to add them to the database and delete them from the XML files
        /* $story = 'none'; */

        for ($counter = 0; $counter < $clustering_index; $counter++) {
            $subject = $multiArr[$counter][0];
            if ($subject == ' ') {
                $subject = 'Pictures';
            }
            $description = '';

            $place_image = $multiArr[$counter][4];
            //Hack to get id
            $place_id = end(explode("/", $place_image));

            $person_image = $multiArr[$counter][5];
            //Hack to get id
            $person_id = end(explode("/", $person_image));

            $num_persons = $multiArr[$counter][6]; // number of the persons
            //find the representative
            $num_images = $multiArr[$counter][18]; // number of the images
            if ($num_images > 0) {
                $representative_image = $this->mediaLocation . $multiArr[$counter][19];
            } else {
                if ($place_image != 'n/a') {
                    $representative_image = 'place'; // $multiArr[$counter][5];
                } else {
                    $representative_image = 'person'; // $multiArr[$counter][6];
                }
            }

            $start_time = $multiArr[$counter][1];
            $end_time = $multiArr[$counter][2];
            $date = $multiArr[$counter][3];
            $startTimeAndDate = new \DateTime($date . ' ' . $start_time);
            $endTimeAndDate = new \DateTime($date . ' ' . $end_time);

            //Check if an activity already exist with this date
            //If so, just continue with next aktivity
            $found_activity = $this->entityManager->getRepository('MLReview\Entity\Activity')
                    ->findActivityWithStartTime($startTimeAndDate);
            /*
              $builder = $this->entityManager->getRepository('MLReview\Entity\Activity')
              ->createQueryBuilder('a');
              $find_activity = $builder
              ->andwhere($builder->expr()->eq('a.startTime', ':startTime'))
              ->setParameter('startTime', $startTimeAndDate)
              ->getQuery()
              ->getOneOrNullResult();
             */
            if ($found_activity !== null) {
                //Found an activity with exactly this startTime
                //Skip this activity then
                continue;
            }

            /*
              $dbh = mysql_connect("localhost", "root", "root3d")//THIS CONNECTS TO THE SERVER WITH YOUR DETAILS
              or die('I cannot connect to the database because: ' . mysql_error()); //IF IT CANNOT CONNECT, THIS ERROR MESSAGE WILL SHOW
              mysql_select_db("memorylane"); //THIS SELECTS THE CORRECT DATABASE FROM YOUR SERVER
              // first check if this activity exist
              $res = mysql_query('select activity_id from activity where start_time = "' . $start_time . '" and date = "' . $date . '"');

              $row = mysql_fetch_array($res);

              $activity_id = $row['activity_id'];
              //echo $activity_id. "<br>";
              if ($activity_id) {
              mysql_close();
              continue;
              }
             */

            /*
              $res = mysql_query('select max(activity_id) last_record from activity'); // this is to find the last ID and add 1 to it so we can insert a new record
              $row = mysql_fetch_array($res);
              $activity_id = $row['last_record'] + 1;
              // insert values to activity table with the provided info
              $query = "insert into activity values ('" . $activity_id . "','" . $subject . "', '" . $description . "','" . $date . "','" . $start_time . "', '" . $end_time . "','" . $representative_image . "','" . $story . "')";
              $result = mysql_query($query);
             */

            //Create a new activity
            $activity = new Activity();
            $activity->setSubject($subject);
            $activity->setDescription($description);
            $activity->setSrc($representative_image);
            $activity->setStartTime($startTimeAndDate);
            $activity->setEndTime($endTimeAndDate);
            //Not yet reviewed
            $activity->setReviewed(false);
            //No need to add storycategory, already empty
            //
/////////////////////////////////////
////////////////////////////////////
/////////////////////////////////////
//////////////////////////////////////
// now add the place

            if ($place_image != 'n/a') { // this means that there is a place related to the activity
                //IMPORTANT: Not really a place_image, but its id!
                $place = $this->entityManager->getRepository('MLReview\Entity\Place')->find($place_id);
                $activity->setPlace($place);

                /*
                  $res = mysql_query('select max(activity_place_id) last_record from activity_place'); // this is to find the last ID and add 1 to it so we can insert a new record
                  $row = mysql_fetch_array($res);
                  $activity_place_id = $row['last_record'] + 1;
                  // find the id of the place
                  $res = mysql_query('select *
                  from place, media where
                  place_id = reference_id and path_name = "' . $place_image . '"');
                  $row = mysql_fetch_array($res);
                  $place_id = $row['place_id'];
                  // now we have the data to insert
                  $query = "insert into activity_place values ('" . $activity_place_id . "','" . $activity_id . "', '" . $place_id . "')";
                  $result = mysql_query($query);
                 */
            } // end if ($place_image != 'n/a')
////////////////////////////////////
/////////////////////////////////////
//////////////////////////////////////
/////////////////////////////////////
// now add the persons
// first add the first person if there is one
            $person_collection = new ArrayCollection();

            if ($person_image != 'n/a') { // this means that there is a place related to the activity
                //IMPORTANT: Not really a person_image, but its id!
                $person = $this->entityManager->getRepository('MLReview\Entity\Person')->find($person_id);
                $person_collection->add($person);

                /*
                  $res = mysql_query('select max(activity_person_id) last_record from activity_person'); // this is to find the last ID and add 1 to it so we can insert a new record
                  $row = mysql_fetch_array($res);

                  $activity_person_id = $row['last_record'] + 1;  //*****************************************
                  // find the id of the place
                  $res = mysql_query('select *
                  from person , media where
                  person_id = reference_id and path_name = "' . $person_image . '"');
                  $row = mysql_fetch_array($res);
                  $person_id = $row['person_id'];
                  // now we have the data to insert
                  $query = "insert into activity_person values ('" . $activity_person_id . "','" . $activity_id . "', '" . $person_id . "')";
                  $result = mysql_query($query);
                 */
            }// end if ($person_image != 'n/a')
// now add the rest of the persons
            if ($num_persons > 1) { // this means that there are persons involved in the activity
                for ($i = 1; $i < $num_persons; $i++) { // take each person and relate him to the activity
                    $source = $multiArr[$counter][$i + 6];
                    //Hack to get id
                    $source_id = end(explode("/", $source));
                    $person = $this->entityManager->getRepository('MLReview\Entity\Person')->find($source_id);
                    $person_collection->add($person);

                    /*
                      $res = mysql_query('select max(activity_person_id) last_record from activity_person'); // this is to find the last ID and add 1 to it so we can insert a new record
                      $row = mysql_fetch_array($res);
                      $activity_person_id = $row['last_record'] + 1;
                      // find the id of the place
                      $res = mysql_query('select *
                      from person , media where
                      person_id = reference_id and path_name = "' . $source . '"');
                      $row = mysql_fetch_array($res);
                      $person_id = $row['person_id'];
                      // now we have the data to insert
                      $query = "insert into activity_person values ('" . $activity_person_id . "','" . $activity_id . "', '" . $person_id . "')";
                      $result = mysql_query($query);
                     */
                }
            } // the end od adding all the persons to the activity
            //Add all the persons
            $activity->addPersons($person_collection);
////////////////////////////
/////////////////////////////
/////////////////////////////
//////////////////////////////
//now add all the images
            $media_collection = new ArrayCollection();
            if ($num_images > 0) {
                for ($i = 0; $i < $num_images; $i++) { // take each person and relate him to the activity
                    $media_name = '';
                    //NOTE: Not good at all to have a magic number like 19 from nowhere. But this is how the code works for now
                    //TODO: Remake needed
                    $path_name = $this->mediaLocation . $multiArr[$counter][2 * $i + 19];
                    /*
                      $res = mysql_query('select max(media_id) last_record from media'); // this is to find the last ID and add 1 to it so we can insert a new record
                      $row = mysql_fetch_array($res);
                      $media_id = $row['last_record'] + 1;
                      $type = 'image';
                      $related_to = 'activity';
                      $time_stamp = '12:00:00';
                      $is_large = 'yes';
                      // now we have the data to insert
                      $query = "insert into media values ('" . $media_name . "','" . $media_id . "', '" . $type . "', '" . $related_to . "','" . $path_name . "','" . $time_stamp . "','" . $activity_id . "','" . $is_large . "')";
                      $result = mysql_query($query);
                     */
                    $media = new Media();
                    $media->setSrc($path_name);
                    //Add media no the collection
                    $media_collection->add($media);
                } // the end of adding images
            } // end if ($num_images >0)

            /* mysql_close(); */
            $activity->addMedias($media_collection);
            //Hack: Fix the representative
            if ($activity->getSrc() === 'person') {
                $activity->setSrc($activity->getPersons()->first()->getSrc());
            } else if ($activity->getSrc() === 'place') {
                $activity->setSrc($activity->getPlace()->getSrc());
            }
            $activity_collection->add($activity);
        } //end of for-loop for activities

        /* Removed alot of xml-removing code here.
         * TODO: Shall we clear the xml-logs as well? */

        //Return all the activities in an Entity collection
        return $activity_collection;
    }

}

?>
