<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\View\Helper;

use Zend\Http\Request;
use Zend\Mvc\Router\Http\TreeRouteStack as Router;
use Zend\View\Helper\AbstractHelper;

/**
 * A simple helper to display current route in web pages
 */
class GetRoute extends AbstractHelper {

    /**
     *
     * @var \Zend\Http\Request
     */
    protected $request;

    /**
     *
     * @var \Zend\Mvc\Router\Http\TreeRouteStack
     */
    protected $router;

    /**
     * Constructor
     * @param \Zend\Http\Request $req
     * @param \Zend\Mvc\Router\Http\TreeRouteStack $rout
     */
    public function __construct(Request $req, Router $rout) {
        $this->request = $req;
        $this->router = $rout;
    }

    /**
     * Will get teh current route as a string
     * @return string
     */
    public function __invoke() {

        $routeMatch = $this->router->match($this->request);
        if (!is_null($routeMatch)) {
            $route = $routeMatch->getMatchedRouteName();
            $action = $routeMatch->getParam('action');
            $id = $routeMatch->getParam('id');
            $result = $route;
            if (!empty($action)) {
                $result = $result . '/' . $action;
                if (!empty($id)) {
                    $result = $result . '/' . $id;
                }
            }
            return $result;
        }
    }

}

?>
