<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Returns an array with ALL the old activity dates
 */
class GetOldActivityDates extends AbstractHelper {

    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     *
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @return array With activity dates like "2013-03-01"
     */
    public function __invoke() {
        $dateTimes = $this->entityManager->getRepository('MLReview\Entity\Activity')
                ->findOldActivityDates();
        $result = array();
        //convert the array with DateTimes to strings
        foreach ($dateTimes as $dateTimeArray):
            $dateTime = reset($dateTimeArray);
            if (!empty($dateTime)) {
                $result[] = $dateTime;
            }
        endforeach;
        return $result;
    }

}
?>

