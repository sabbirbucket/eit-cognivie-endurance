<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Returns the user session
 * @todo Careful with that password!
 */
class GetSession extends AbstractHelper {

    /**
     *
     * @var \Zend\Session\Container
     */
    protected $session;

    /**
     *
     * @param \Zend\Session\Container $session
     */
    public function __construct($session) {
        $session->password = null;
        $this->session = $session;
    }

    /**
     *
     * @return \Zend\Session\Container
     */
    public function __invoke() {
        return $this->session;
    }

}

?>
