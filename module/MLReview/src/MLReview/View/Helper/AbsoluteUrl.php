<?php

/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\View\Helper;

use Zend\Http\Request;
use Zend\View\Helper\AbstractHelper;

/**
 * A simple helper to display absolute url in web pages
 */
class AbsoluteUrl extends AbstractHelper {

    /**
     *
     * @var \Zend\Http\Request
     */
    protected $request;

    /**
     * Constructor
     * @param \Zend\Http\Request $req
     */
    public function __construct(Request $req) {
        $this->request = $req;
    }

    /**
     *
     * @return string Representing the absolute url of this page
     */
    public function __invoke() {
        return $this->request->getUri()->normalize();
    }

}

?>
