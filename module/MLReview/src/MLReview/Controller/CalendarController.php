<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Controller;

use Zend\View\Model\ViewModel;

/**
 * MVC framework. Controller for showing dates with activities.
 */
class CalendarController extends BaseController {

    /**
     * Base action for calendar.
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        return new ViewModel();
    }

    /**
     * Important action to actual show activities based on dates.
     * @return \Zend\View\Model\ViewModel
     */
    public function showAction() {
        //Get the date sought after
        $today_date = $this->params()->fromQuery('date');
        if (empty($today_date)) {
            return $this->redirect()->toRoute('home');
        }
        $generator = $this->getServiceLocator()->get('DisplayDateGenerator');
        $generator->setToday($today_date);
        $array = $generator->generate();

        return new ViewModel(
                $array
        );
    }

}

?>
