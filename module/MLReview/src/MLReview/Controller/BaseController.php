<?php

/*
 * This file is part of the Memory Lane Review Client.
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * Abstract class to be able to get the entity manager for all other controllers
 */
class BaseController extends AbstractActionController {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * Get the entity manager
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager() {
        if (null === $this->entityManager) {
            $this->entityManager = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        }
        return $this->entityManager;
    }

}

?>
