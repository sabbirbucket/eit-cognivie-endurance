<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Controller;

use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use MLReview\Service\Adapter\PostAdapterAbstract;
use MLReview\Service\Adapter\MLClusterAdapter;

/**
 * MVC framework. Controller for the overview page.
 *
 */
class IndexController extends BaseController {

    /**
     * First action called when website is loaded.
     * Handles all the sorting method calls defined in configuration file local.php
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {

        //Check to see if we have any sorting methods we like to use
        $config = $this->serviceLocator->get('Config');
        $all_methods = $config['sortingMethods'];
        $error = $this->params()->fromPost('error');
        if (empty($error)) {
            $error = $this->params()->fromQuery('error');
        }

        //We got posted data
        if ($this->request->isPost() || !empty($error)) {

            //Check if there are more methods in line to be used
            $session = new Container('user');
            $in_queue_methods = $session->sortingMethod;

            if (!empty($error)) {
                //Got an error from this sorting method
                //Just call next method in queue instead
                if (!empty($in_queue_methods)) {
                    //There are more methods in line! Call them
                    $this->nextMethod($in_queue_methods);
                }
            } else {
                //No error
                //Count all sorting methods
                $total_methods_count = \count($all_methods);
                $current_methods_count = \count($in_queue_methods);

                //What adapter to create
                $index_at = ($total_methods_count - $current_methods_count - 1);
                $currentMethod = \current(\array_slice($all_methods, $index_at, 1));
                $adapterName = $currentMethod['adapterName'];

                //Create the adapter
                $adapter = $this->serviceLocator->get($adapterName);

                if (!empty($adapter)) {
                    //Attach the filters if any
                    $filters = $currentMethod['filters'];
                    if (!empty($filters)) {
                        foreach ($filters as $filterName => $filterOptions):
                            $filterObject = $this->serviceLocator->get($filterName);
                            $filterObject->setOptions($filterOptions);
                            $adapter->addFilter($filterObject);
                        endforeach;
                    }

                    if ($adapter instanceof PostAdapterAbstract) {
                        //A PostAdapter handles posted data
                        $post = $this->getRequest()->getPost()->toArray();

                        $adapter->setPost($post);
                        //The actual work for the adapter
                        $adapter->convert();
                        //Filters will be applied automatically
                        $old_activities = $adapter->getOldActivities();
                        $this->saveActivities($old_activities);
                        //Filters will be applied automatically
                        $new_activities = $adapter->getActivities();
                        $this->saveActivities($new_activities);
                        if ($adapter instanceof MLClusterAdapter) {
                            //Do some cleanup
                            $assetUtil = $this->serviceLocator->get('AssetUtil');
                            $public_path = $assetUtil->getPublicPath();
                            $files = $currentMethod['queryOptions'];
                            $gps = $public_path . $files['gps_source'];
                            $bt = $public_path . $files['bt_source'];
                            $mediaimage = $public_path . $files['mediaimage_source'];
                            if (file_exists($gps)) {
                                $assetUtil::deleteFile($gps);
                            }
                            if (file_exists($bt)) {
                                $assetUtil::deleteFile($bt);
                            }
                            if (file_exists($mediaimage)) {
                                $assetUtil::deleteFile($mediaimage);
                            }
                        }
                    } else {
                        //Some other adapter
                    }
                }

                //Check to see if you should call other methods
                if (!empty($in_queue_methods)) {
                    //There are more methods in line! Call them
                    $this->nextMethod($in_queue_methods);
                }
            }
        } else {
            //Not a regular post, start a new queue!
            $this->nextMethod($all_methods);
        }

        $generator = $this->getServiceLocator()->get('DisplayDateGenerator');
        $array = $generator->generate();

        //TODO: If we got an GET-parameter like /?error=true, we need to remove this. But how?

        $viewModel = new ViewModel(
                $array
        );

        //Not regular view, redirects to 'ml-review/calendar/show'
        return $viewModel;
    }

    /**
     * Immediately closes the current browser window
     * @return \Zend\View\Model\ViewModel
     */
    public function closeAction() {
        return new ViewModel();
    }

    /**
     * Immediately closes the current browser window and refreshes the parent window
     * Used by pop-ups
     * @return \Zend\View\Model\ViewModel
     */
    public function closerefreshAction() {
        return new ViewModel();
    }

    /**
     * Help function to call the activity sort methods.
     * Gets the next method in queue according to the config file
     * @param array $methods The methods to call
     */
    private function nextMethod($methods) {
        if (!empty($methods)) {
            //Extract the first method of the Sorting Algorithms
            //Also removes the method from the list
            $first_method = array_shift($methods);
            //Route destination of this first method
            $route = $first_method['route'];
            //Params for example which action
            $params = $first_method['params'];
            if (null === $params) {
                $params = array();
            }

            //Options (if any) for this first method
            $query_options = $first_method['queryOptions'];
            //What address to post to
            $post_to = $first_method['postTo'];
            if (!empty($route)) {
                //Pass to the correct forward
                $query = array();
                if (!empty($query_options)) {
                    $query["options"] = $query_options;
                }
                if (!empty($post_to)) {
                    $query["postTo"] = $post_to;
                }
                //Save the next method_config in session
                $session = new Container('user');
                $session->sortingMethod = $methods;

                //Call the current sorting method
                $this->redirect()->toRoute(
                        $route, $params, array("query" => $query));
            }
        }
    }

    /**
     * Permanent save the activities persisting them in the database
     * @param type $activities
     * @return null
     */
    private function saveActivities($activities) {
        if (empty($activities)) {
            return null;
        }
        foreach ($activities as $activity):
            //Persist activity data
            $this->getEntityManager()->persist($activity);
            $this->getEntityManager()->flush();
        endforeach;
    }

}

