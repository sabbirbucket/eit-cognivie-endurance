<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Controller;

use Zend\View\Model\ViewModel;

/**
 * MVC framework. Controller for showing dates with activities.
 */
class UploadController extends BaseController {

    /**
     * Base action for calendar.
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        return new ViewModel();
    }

    /**
     * Important action to actual show activities based on dates.
     * @return \Zend\View\Model\ViewModel
     */
  public function callbackAction() {
      return new ViewModel();
    }
    
     public function updateAction() {
      //$this->view->mlreviewuploadxmldata->update()->scriptTags = '<script src="load.js"></script>';
      return new ViewModel();
    }
      public function dataparserAction() {
      //return new ViewModel();
      $this->layout('layout/empty');
    }
    
       public function sendAction() {
       $this->layout('layout/empty');

    }
    
    public function postAction() {
       $this->layout('layout/empty');

    }
    
    public function logdirAction(){
       $this->layout('layout/empty');
    }
    
    public function downloadAction(){
       $this->layout('layout/empty');
    }
}

?>
