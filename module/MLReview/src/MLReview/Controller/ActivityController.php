<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Controller;

use Zend\View\Model\ViewModel;
use MLReview\Form\ActivityForm;
use MLReview\Form\MediaUploadForm;
use MLReview\Entity\Activity;

/**
 * MVC framework. Controller for the activity pages.
 *
 */
class ActivityController extends BaseController {

    /**
     * The base action. Not used very much.
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        return new ViewModel();
    }

    /**
     * Add a brand new activity.
     * @return \Zend\View\Model\ViewModel
     */
    public function addAction() {

        //Create the form
        $form = new ActivityForm($this->serviceLocator);

        //Create a new, empty entity and bind it to the form
        $activity = new Activity();
        $form->bind($activity);

        //Set images to default one
        $config = $this->serviceLocator->get('Config');
        $activity_image = $config['default']['activity_image'];
        $form->get('activity')->get('src')->setValue($activity_image);

        //Set correct onclick
        $set_place = $form->get('set_place');
        $set_place->setAttribute('onclick', "open_info_window('" .
                $this->url()->fromRoute('activity', array('action' => 'setplace')) .
                "','');return false");
        $add_person = $form->get('add_persons');
        $add_person->setAttribute('onclick', "open_info_window('" .
                $this->url()->fromRoute('activity', array('action' => 'setperson')) .
                "','');return false");
        $representative = $form->get('representative');
        $representative->setAttribute('onclick', "open_info_window('" .
                $this->url()->fromRoute('activity', array('action' => 'setrepresentative')) .
                "','');return false");

        if ($this->request->isPost()) {
            $request = $this->getRequest();
            // Make certain to merge the files info!
            $post = array_merge_recursive(
                    $request->getPost()->toArray(), $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                //Persist activity data
                $this->getEntityManager()->persist($activity);
                $this->getEntityManager()->flush();

                //Do some cleanup
                $this->postPersist($activity);

                //var_dump($activity->getMedias());
                //Done. Close this window
                return $this->redirect()->toRoute('home');
            }
        }

        //We are adding a activity
        $form->setAttribute('action', $this->url()->fromRoute('activity', array('action' => 'add')));

        //Adding media as well
        $media_form = new MediaUploadForm($this->serviceLocator);
        $media_form->setAttribute('action', $this->url()->fromRoute('activity', array('action' => 'setmedia')));
        $media_form->setAttribute('method', 'post');
        $media_form->setAttribute('id', 'media_form');

        return new ViewModel(array(
            'form' => $form,
            'media_form' => $media_form,
            'view_only' => false
        ));
    }

    /**
     * Shows all places that can be chosen for an activity
     * @return \Zend\View\Model\ViewModel
     */
    public function setPlaceAction() {

        //Switch layout to the simple one instead of standard layout
        $this->layout()->setTemplate('simple/layout');

        return new ViewModel(array(
            //TODO: change this to ONLY search for a specific user's persons->findAll
            //idea: can one change the database view only, and keep all code intact?
            'places' => $this->getEntityManager()->getRepository('MLReview\Entity\Place')->findAll()
        ));
    }

    /**
     * Shows which specific persons that can be chosen for an activity
     * @return \Zend\View\Model\ViewModel
     */
    public function setPersonAction() {

        //Switch layout to the simple one instead of standard layout
        $this->layout()->setTemplate('simple/layout');

        return new ViewModel(array(
            //TODO: change this to ONLY search for a specific user's persons->findAll
            //idea: can one change the database view only, and keep all code intact?
            'persons' => $this->getEntityManager()->getRepository('MLReview\Entity\Person')->findAll()
        ));
    }

    /**
     * Shows what images can be set as the representative image for an activity
     * @return \Zend\View\Model\ViewModel
     */
    public function setRepresentativeAction() {

        //Switch layout to the simple one instead of standard layout
        $this->layout()->setTemplate('simple/layout');

        return new ViewModel(array(
                //nothing to pass
        ));
    }

    /**
     * Uploads a temporary image (media) for an activity
     * @return Zend\Http\PhpEnvironment\Response Json encoding read by the caller (viewing webpage)
     */
    public function setMediaAction() {
        //Create the form
        $form = new MediaUploadForm($this->serviceLocator);

        $request = $this->getRequest();
        $response = $this->getResponse();

        if ($request->isPost()) {

            // Make certain to merge the files info!
            $post = array_merge_recursive(
                    $request->getPost()->toArray(), $request->getFiles()->toArray()
            );
            $form->setData($post);

            $fileErrors = $form->get('upload')->get('file')->getMessages();
            $temp_image_path = null;
            if (empty($fileErrors)) {
                $assetUtil = $this->getServiceLocator()->get('AssetUtil');
                $file = $form->get('upload')->get('file')->getValue();
                $temp_image_path = $assetUtil->uploadTempImage($file);
            }

            if (empty($temp_image_path))
                $response->setContent(\Zend\Json\Json::encode(array('response' => false)));
            else {
                $response->setContent(\Zend\Json\Json::encode(array('response' => true, 'image_path' => $temp_image_path)));
            }
        }
        return $response;
    }

    /**
     * View the media of an activity
     * @return \Zend\View\Model\ViewModel
     */
    public function viewMediaAction() {

        $id = (int) $this->params()->fromRoute('id', -1);
        if ($id < 0) {
            return $this->redirect()->toRoute('close');
        }

        //Switch layout to the simple one instead of standard layout
        $this->layout()->setTemplate('simple/layout');

        return new ViewModel(array(
            'id' => $id
        ));
    }

    /**
     * View a specific activity
     * @return \Zend\View\Model\ViewModel
     */
    public function viewAction() {
        //Create the form
        $form = new ActivityForm($this->serviceLocator);

        //Set correct label
        $form->get('activity')->setLabel(\MLReview\Util\Translator::translate('View Activity'));

        //Fetch the current data of this activity
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('home');
        }
        $activity = $this->getEntityManager()->getRepository('MLReview\Entity\Activity')->find($id);
        if (!$activity) {
            return $this->redirect()->toRoute('home');
        }

        $form->bind($activity);

        //Iterate and disable all elements of the activity
        $activityFieldset = $form->get('activity');
        foreach ($activityFieldset as $element):
            $element->setAttribute('disabled', 'true');
        endforeach;

        //Change buttons to viewing
        $activityId = $activity->getActivityId();
        $yes_button = $form->get('submit-yes');
        $yes_button->setLabel(\MLReview\Util\Translator::translate('Edit'));
        $yes_button->setAttribute('onclick', "window.open('" .
                $this->url()->fromRoute('activity', array('action' => 'edit', 'id' => $activityId))
                . "', '_self')");
        $no_button = $form->get('submit-no');
        $no_button->setLabel(\MLReview\Util\Translator::translate('Delete'));
        $no_button->setAttribute('onclick', "open_info_window('" .
                $this->url()->fromRoute('activity', array('action' => 'delete', 'id' => $activityId))
                . "', '')");

        //Set correct onclick
        $place = $form->get('activity')->get('place');
        $placeId = $place->get('placeId')->getValue();
        $set_place = $form->get('set_place');
        //$set_place->setAttribute('src', $placeSrc);
        $set_place->setAttribute('onclick', "open_info_window('" .
                $this->url()->fromRoute('place', array('action' => 'view', 'id' => $placeId)) .
                "','');return false");
        $set_place->setLabel(\MLReview\Util\Translator::translate('Place'));
        $add_person = $form->get('add_persons');
        $add_person->setAttribute('hidden', 'true');
        $add_person->setLabel(\MLReview\Util\Translator::translate('Persons'));
        $representative = $form->get('representative');
        $representative->setAttribute('onclick', 'return false;');
        $representative->setAttribute('disabled', 'true');

        //Can not upload images or review images when viewing
        $form->remove('upload');
        $form->remove('select_all_button');
        $form->remove('deselect_all_button');

        //Set the media image src to be same as hidden src
        $form->get('activity')->setMediaImageToSrc();

        $src = $activity->getSrc();

        $representative->setAttribute('src', $src);

        //We are viewing an activity
        $form->setAttribute('action', $this->url()->fromRoute('activity', array('action' => 'view', 'id' => $id)));

        return new ViewModel(array(
            'form' => $form,
            'view_only' => true
        ));
    }

    /**
     * Edit a specific activity
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction() {

        //Create the form
        $form = new ActivityForm($this->serviceLocator);

        //Set correct label
        $form->get('activity')->setLabel(\MLReview\Util\Translator::translate('Edit Activity'));

        //Fetch the current data of this activity
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('home');
        }
        $activity = $this->getEntityManager()->getRepository('MLReview\Entity\Activity')->find($id);
        if (!$activity) {
            return $this->redirect()->toRoute('home');
        }

        $form->bind($activity);

        //Change buttons to viewing
        //$activityId = $activity->getActivityId();
        $yes_button = $form->get('submit-yes');
        $yes_button->setLabel(\MLReview\Util\Translator::translate('Save'));
        $yes_button->setAttribute('onclick', "special_submit(this.form);");
        $no_button = $form->get('submit-no');
        $no_button->setLabel(\MLReview\Util\Translator::translate('Exit'));
        $no_button->setAttribute('onclick', "window.open('" .
                $this->url()->fromRoute('home')
                . "', '_self')");

        //Set correct onclick
        $set_place = $form->get('set_place');
        $set_place->setAttribute('onclick', "open_info_window('" .
                $this->url()->fromRoute('activity', array('action' => 'setplace')) .
                "','');return false");
        $add_person = $form->get('add_persons');
        $add_person->setAttribute('onclick', "open_info_window('" .
                $this->url()->fromRoute('activity', array('action' => 'setperson')) .
                "','');return false");
        $representative = $form->get('representative');
        $representative->setAttribute('onclick', "open_info_window('" .
                $this->url()->fromRoute('activity', array('action' => 'setrepresentative')) .
                "','');return false");


        if ($this->request->isPost()) {
            $request = $this->getRequest();
            // Make certain to merge the files info!
            $post = array_merge_recursive(
                    $request->getPost()->toArray(), $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                //This activity is now reviewed
                $activity->setReviewed(true);
                //Persist activity data
                $this->getEntityManager()->persist($activity);
                $this->getEntityManager()->flush();

                //Do some cleanup
                $this->postPersist($activity);

                //var_dump($activity);
                //Done. Close this window
                return $this->redirect()->toRoute('activity', array('action' => 'view', 'id' => $id));
            }
        }

        $src = $activity->getSrc();
        $representative->setAttribute('src', $src);

        //Set the media image src to be same as hidden src
        $form->get('activity')->setMediaImageToSrc();

        //We are adding a activity
        $form->setAttribute('action', $this->url()->fromRoute('activity', array('action' => 'edit', 'id' => $id)));

        //Adding media as well
        $media_form = new MediaUploadForm($this->serviceLocator);
        $media_form->setAttribute('action', $this->url()->fromRoute('activity', array('action' => 'setmedia')));
        $media_form->setAttribute('method', 'post');
        $media_form->setAttribute('id', 'media_form');

        return new ViewModel(array(
            'form' => $form,
            'media_form' => $media_form,
            'view_only' => false
        ));
    }

    /**
     * Delete an activity
     * @return \Zend\View\Model\ViewModel
     */
    public function deleteAction() {

        //Use another layout than the standard one
        $this->layout()->setTemplate('simple/layout');

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('close');
        }

        $activity = $this->getEntityManager()->getRepository('MLReview\Entity\Activity')->find($id);
        if (!$activity) {
            return $this->redirect()->toRoute('close');
        }

        //Create the form
        $form = new ActivityForm($this->serviceLocator);
        //Change label to deleting
        $form->get('activity')->setLabel('Are you sure you want to delete this activity?');

        $request = $this->getRequest();

        if ($request->isPost()) {

            $form->setData($request->getPost());

            //Special in this case means that we have clicked 'yes'
            $special = $form->get('specialFlag')->getValue();
            if (!empty($special)) {
                //Our Entity Listener will make suer that uploaded images gets deleted as well
                $this->getEntityManager()->remove($activity);
                $this->getEntityManager()->flush();

                //Done. Close this window
                return $this->redirect()->toRoute('closerefresh');
            }
        }

        //We are deleting an activity
        $form->setAttribute('action', $this->url()->fromRoute('activity', array('action' => 'delete', 'id' => $id)));

        return new ViewModel(array(
            'form' => $form,
            'id' => $id
        ));
    }

    /**
     * Helper to make sure Activity src is a valid src
     * @param \MLReview\Entity\Activity $activity
     * @return type
     */
    private function postPersist(\MLReview\Entity\Activity $activity) {
        $assetUtil = $this->getServiceLocator()->get('AssetUtil');
        $public_path = $assetUtil->getPublicPath();
        //Get current src of this activity
        $src = $activity->getSrc();
        //Check if src of activity exists
        if (file_exists($public_path . $src)) {
            //no worries, file exists
            return;
        }
        $src_array = explode('/', $src);
        $last_part_src = end($src_array);
        foreach ($activity->getMedias() as $media):
            $media_src = $media->getSrc();
            $media_src_array = explode('/', $media_src);
            $last_part_media_src = end($media_src_array);
            if ($last_part_media_src === $last_part_src) {
                //Found the media, but wrong path!
                //Change activity src to media src
                $activity->setSrc($media_src);
                //Persist activity data again
                $this->getEntityManager()->persist($activity);
                $this->getEntityManager()->flush();
                return;
            }
        endforeach;
        //Could not find the old src, and no media carry src
        //Set to default value and persist
        $config = $this->getServiceLocator()->get('Config');
        $activity_image = $config['default']['activity_image'];
        $activity->setSrc($activity_image);
        //Persist activity data again
        $this->getEntityManager()->persist($activity);
        $this->getEntityManager()->flush();
    }

}

?>
