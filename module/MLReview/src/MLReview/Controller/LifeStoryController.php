<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Controller;

use Zend\View\Model\ViewModel;

/**
 * MVC framework. Controller for the life story page.
 */
class LifeStoryController extends BaseController {

    /**
     * Shows all story categories and their belonging activities.
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        //All story categories in the database
        $storyCategories = $this->getEntityManager()->getRepository('MLReview\Entity\StoryCategory')->findAll();
        $activityArray = array();
        foreach ($storyCategories as $category) {
            $categoryId = $category->getStoryCategoryId();
            //Find all the activities belonging to this storycategory
            //TODO: Only the ones belonging to this user!
            $activity = $this->getEntityManager()->getRepository('MLReview\Entity\Activity')
                    ->findBy(array('storyCategory' => $category));
            $activityArray[$categoryId] = $activity;
        }
        return new ViewModel(array(
            //All story categories in the database
            'storyCategories' => $storyCategories,
            'activityArray' => $activityArray,
        ));
    }

}

?>
