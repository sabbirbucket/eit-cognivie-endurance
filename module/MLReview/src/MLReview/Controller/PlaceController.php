<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Controller;

use Zend\View\Model\ViewModel;
use MLReview\Form\PlaceForm;
use MLReview\Entity\Place;

/**
 * MVC framework. Controller for the place.
 */
class PlaceController extends BaseController {

    /**
     * View all available places
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        return new ViewModel(array(
            //TODO: change this to ONLY search for a specific user's places->findAll
            //idea: can one change the database view only, and keep all code intact?
            'places' => $this->getEntityManager()->getRepository('MLReview\Entity\Place')->findAll()
        ));
    }

    /**
     * View a single place for later update/deletion
     * @return \Zend\View\Model\ViewModel
     */
    public function viewAction() {
        //Switch layout to the simple one instead of standard layout
        $this->layout()->setTemplate('simple/layout');

        //Check if we should refresh the parent page or not
        $refresh_on_close = $this->params()->fromQuery('refresh_on_close');

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('closerefresh');
        }

        $place = $this->getEntityManager()->getRepository('MLReview\Entity\Place')->find($id);

        //Not found, close window
        if ($place == null) {
            return $this->redirect()->toRoute('closerefresh');
        }

        return new ViewModel(array(
            'place' => $place,
            'refresh_on_close' => $refresh_on_close
        ));
    }

    /**
     * Add a new place
     * @return \Zend\View\Model\ViewModel
     */
    public function addAction() {
        //Use another layout than the standard one
        $this->layout()->setTemplate('simple/layout');

        //Create the form
        $form = new PlaceForm($this->serviceLocator);

        //Create a new, empty entity and bind it to the form
        $place = new Place();
        $form->bind($place);

        //Set image to default one
        $config = $this->serviceLocator->get('Config');
        $place_image = $config['default']['place_image'];
        $form->get('tempImage')->setValue($place_image);

        if ($this->request->isPost()) {
            $assetUtil = $this->getServiceLocator()->get('AssetUtil');
            $isValid = $assetUtil->imageFormHelper($this->getRequest(), $form, 'place');
            if ($isValid) {
                //Set the place src to this saved image file
                $place->setSrc($form->get('place')->get('src')->getValue());

                //Persist place data
                $this->getEntityManager()->persist($place);
                $this->getEntityManager()->flush();
                //Done. Close this window
                return $this->redirect()->toRoute('closerefresh');
            }
        }

        //We are adding a place
        $form->setAttribute('action', $this->url()->fromRoute('place', array('action' => 'add')));

        return new ViewModel(array(
            'form' => $form
        ));
    }

    /**
     * Edit an already known place
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction() {

        //Use another layout than the standard one
        $this->layout()->setTemplate('simple/layout');

        //Create the form
        $form = new PlaceForm($this->serviceLocator);

        //Fetch the place's current data
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('close');
        }
        $place = $this->getEntityManager()->getRepository('MLReview\Entity\Place')->find($id);
        if (!$place) {
            return $this->redirect()->toRoute('close');
        }
        $form->bind($place);

        //Change label to editing
        $form->get('place')->setLabel('Edit place');
        $form->get('upload')->get('upload-button')->setLabel('Change Image');

        //Set temp image to the one we got from the database!
        $place_image = $form->get('place')->get('src')->getValue();
        $form->get('tempImage')->setValue($place_image);

        if ($this->request->isPost()) {
            $assetUtil = $this->getServiceLocator()->get('AssetUtil');
            $isValid = $assetUtil->imageFormHelper($this->getRequest(), $form, 'place');
            if ($isValid) {
                //Set the place src to this saved image file
                $place->setSrc($form->get('place')->get('src')->getValue());

                //Persist place data
                $this->getEntityManager()->persist($place);
                $this->getEntityManager()->flush();
                //Done. Close this window
                return $this->redirect()->toRoute('closerefresh');
            }
        }

        //We are editing a place
        $form->setAttribute('action', $this->url()->fromRoute('place', array('action' => 'edit', 'id' => $id)));

        return new ViewModel(array(
            'form' => $form
        ));
    }

    /**
     * Delete a place
     * @return \Zend\View\Model\ViewModel
     */
    public function deleteAction() {

        //Use another layout than the standard one
        $this->layout()->setTemplate('simple/layout');

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('close');
        }

        $place = $this->getEntityManager()->getRepository('MLReview\Entity\Place')->find($id);
        if (!$place) {
            return $this->redirect()->toRoute('close');
        }

        //Create the form
        $form = new PlaceForm($this->serviceLocator);
        //Change label to deleting
        $form->get('place')->setLabel('Are you sure you want to delete this place?');

        $request = $this->getRequest();

        if ($request->isPost()) {

            $form->setData($request->getPost());

            //Special in this case means that we have clicked 'yes'
            $special = $form->get('specialFlag')->getValue();
            if (!empty($special)) {
                $assetUtil = $this->getServiceLocator()->get('AssetUtil');
                $public_path = $assetUtil->getPublicPath();
                $image_path = $public_path . $place->getSrc();
                $this->getEntityManager()->remove($place);
                $this->getEntityManager()->flush();
                //Also delete the image src
                try {
                    if ($assetUtil->isUploadedFile($image_path)) {
                        //Only delete if uploaded file
                        $assetUtil::deleteFile($image_path);
                    }
                } catch (\Exception $e) {
                    //TODO: Could not delete file. Log this and continue.
                }
                //Done. Close this window
                return $this->redirect()->toRoute('closerefresh');
            }
        }

        //We are deleting a place
        $form->setAttribute('action', $this->url()->fromRoute('place', array('action' => 'delete', 'id' => $id)));
        return new ViewModel(array(
            'form' => $form,
            'id' => $id
        ));
    }

}

?>
