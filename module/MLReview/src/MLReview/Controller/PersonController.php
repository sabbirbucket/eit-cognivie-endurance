<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Controller;

use Zend\View\Model\ViewModel;
use MLReview\Form\PersonForm;
use MLReview\Entity\Person;

/**
 * MVC framework. Controller for the person.
 */
class PersonController extends BaseController {

    /**
     * View all available persons
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        return new ViewModel(array(
            //TODO: change this to ONLY search for a specific user's persons->findAll
            //idea: can one change the database view only, and keep all code intact?
            'persons' => $this->getEntityManager()->getRepository('MLReview\Entity\Person')->findAll()
        ));
    }

    /**
     * View a single person for later update/deletion
     * @return \Zend\View\Model\ViewModel
     */
    public function viewAction() {

        //Switch layout to the simple one instead of standard layout
        $this->layout()->setTemplate('simple/layout');

        //Check if we should refresh the parent page or not
        $refresh_on_close = $this->params()->fromQuery('refresh_on_close');

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('closerefresh');
        }

        $person = $this->getEntityManager()->getRepository('MLReview\Entity\Person')->find($id);

        //Not found, close window
        if ($person == null) {
            return $this->redirect()->toRoute('closerefresh');
        }

        return new ViewModel(array(
            'person' => $person,
            'refresh_on_close' => $refresh_on_close
        ));
    }

    /**
     * Add a new person
     * @return \Zend\View\Model\ViewModel
     */
    public function addAction() {

        //Use another layout than the standard one
        $this->layout()->setTemplate('simple/layout');

        //Create the form
        $form = new PersonForm($this->serviceLocator);

        //Create a new, empty entity and bind it to the form
        $person = new Person();
        $form->bind($person);

        //Set image to default one
        $config = $this->serviceLocator->get('Config');
        $person_image = $config['default']['person_image'];
        $form->get('tempImage')->setValue($person_image);

        if ($this->request->isPost()) {
            $assetUtil = $this->getServiceLocator()->get('AssetUtil');
            $isValid = $assetUtil->imageFormHelper($this->getRequest(), $form, 'person');
            if ($isValid) {
                //Set the person src to this saved image file
                $person->setSrc($form->get('person')->get('src')->getValue());

                //Persist person data
                $this->getEntityManager()->persist($person);
                $this->getEntityManager()->flush();
                //Done. Close this window
                return $this->redirect()->toRoute('closerefresh');
            }
        }

        //We are adding a person
        $form->setAttribute('action', $this->url()->fromRoute('person', array('action' => 'add')));

        return new ViewModel(array(
            'form' => $form
        ));
    }

    /**
     * Edit an already known person
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction() {

        //Use another layout than the standard one
        $this->layout()->setTemplate('simple/layout');

        //Create the form
        $form = new PersonForm($this->serviceLocator);

        //Fetch the person's current data
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('close');
        }
        $person = $this->getEntityManager()->getRepository('MLReview\Entity\Person')->find($id);
        if (!$person) {
            return $this->redirect()->toRoute('close');
        }
        $form->bind($person);

        //Change label to editing
        $form->get('person')->setLabel('Edit person');
        $form->get('upload')->get('upload-button')->setLabel('Change Image');

        //Set temp image to the one we got from the database!
        $person_image = $form->get('person')->get('src')->getValue();
        $form->get('tempImage')->setValue($person_image);

        if ($this->request->isPost()) {
            $assetUtil = $this->getServiceLocator()->get('AssetUtil');
            $isValid = $assetUtil->imageFormHelper($this->getRequest(), $form, 'person');
            if ($isValid) {
                //Set the person src to this saved image file
                $person->setSrc($form->get('person')->get('src')->getValue());

                //Persist person data
                $this->getEntityManager()->persist($person);
                $this->getEntityManager()->flush();
                //Done. Close this window
                return $this->redirect()->toRoute('closerefresh');
            }
        }

        //We are editing a person
        $form->setAttribute('action', $this->url()->fromRoute('person', array('action' => 'edit', 'id' => $id)));

        return new ViewModel(array(
            'form' => $form
        ));
    }

    /**
     * Delete a person
     * @return \Zend\View\Model\ViewModel
     */
    public function deleteAction() {
        //Use another layout than the standard one
        $this->layout()->setTemplate('simple/layout');

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('close');
        }

        $person = $this->getEntityManager()->getRepository('MLReview\Entity\Person')->find($id);
        if (!$person) {
            return $this->redirect()->toRoute('close');
        }

        //Create the form
        $form = new PersonForm($this->serviceLocator);
        //Change label to deleting
        $form->get('person')->setLabel('Are you sure you want to delete this person?');

        $request = $this->getRequest();

        if ($request->isPost()) {

            $form->setData($request->getPost());

            //Special in this case means that we have clicked 'yes'
            $special = $form->get('specialFlag')->getValue();
            if (!empty($special)) {
                $assetUtil = $this->getServiceLocator()->get('AssetUtil');
                $public_path = $assetUtil->getPublicPath();
                $image_path = $public_path . $person->getSrc();
                $this->getEntityManager()->remove($person);
                $this->getEntityManager()->flush();
                //Also delete the image src
                try {
                    if ($assetUtil->isUploadedFile($image_path)) {
                        //Only delete if uploaded file
                        $assetUtil::deleteFile($image_path);
                    }
                } catch (\Exception $e) {
                    //TODO: Could not delete file. Log this and continue.
                }
                //Done. Close this window
                return $this->redirect()->toRoute('closerefresh');
            }
        }

        //We are deleting a person
        $form->setAttribute('action', $this->url()->fromRoute('person', array('action' => 'delete', 'id' => $id)));
        return new ViewModel(array(
            'form' => $form,
            'id' => $id
        ));
    }

}

?>
