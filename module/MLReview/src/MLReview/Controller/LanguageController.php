<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;

/**
 * MVC framework. Controller for changing user language.
 */
class LanguageController extends AbstractActionController {

    /**
     * Change the language setting for this user.
     * Uses a session to save data.
     * @return \Zend\View\Model\ViewModel
     */
    public function changeAction() {

        if ($this->request->isPost()) {
            $data = $this->getRequest()->getPost()->toArray();
            $session_language = $data['language'];
            $current_page = $data['current_route'];

            //Set the new language in the user session
            $user_session = new Container('user');
            $user_session->language = $session_language;

            return $this->redirect()->toUrl($current_page);
        }

        //Something is wrong. Redirect to home
        return $this->redirect()->toRoute('home');
    }

}
