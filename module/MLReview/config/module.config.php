<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview;

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'MLReview\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'close' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/close',
                    'defaults' => array(
                        'controller' => 'MLReview\Controller\Index',
                        'action' => 'close',
                    ),
                ),
            ),
            'closerefresh' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/closerefresh',
                    'defaults' => array(
                        'controller' => 'MLReview\Controller\Index',
                        'action' => 'closerefresh',
                    ),
                ),
            ),
            'calendar' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/calendar[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'MLReview\Controller\Calendar',
                        'action' => 'index',
                    ),
                ),
            ),
            'activity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/activity[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'MLReview\Controller\Activity',
                        'action' => 'add',
                    ),
                ),
            ),
            'lifestory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/lifestory[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'MLReview\Controller\LifeStory',
                        'action' => 'index',
                    ),
                ),
            ),
            'person' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/person[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'MLReview\Controller\Person',
                        'action' => 'index',
                    ),
                ),
            ),
            'place' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/place[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'MLReview\Controller\Place',
                        'action' => 'index',
                    ),
                ),
            ),
            
            'uploadxmldata' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/uploadxmldata',
                       'defaults' => array(
                        'controller' => 'MLReview\Controller\Upload',
                        'action' => 'index',
                    ),

                ),
            ),
            
             'callback' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/uploadxmldata/callback',
                       'defaults' => array(
                        'controller' => 'MLReview\Controller\Upload',
                        'action' => 'callback',
                    ),

                ),
            ),
            
            'update' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/uploadxmldata/update',
                       'defaults' => array(
                        'controller' => 'MLReview\Controller\Upload',
                        'action' => 'update',
                    ),

                ),
            ),
            
            'dataparser' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/uploadxmldata/dataparser',
                       'defaults' => array(
                        'controller' => 'MLReview\Controller\Upload',
                        'action' => 'dataparser',
                    ),

                ),
            ),
            'send' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/uploadxmldata/send',
                       'defaults' => array(
                        'controller' => 'MLReview\Controller\Upload',
                        'action' => 'send',
                    ),

                ),
            ),
            'post' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/uploadxmldata/post',
                       'defaults' => array(
                        'controller' => 'MLReview\Controller\Upload',
                        'action' => 'post',
                    ),

                ),
            ),
            'logdir' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/uploadxmldata/logdir',
                       'defaults' => array(
                        'controller' => 'MLReview\Controller\Upload',
                        'action' => 'logdir',
                    ),

                ),
            ),
            'download' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/uploadxmldata/download',
                       'defaults' => array(
                        'controller' => 'MLReview\Controller\Upload',
                        'action' => 'download',
                    ),

                ),
            ),
            'change_language' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/language',
                    'defaults' => array(
                        'controller' => 'MLReview\Controller\Language',
                        'action' => 'change',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'en_GB',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'MLReview\Controller\Index' => 'MLReview\Controller\IndexController',
            'MLReview\Controller\Calendar' => 'MLReview\Controller\CalendarController',
            'MLReview\Controller\Activity' => 'MLReview\Controller\ActivityController',
            'MLReview\Controller\LifeStory' => 'MLReview\Controller\LifeStoryController',
            'MLReview\Controller\Person' => 'MLReview\Controller\PersonController',
            'MLReview\Controller\Place' => 'MLReview\Controller\PlaceController',
            'MLReview\Controller\Upload' => 'MLReview\Controller\UploadController',
            'MLReview\Controller\Language' => 'MLReview\Controller\LanguageController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml', //used for standard web pages
            'simple/layout' => __DIR__ . '/../view/layout/simple.phtml', //used for simple web pages
            'close/layout' => __DIR__ . '/../view/layout/close.phtml', //close current window
            'ml-review/index/index' => __DIR__ . '/../view/mlreview/calendar/index.phtml',
            'ml-review/index/close' => __DIR__ . '/../view/mlreview/index/close.phtml', //close current window
            'ml-review/index/closerefresh' => __DIR__ . '/../view/mlreview/index/closerefresh.phtml', //close current window, update parent
            'ml-review/index/overview' => __DIR__ . '/../view/mlreview/index/overview.phtml',
            'ml-review/activity/index' => __DIR__ . '/../view/mlreview/activity/index.phtml',
            'ml-review/activity/add' => __DIR__ . '/../view/mlreview/activity/addeditview.phtml',
            'ml-review/activity/view' => __DIR__ . '/../view/mlreview/activity/addeditview.phtml',
            'ml-review/activity/edit' => __DIR__ . '/../view/mlreview/activity/addeditview.phtml',
            'ml-review/activity/delete' => __DIR__ . '/../view/mlreview/activity/delete.phtml',
            'ml-review/activity/setplace' => __DIR__ . '/../view/mlreview/activity/setplace.phtml',
            'ml-review/activity/setperson' => __DIR__ . '/../view/mlreview/activity/setperson.phtml',
            'ml-review/activity/setrepresentative' => __DIR__ . '/../view/mlreview/activity/setrepresentative.phtml',
            'ml-review/activity/viewmedia' => __DIR__ . '/../view/mlreview/activity/viewmedia.phtml',
            //'ml-review/activity/editmedia' => __DIR__ . '/../view/mlreview/activity/editmedia.phtml',
            'ml-review/life-story/index' => __DIR__ . '/../view/mlreview/lifestory/index.phtml',
            'ml-review/person/index' => __DIR__ . '/../view/mlreview/person/index.phtml',
            'ml-review/person/view' => __DIR__ . '/../view/mlreview/person/view.phtml',
            'ml-review/person/edit' => __DIR__ . '/../view/mlreview/person/addedit.phtml',
            'ml-review/person/add' => __DIR__ . '/../view/mlreview/person/addedit.phtml',
            'ml-review/person/delete' => __DIR__ . '/../view/mlreview/person/delete.phtml',
            'ml-review/place/index' => __DIR__ . '/../view/mlreview/place/index.phtml',
            'ml-review/place/view' => __DIR__ . '/../view/mlreview/place/view.phtml',
            'ml-review/place/edit' => __DIR__ . '/../view/mlreview/place/addedit.phtml',
            'ml-review/place/add' => __DIR__ . '/../view/mlreview/place/addedit.phtml',
            'ml-review/place/delete' => __DIR__ . '/../view/mlreview/place/delete.phtml',
            'ml-review/calendar/index' => __DIR__ . '/../view/mlreview/calendar/index.phtml',
            'ml-review/calendar/show' => __DIR__ . '/../view/mlreview/calendar/index.phtml',
            'ml-review/upload/index' => __DIR__ . '/../view/mlreview/uploadxmldata/index.phtml',
            'ml-review/upload/callback' => __DIR__ . '/../view/mlreview/uploadxmldata/callback.phtml',
            'ml-review/upload/update' => __DIR__ . '/../view/mlreview/uploadxmldata/update.phtml',
            'ml-review/upload/dataparser' => __DIR__ . '/../view/mlreview/uploadxmldata/dataparser.php',
            'ml-review/upload/send' => __DIR__ . '/../view/mlreview/uploadxmldata/send.php',
            'ml-review/upload/post' => __DIR__ . '/../view/mlreview/uploadxmldata/post.php',
            'ml-review/upload/logdir' => __DIR__ . '/../view/mlreview/uploadxmldata/logdir.php',
            'ml-review/upload/download' => __DIR__ . '/../view/mlreview/uploadxmldata/download.php',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Doctrine config
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/' . __NAMESPACE__ . '/Entity',
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
        'configuration' => array(
            'orm_default' => array(
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'driver' => 'orm_default',
            //'generate_proxies' => false,
            //'proxy_dir' => __DIR__ . '/../../../data/DoctrineORMModule/Proxy',
            //'proxy_namespace' => 'DoctrineORMModule\DoctrineProxy'
            )
        ),
    ),
    //Session config
    'session' => array(
        'remember_me_seconds' => 2419200,
        'use_cookies' => true,
        'cookie_httponly' => true,
    ),
);
