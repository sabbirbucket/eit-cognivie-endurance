��    E      D  a   l      �     �               *     3     D     S     k     ~     �     �     �  
   �  .   �  ,   �  +        F     K     T  
   a     l     r     x          �     �     �  
   �     �     �     �     �     �  
   �  &   �  	     
        *     B     U     [     `     c  
   p     {     �     �     �     �     �     �     �     �     �     �     	     		     	  	   ,	  	   6	  
   @	     K	     P	     Y	     \	     i	     w	     {	  &  �	     �     �     �  	   �     �               8     P     e     y     �     �  6   �  3   �  2        9     B     K     W     _     e     l     t  	   �     �     �     �  	   �     �     �  	   �     �     �  8   �  	   6     @     M     h     {     �     �     �  	   �  	   �     �     �     �     �     �     �                    &     4     :     N     a     m  
   y     �     �     �     �     �     �     �     +   
   6                            .   *   (                =   '           ,   !       9      B          3   <   "   8   4          &             >   ?          2   0                     7           C         %   @                     $                    D       E   -   	      ;       )          5   /         :           #   1         A               - No Life Story - A 404 error occurred Activities loaded Activity Activity Persons Activity Place Activity Representative Add a new activity Add a new person Add a new place Add persons Address Adult Life Are you sure you want to delete this activity? Are you sure you want to delete this person? Are you sure you want to delete this place? Back Calendar Change Image Child Life Clear Close Delete Description Edit Edit Activity Edit person Edit place End date Exit File to upload First name* From Goto slide Invalid date, must be yyyy-mm-dd HH:MM Last name Life Story Loading Activies Failed Loading activities Name* Next No Not Reviewed Older Life Overview Personal Information Persons Phone number Pick a person Pick a place Pick a representative Place Places Previous Representative Save Save this person? Save this place? Set Place Set place Start date Stop Subject* To Upload image View Activity Yes with Project-Id-Version: MLReview
POT-Creation-Date: 2013-08-03 20:52+0100
PO-Revision-Date: 2013-08-03 20:53+0100
Last-Translator: 
Language-Team: 
Language: Swedish
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;translate;setLabel
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: C:\workspace\MLReview\public
X-Poedit-SearchPath-1: C:\workspace\MLReview\module
X-Poedit-SearchPath-2: C:\workspace\MLReview\config
 - Ingen livshistoria - Ett fel har inträffat (404) Aktiviteter laddar Aktivitet Personer i akvititet Aktivitetsplats Aktivitetens representativ Lägg till ny aktivitet Lägg till ny person Lägg till ny plats Lägg till person Adress Vuxenliv Är du säker på att du vill ta bort denna aktivitet? Är du säker på att du vill ta bort denna person? Är du säker på att du vill ta bort denna plats? Tillbaka Kalender Ändra bild Barndom Rensa Stäng Ta bort Beskrivning Uppdatera Ändra Aktivitet Uppdatera person Uppdatera plats Slutdatum Stäng Fil att ladda upp Förnamn* Från Gå till bild Felaktigt datum. Måste vara på formen yyyy-mm-dd HH:MM Efternamn Livshistoria Kunde ej ladda aktiviteter Laddar aktiviteter Namn* Nästa Nej Icke Granskad Ålderdom Översikt Personlig information Personer Telefonnummer Välj person Välj plats Välj en representativ Plats Platser Föregående Representativ Spara Spara denna person? Spara denna plats? Välj plats Välj plats Startdatum Stopp Ämne* Till Ladda upp bild Visa Aktivitet Ja med 