<?php
//require 'database_cfg.php';
function insert_activities_into_db($sql)
{
    $host='localhost';
    $user='root';
    $passwd='';
    $dbname='MLReview';
    $existance_of_id=false;
    $link = mysqli_connect($host, $user, $passwd,$dbname);
    if (!$link) 
    { 
        die('Could not connect: ' . mysql_error()); 

    }
    else
    {
        $result = mysqli_multi_query($link,$sql);
    }
    mysqli_close($link);
}
//To identify wheather the activity id exist in database.....
function id_exists($activityId)
{
    $host='localhost';
    $user='root';
    $passwd='';
    $dbname='MLReview';
    $existance_of_id=false;
    $link = mysqli_connect($host, $user, $passwd,$dbname);
    if (!$link) 
    { 
        die('Could not connect: ' . mysql_error()); 

    }
    else
    {
           $sql = "SELECT * FROM  activity WHERE activityId=".$activityId;
           $result = mysqli_query($link,$sql);
           $existance_of_id = mysqli_num_rows($result) > 0 ? true : false;
    }
        
  mysqli_close($link);
  return $existance_of_id;
}

function pic_folder_exist_()
{
	$folder_name_=array();
	$output_dir="public/data/logsync/Logs/";
	if ($handle = opendir($output_dir)) 
	{
		while (false !== ($entry = readdir($handle))) 
                {
                    if ($entry != "." && $entry != "..") 
                    {	
                                    if(is_dir($output_dir.$entry))
                                    {
                                        array_push($folder_name_,$entry);
                                    }
                    }
                }
	} 
	return $folder_name_;
}
function pic_file_exist_($folder)
{
        $output_dir="public/data/logsync/Logs/".$folder."/";
	$allowedImgExts = array("gif", "jpeg", "jpg", "png");
	$files_name_=array();
	//$output_dir=$_SERVER['DOCUMENT_ROOT']."/MLReview/public/data/uploaded/post/";
	//$post_dir="public/data/uploaded/post/";
	
	if ($handle = opendir($output_dir)) 
	{
		while (false !== ($entry = readdir($handle))) 
	    {
	    	if ($entry != "." && $entry != "..") 
	        {
                        array_push($files_name_, $entry);
	        	
//				$getExt = explode(".", $entry);
//				if(in_array($getExt[1], $allowedImgExts))
//				{
//					$file_name_=$file_name_.$getExt[0].' ';
//				}
	        }
	    }
	} 
	return $files_name_;
}

//if exists delete XML files to prevent overwrite activities
function delete_XML_files()
{
    $output_dir="public/data/logsync/Logs/";
    $XML_gps=$output_dir."GPSlogTemp.xml";
    $XML_image=$output_dir."imagesTemp.xml";
    if (file_exists($XML_image)) 
    {
        unlink($XML_image);
    }
    if (file_exists($XML_gps)) 
    {
        unlink($XML_gps);
    }
    
}
//remove directory recursively
function recurseRmdir($dir) 
{
  $files = array_diff(scandir($dir), array('.','..'));
  foreach ($files as $file) 
  {
    (is_dir("$dir/$file")) ? recurseRmdir("$dir/$file") : unlink("$dir/$file");
  }
  return rmdir($dir);
}
$debug="";
require_once("OAuth.php");
require 'oauth_cfg.phtml';
$file_path='public/data/uploaded/post/' ;
$downloaded_file_path='public/data/uploaded/downloaded/' ;
$plaintext_method = new OAuthSignatureMethod_PLAINTEXT();
$test_consumer = new OAuthConsumer($config['consumerKey'], $config['consumerSecret'], NULL);
$acc_token = new OAuthConsumer($oauth_token, $tokenSecret, 1);
$url="https://healthlabs.ehv.campus.philips.com/resources/".$_REQUEST['resource'];
$method=$_REQUEST["method"];
$sig_method = $plaintext_method;

$echo_req = OAuthRequest::from_consumer_and_token($test_consumer, $acc_token,$method , $url);
$echo_req->sign_request($sig_method, $test_consumer, $acc_token);

if( isset($_REQUEST["file_name"])&& isset($_REQUEST["method"])&&$_REQUEST["method"]=="POST")
{
//    $file_name_array=preg_split('/#/', $_REQUEST["file_name"]);
//    $file_name_without_time_stamp=pathinfo($file_name_array[1]);
//    $resource=$file_name_without_time_stamp['filename'];
//    $url=$url.$resource;
    //print_r($echo_req);
        
    error_reporting(-1);
    ini_set('display_errors', true);
    
    $header= array(   "Accept: application/json",
		                  "Content-type: application/json",
		                  $echo_req->to_header("")
		   );
    
    
    
    $file_content = file_get_contents($file_path.$_REQUEST['file_name']);
//    $file_content = substr(substr($file_content, 1),0, -1).",";
////$debug=$debug.$file_content."-";
//	$split_array = preg_split("/{*},/", $file_content);
//
//	
//
//	$json_array=null;
//
//	foreach ($split_array as $key => $value) 
//
//	{
//
//		$json_array[$key]=$value."}";
//
//
//
//	}
//        
//	for ($i=0; $i <count($split_array)-1 ; $i++) 
//
//	{ 
//
//			$json_array[$i]=$split_array[$i]."}";
//
//	}
//
//	$json_array[count($split_array)-1]=$split_array[count($split_array)-1];

	// echo "<hr>";
	//echo "File contents:\n";
	//print_r($json_array);
        $response=null;
        //for($st=0;$st<count($json_array)-1;$st++)
        $json_array=json_decode($file_content);

        
       foreach ($json_array as $jsons) 
        {
           $json=json_encode($jsons);
            //echo $json_array[$st];
            $handle = curl_init();
            $curl_option=array(
                                       CURLOPT_URL             => $url, 
                                       CURLOPT_RETURNTRANSFER  => true,
                                       CURLOPT_VERBOSE         => true,
                                       CURLOPT_POSTFIELDS      => $json,
                                       CURLOPT_HTTPHEADER      => $header,
                                       CURLOPT_SSL_VERIFYPEER  => false

                         );
            //$debug=$debug.$json_array[$st]."+";
            curl_setopt_array( $handle, $curl_option  );
            //perform POST/GET operation and GET the response
            $response_ = curl_exec($handle); 
            //close the curl adapter
            curl_close($handle);
            // Send the data back
            $response=$response_.$response;
        //print_r($json_array[$st]);
        }
        if($response!=null)
        {
           print_r($response); 
           unlink($file_path.$_REQUEST['file_name']);
        }
        else
        {
           //$debug="Debug: ".$debug; 
           //echo $debug;
        }
        

}


//if 
if( isset($_REQUEST["resource"]) && isset($_REQUEST["method"])&& $_REQUEST["method"]=="GET" )
{               
                $sql_array="";
		error_reporting(-1);
		ini_set('display_errors', true);

		$handle = curl_init();
		$header= array(   "Accept: application/json",
		                  "Content-type: application/json",
		                  $echo_req->to_header("")
		               );

		$curl_option=array(
                                        CURLOPT_URL => $url, 
                                        CURLOPT_RETURNTRANSFER  => true,
                                        CURLOPT_VERBOSE         => true,
                                        CURLOPT_HTTPHEADER      => $header,
                                        CURLOPT_SSL_VERIFYPEER  => false
		                                      
		                );
		
		curl_setopt_array( $handle, $curl_option  );
		//perform POST/GET operation and GET the response
		$response = curl_exec($handle); 
		//close the curl adapter
		curl_close($handle);
		// Send the data back
		print_r($response); 
                if (!file_exists($downloaded_file_path)) 
                {
                                    mkdir($downloaded_file_path, 0777, true);
                }
                $filehandel = fopen($downloaded_file_path.$_REQUEST["resource"].'.txt','w');
                //$_data=json_decode($response, true);
                fwrite($filehandel,$response); 
                fclose($filehandel);
//                $filehandel2 = fopen($downloaded_file_path.'debug.txt','w'); 
//                $filewrite_data="";
                if($_REQUEST["resource"]=='activity')
                {
                    $dirs=pic_folder_exist_();
                    $filewrite_data="";
                    //$filehandel = fopen($file_path.'nada.txt','w'); 
                    $json_array=json_decode($response, true);
                    foreach ($json_array as $json_value) 
                    {       
                            $activity_count=1;
                            $activityId=$json_value['id'];
                            $subject=$json_value['name'];
                            $start_=$json_value['started_at'];
                            $end_=$json_value['ended_at'];
                            
                            $start=str_replace(' ', '', $start_);
                            $start=preg_replace('[-|:]', '', $start);
                            $startDate=substr($start, 0,8);
                            $startTime=substr($start, 8,6);
                            
                            $end=str_replace(' ', '', $end_);
                            $end=preg_replace('[-|:]', '', $end);
                            $endDate=substr($end, 0,8);
                            $endTime=substr($end, 8,6);
//                            if ($activityId>4000)
//                                            $filewrite_data=$filewrite_data."#".$activityId."st:".  $startDate." en: ".$endDate;
                            if(!id_exists($activityId))
                            {
                                //$filewrite_data=$filewrite_data."#".$activityId;
                                foreach ($dirs as $dir) 
                                {
                                    
                                    if($dir>=$startDate && $dir<=$endDate)
                                    {
                                       
                                        $files_for_current_dir=pic_file_exist_($dir);
                                        foreach ($files_for_current_dir as $file) 
                                        {
                                            $fileExt = explode(".", $file);
                                            $fileName=$fileExt[0];
                                            if($fileName>=$startTime && $fileName<=$endTime)
                                            {
                                                //insert activity
                                                $src="/data/logsync/Logs/".$dir."/".$file;
                                                //$filewrite_data=$filewrite_data.$activityId." ".$start_." ".$end_." ".$subject." ".$src.'?';
                                                if($activity_count==1)
                                                {
                                                    $sql="INSERT INTO activity (activityId,startTime,endTime,subject,src,reviewed) 
                                                        VALUES
                                                        ('$activityId','$start_','$end_','$subject','$src',0);";
                                                    $sql_array=$sql_array.$sql;
                                                }
                                                
                                                $sql_media="INSERT INTO media (src,activityId) 
                                                        VALUES
                                                        ('$src','$activityId');";
                                                $sql_array=$sql_array.$sql_media;
                                                $activity_count++;
                                                
                                                //insert_activity_into_db($sql);
                                                //$filewrite_data=$filewrite_data.$sql;
                                            }
                                        }
                                    }
                                    
                                }
                                //$filewrite_data=$filewrite_data.$activityId." ";
                            }
                            
                            
                    }
                    insert_activities_into_db($sql_array);
                    //$filewrite_data=$filewrite_data.$sql_array;
//                    fwrite($filehandel2,$filewrite_data); 
//                    fclose($filehandel2); 
                delete_XML_files();    
                }
               
                
                
}


?>
<?php
//delete the file requested file
if( isset($_REQUEST["delete"]))
{
   $output_dir="public/data/uploaded/post/";
   $file=$output_dir.$_REQUEST["delete"];
   if(is_dir($file))
       recurseRmdir ($file);
   else
       unlink($file); 
   //echo $file;
}
?>
<?php
//rename the file requested file
if( isset($_REQUEST["rename"]) && isset($_REQUEST["replace"]))
{
   $output_dir="public/data/uploaded/post/";
   $file=$output_dir.$_REQUEST["rename"];
   if(is_dir($file))
       rename($file,$output_dir.$_REQUEST["replace"]);
   else
   {
       $ext=".".explode(".", $file)[1]; 
       rename($file,$output_dir.$_REQUEST["replace"].$ext); 
          
   }

   //echo $file;
}
?>
<?php
//GET AND POST Working fine:
//
//require_once("OAuth.php");
//require 'oauth_cfg.phtml';
//
//$plaintext_method = new OAuthSignatureMethod_PLAINTEXT();
//$test_consumer = new OAuthConsumer($config['consumerKey'], $config['consumerSecret'], NULL);
//$acc_token = new OAuthConsumer($oauth_token, $tokenSecret, 1);
//$url="https://healthlabs.ehv.campus.philips.com/resources/".$_REQUEST['resource'];
//$method=$_REQUEST["method"];
//$sig_method = $plaintext_method;
//
//$echo_req = OAuthRequest::from_consumer_and_token($test_consumer, $acc_token,$method , $url);
//$echo_req->sign_request($sig_method, $test_consumer, $acc_token);
//$data_by_activity=array('activity'=>1,'activity_count'=>2,'geo_coordinate'=>3,'heart_rate'=>4,'location_type'=>5,'location'=>6,'profile'=>7); 
//$recorded_at= date('Y-m-d H:i:s');
//$json_array=array(	
//					1=>'{	"name": "Running", "ended_at": "'.$recorded_at.'",  "started_at": "2013-08-13 11:20:34"	}',
//					2=>'{	"interval": 30, "measured_on": "chest", "started_at": "'.$recorded_at.'", "values": [70,71,72,72,75,75,76,74,75]	}',
//					3=>'{	"latitude": 51.40125, "longitude": 5.38573, "elevation": 12.5, "recorded_at": "'.$recorded_at.'"	}',
//					4=>'{ 	"started_at": "'.$recorded_at.'",  "interval": 30, "values": [ 70, 71, 72, 72, 75, 75, 76, 74, 75] }',
//					5=>'{ 	"name": "LTU",  "bounds": [ { "latitude": 55.625, "longitude": 5.211 }, { "latitude": 55.876, "longitude": 5.202 } ]}',
//					6=>'{	"location_type_id": 34, "ended_at": "'.$recorded_at.'", "started_at": "2013-09-12 11:20:34" }',
//					7=>'{	"first_name": "John", "last_name": "Doe", "gender": "male", "date_of_birth": "1983-07-25" }'
//	);
//
//
//
//
//if( isset($_REQUEST["resource"]) && isset($_REQUEST["method"]) )
//{
//		error_reporting(-1);
//		ini_set('display_errors', true);
//
//		$handle = curl_init();
//		//$url = 'http://127.0.0.1:50006';
//		//url to post or get
//		//$url = 'https://healthlabs.ehv.campus.philips.com/resources/geo_coordinate';
//		//data to be send
//		$data=$json_array[ 		$data_by_activity[	$_REQUEST['resource']	] 	];
//		$header= array(   "Accept: application/json",
//		                  "Content-type: application/json",
//		                  $echo_req->to_header("")
//		               );
//
//		$curl_option=array(
//		                                    CURLOPT_URL             => $url, 
//		                                    CURLOPT_RETURNTRANSFER  => true,
//		                                    CURLOPT_VERBOSE         => true,
//		                                    CURLOPT_POSTFIELDS      => $data,
//		                                    CURLOPT_HTTPHEADER      => $header,
//		                                    CURLOPT_SSL_VERIFYPEER  => false
//		                                      
//		                );
//		if($_REQUEST["method"]=="GET")
//		{
//			$curl_option=array(
//		                                    CURLOPT_URL             => $url, 
//		                                    CURLOPT_RETURNTRANSFER  => true,
//		                                    CURLOPT_VERBOSE         => true,
//		                                    CURLOPT_HTTPHEADER      => $header,
//		                                    CURLOPT_SSL_VERIFYPEER  => false
//		                                      
//		                );
//		}
//		
//		curl_setopt_array( $handle, $curl_option  );
//		//perform POST/GET operation and GET the response
//		$response = curl_exec($handle); 
//		//close the curl adapter
//		curl_close($handle);
//		// Send the data back
//		print_r($response); 
//                //print_r($method);
//		//print_r($data);
//                
//}
//

?>
