<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLReview;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Doctrine\DBAL\Types\Type;
use Zend\Session\Container;
use Zend\Session\Config\SessionConfig;
use Zend\Session\SessionManager;

/**
 * Base module for this application.
 */
class Module {

    /**
     * Run at upstart of this module.
     * @param \Zend\Mvc\MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e) {
        $sm = $e->getApplication()->getServiceManager();

        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        //Session settings
        $config = $sm->get('Configuration');
        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config['session']);
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        /**
         * Optional: If you later want to use namespaces, you can already store the
         * Manager in the shared (static) Container (=namespace) field
         */
        Container::setDefaultManager($sessionManager);

        //Language settings
        $translator = $sm->get('translator');
        $user_session = new Container('user');
        $session_language = 'en_GB';
        if (!empty($user_session->language)) {
            $session_language = $user_session->language;
        }
        $translator
                ->setLocale($session_language)
                ->setFallbackLocale('en_GB');

        //Register my custom types in Doctrine
        Type::addType('point', 'MLReview\Entity\Types\Point');
        Type::addType('polygon', 'MLReview\Entity\Types\Polygon');
        $em = $sm->get('doctrine.entitymanager.orm_default');
        $conn = $em->getConnection();
        $conn->getDatabasePlatform()->registerDoctrineTypeMapping('POINT', 'point');
        $conn->getDatabasePlatform()->registerDoctrineTypeMapping('POLYGON', 'polygon');

        //Doctrine EventListener for handling file uploads
        $base_path = $config['default']['base_upload_path'];
        $public_path = $config['default']['public_path'];
        $dem = $em->getEventManager();
        $dem->addEventListener(array(
            \Doctrine\ORM\Events::preUpdate,
            \Doctrine\ORM\Events::prePersist,
            \Doctrine\ORM\Events::postRemove
                ), new Entity\Listener\FileHandlerListener($base_path, $public_path)
        );
    }

    /**
     * The configuration for this module.
     * @return type
     */
    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            //'Zend\Loader\ClassMapAutoloader' => array(
            //    __DIR__ . '/autoload_classmap.php',
            //),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    //Custom Form Elements
    /* public function getFormElementConfig() {

      return array(
      'invokables' => array(
      'picture' => 'MLReview\Form\Element\Picture'
      )
      );
      }
     */

    /**
     * The local services for this module. Reachable by the ServiceLocator.
     * @return array
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'MLCluster' => function ($sm) {
                    $entityManager = $sm->get('doctrine.entitymanager.orm_default');
                    $config = $sm->get('Config');
                    $media_path = $config['default']['logsync_media_path'];
                    return new \MLReview\Service\Adapter\MLClusterAdapter($entityManager, $media_path);
                },
                'DisplayDateGenerator' => function ($sm) {
                    $entityManager = $sm->get('doctrine.entitymanager.orm_default');
                    return new \MLReview\Util\DisplayDateGenerator($entityManager, new \DateTime());
                },
                'AssetUtil' => function ($sm) {
                    $config = $sm->get('Config');
                    $base_path = $config['default']['base_upload_path'];
                    $public_path = $config['default']['public_path'];
                    return new \MLReview\Util\AssetUtil($base_path, $public_path);
                },
                'NumberOfMediaFilter' => function () {
                    return new \MLReview\Service\Adapter\Filter\NumberOfMediaFilter(0);
                },
                'MinTimeActivityFilter' => function () {
                    return new \MLReview\Service\Adapter\Filter\MinTimeActivityFilter(0);
                },
            )
        );
    }

    /**
     * The View helpers of this module.
     * @return array
     */
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
                'absoluteUrl' => function($sm) {
                    $sl = $sm->getServiceLocator();
                    return new \MLReview\View\Helper\AbsoluteUrl($sl->get('Request'));
                },
                'getRoute' => function($sm) {
                    $sl = $sm->getServiceLocator();
                    return new \MLReview\View\Helper\GetRoute($sl->get('Request'), $sl->get('Router'));
                },
                'getSession' => function() {
                    return new \MLReview\View\Helper\GetSession(new Container('user'));
                },
                'getOldActivityDates' => function($sm) {
                    $entityManager = $sm->getServiceLocator()->get('doctrine.entitymanager.orm_default');
                    return new \MLReview\View\Helper\GetOldActivityDates($entityManager);
                },
            ),
        );
    }

}

