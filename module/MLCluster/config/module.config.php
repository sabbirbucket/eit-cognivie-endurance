<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLCluster;

return array(
    'router' => array(
        'routes' => array(
            'mlcluster' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/mlcluster',
                    'defaults' => array(
                        'controller' => 'MLCluster\Controller\Cluster',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'MLCluster\Controller\Cluster' => 'MLCluster\Controller\ClusterController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'cluster/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'ml-cluster/cluster/index' => __DIR__ . '/../view/mlcluster/index/index.phtml',
        //'error/404' => __DIR__ . '/../view/error/404.phtml',
        //'error/index' => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
