<?php

/**
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

namespace MLCluster\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Http\ClientStatic;

/**
 * MVC framework. Controller for the cluster page.
 *
 */
class ClusterController extends AbstractActionController {

    /**
     * Extracts the query from the calling client and directs to the Javascript pages.
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction() {
        //Switch layout to the simple one instead of standard layout
        $this->layout()->setTemplate('cluster/layout');

        //Extract the options from the route call
        $options = $this->params()->fromQuery('options');
        $known_places = $options['known_places_source'];
        $known_bt = $options['known_bt_source'];
        $gps = $options['gps_source'];
        $bt = $options['bt_source'];
        $mediaimage = $options['mediaimage_source'];

        //This is the page to post the data to
        $post_to = $this->params()->fromQuery('postTo');
        $root = $_SERVER['DOCUMENT_ROOT'];
        if (!file_exists($root . $known_places) || !file_exists($root . $known_bt) || !file_exists($root . $mediaimage)) {
            //We just post back an error
            //A bit slow, but best we got
            ClientStatic::post($post_to, array(
                'error' => 'true',
            ));

            //Show ugly get parameter at mlreview home...
            //TODO: Solve this?
            //$this->redirect()->toUrl($post_to . "?error=true");
        }


        return new ViewModel(array(
            'known_places_source' => $known_places,
            'known_bt_source' => $known_bt,
            'gps_source' => $gps,
            'bt_source' => $bt,
            'mediaimage_source' => $mediaimage,
            'post_to' => $post_to
        ));
    }

}
