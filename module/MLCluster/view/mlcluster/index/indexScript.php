
<?php
date_default_timezone_set('Europe/Stockholm');
?>
<script>


    /*
     function find_date()
     {
     var x= new Date();
     var y= x.getYear();
     var m= x.getMonth()+1;  // added +1 because javascript counts month from 0
     var d= x.getDate();

     var the_date = m+'/'+d+'/'+y;
     return the_date;


     }


     */
    function pausecomp(millis)
    {
        var date = new Date();
        var curDate = null;

        do {
            curDate = new Date();
        }
        while (curDate - date < millis);
    }


    function load_activities(image)
    {
//pausecomp(3000);
        image_source = image.src;
//window.alert('dddd');
//window.alert(image_source);
// this image is chosen randomly just to give an indication if the system is loading the activities or the user clicked on 1 image to start reviewing an activity
        if (false /*image_source != 'http://127.0.0.1/xampp/review/images/Add/reviewed.png'*/)
            updateInterface(loading_activities); // show the messagr hamter handelser

//document.getElementsByName("demo3").item(0).value = '';
        xmlDoc = document.implementation.createDocument("", "", null);
        xmlDoc1 = document.implementation.createDocument("", "", null);

        xmlDoc2 = document.implementation.createDocument("", "", null);
        xmlDoc3 = document.implementation.createDocument("", "", null);
        xmlDoc4 = document.implementation.createDocument("", "", null); // this is for the images


        var xmlFile2 = gps_source;
        var xmlFile3 = known_places_source;
        var xmlFile = bt_source;
        var xmlFile1 = known_bt_source;
        var xmlFile4 = mediaimage_source; // this is for the images

        xmlDoc4.load(xmlFile4); // this is for the images
        xmlDoc3.load(xmlFile3);
        xmlDoc2.load(xmlFile2);
        xmlDoc1.load(xmlFile1);
        xmlDoc.load(xmlFile);

        xmlDoc.onload = Process_activity_main;


    } // end of the main function

    function Process_activity_main()
    {

//var number_of_added_places = 0;

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
// dealing with persons first

//var c = count_time_BT("01:30:40",150);
//window.alert(c);
//var c = toDate("01:30:40");
//window.alert(c);
        number_of_added_UID = 0; // this is a counter to count how many UIDs are added to the array
//window.alert(number_of_added_UID);

        var devices = xmlDoc.getElementsByTagName("Devices"); // get all the nodes with the name devices
        var number_of_devices = devices.length; // get the number of the nodes
//window.alert(number_of_devices);
// *****************************************************************************************************************
// this is to control the number of the BT so it does not exceed the limit
        if (number_of_devices >= num_UID_index - 1)
            number_of_devices = num_UID_index - 1;

        if (number_of_devices != 0 && number_of_devices != 1)
        {
            //window.alert('dddd');
            for (var i = 0; i < number_of_devices; i++) // this loop is to go over all the devices node. For each node, we will check how many times repeated and then get the date time //and UIDs
            {
                //window.alert(number_of_devices);
                var repeated = devices[i].attributes.getNamedItem("repeat").nodeValue; // find the repeating attribute for each node
                var Date_time = devices[i].attributes.getNamedItem("scanTime").nodeValue; // find the time and date attribute for each node
                var newDate_time = '20' + Date_time; // to make it look like 2009-02-16
                var splitting = newDate_time.split(" "); // split when there is a space
                var the_date = splitting[0]; // get the date
                var the_time = splitting[1]; // get the time

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var reviewed = devices[i].getElementsByTagName("reviewed")[0].firstChild.nodeValue;


                var UIDs = devices[i].getElementsByTagName("UID");
                var number_of_UID = UIDs.length; // find how many UIDs inside each node of devices

                //window.alert(number_of_UID);

                if (i == 0) // the node is the first one, so we need to add all UIDs
                {

                    for (var y = 0; y < number_of_UID; y++) // make a loop inside each devices node to read the UIDs values
                    {
                        if (repeated != 0)
                        {

                            var UID_value = UIDs[y].firstChild.nodeValue; // this is the value of UID
                            var next_Date_time = devices[i + 1].attributes.getNamedItem("scanTime").nodeValue; // find the time and date attribute for each node
                            var next_newDate_time = '20' + next_Date_time; // to make it look like 2009-02-16
                            var next_splitting = next_newDate_time.split(" "); // split when there is a space
                            var next_the_date = next_splitting[0]; // get the date
                            var next_the_time = next_splitting[1]; // get the time

                            // now we can add the data to the first row of the array of UIDs
                            BT_detected[number_of_added_UID][0] = UID_value;
                            BT_detected[number_of_added_UID][1] = the_time;
                            BT_detected[number_of_added_UID][2] = next_the_time;
                            BT_detected[number_of_added_UID][3] = the_date;
                            BT_detected[number_of_added_UID][4] = repeated;
                            BT_detected[number_of_added_UID][5] = 'no';
                            BT_detected[number_of_added_UID][6] = '';

                            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                            BT_detected[number_of_added_UID][7] = reviewed;



                            //document.write(UID_value);

                            number_of_added_UID++;

                        } // end if (repeated != 0)

                        else
                        {

                            // here is when the person is detected just 1 time without repeating (i didn't add it)
                            /*
                             var UID_value = UIDs[y].firstChild.nodeValue; // this is the value of UID
                             var next_Date_time = devices[i+1].attributes.getNamedItem("scanTime").nodeValue; // find the time and date attribute for each node
                             var next_newDate_time = '20' + next_Date_time; // to make it look like 2009-02-16
                             var next_splitting = next_newDate_time.split(" "); // split when there is a space
                             var next_the_date = next_splitting[0]; // get the date
                             var next_the_time = next_splitting[1]; // get the time

                             // now we can add the data to the first row of the array of UIDs
                             BT_detected[number_of_added_UID][0] = UID_value;
                             BT_detected[number_of_added_UID][1] = the_time;
                             BT_detected[number_of_added_UID][2] = the_time; // because it is not repeated so we don't need to put the next time
                             BT_detected[number_of_added_UID][3] = the_date;
                             BT_detected[number_of_added_UID][4] = repeated;
                             BT_detected[number_of_added_UID][5] = 'no';

                             //document.write(UID_value);

                             number_of_added_UID++;

                             */
                        }// end else

                    } // end of the loop var y=0; y<number_of_UID; y++
                } // end if i == 0

                else

                if (i != number_of_devices - 1) // if the node is not the first one or the last one
                {
                    //document.write(i);document.write('<br>');
                    //document.write(the_time);document.write('<br>');


                    /*
                     if (i == 435)
                     {
                     document.write(devices[i].attributes.getNamedItem("scanTime").nodeValue);
                     }
                     document.write(i);
                     document.write('<br>');

                     */

                    for (var y = 0; y < number_of_UID; y++) // make a loop inside each devices node to read the UIDs values
                    {
                        var UID_value = UIDs[y].firstChild.nodeValue; // this is the value of UID
                        var Date_time = devices[i].attributes.getNamedItem("scanTime").nodeValue; // find the time and date attribute for each node
                        var newDate_time = '20' + Date_time; // to make it look like 2009-02-16
                        var splitting = newDate_time.split(" "); // split when there is a space
                        var the_date = splitting[0]; // get the date
                        var the_time = splitting[1]; // get the time
                        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                        var reviewed = devices[i].getElementsByTagName("reviewed")[0].firstChild.nodeValue;
                        //document.write(UID_value);
                        // document.writeln("<br>");

                        var next_Date_time = devices[i + 1].attributes.getNamedItem("scanTime").nodeValue; // find the time and date attribute for each node
                        var next_newDate_time = '20' + next_Date_time; // to make it look like 2009-02-16
                        var next_splitting = next_newDate_time.split(" "); // split when there is a space
                        var next_the_date = next_splitting[0]; // get the date
                        var next_the_time = next_splitting[1]; // get the time

                        var exist = 'NO'; // this variable to check if this UID already added to the array before
                        for (var compare = 0; compare < number_of_added_UID; compare++) // go for each UID in the array
                        {

                            if (UID_value == BT_detected[compare][0] && the_date == next_the_date) // compare the current value with the UIDs in the saved array
                            {
                                the_start_time_in_the_array = BT_detected[compare][1];
                                the_end_time_in_the_array = BT_detected[compare][2];
                                the_repeated_in_the_array = BT_detected[compare][4];
                                if (the_time == the_end_time_in_the_array) // this means that the UID is repeated exactly in the next node
                                {
                                    BT_detected[compare][2] = next_the_time;
                                    BT_detected[compare][4] = parseInt(the_repeated_in_the_array) + parseInt(repeated) + 1;
                                    exist = 'YES';
                                } // end of if (UID_value == BT_detected[compare][0])

                            }//end of if (UID_value == BT_detected[compare][0])


                        } // end of for (var compare=0; compare<number_of_added_UID; compare++)
                        if (exist == 'NO') // this means that this UID does not exist before in the array
                        {
                            //if(i > 150) alert(repeated + ' ' + the_time);
                            if (repeated != 0)

                            {

                                BT_detected[number_of_added_UID][0] = UID_value;
                                BT_detected[number_of_added_UID][1] = the_time;
                                BT_detected[number_of_added_UID][2] = next_the_time;
                                BT_detected[number_of_added_UID][3] = the_date;
                                BT_detected[number_of_added_UID][4] = repeated;
                                BT_detected[number_of_added_UID][5] = 'no';
                                BT_detected[number_of_added_UID][6] = '';
                                BT_detected[number_of_added_UID][7] = reviewed;

                                number_of_added_UID++;

                            } // end if (repeated != 0)

                            else

                            {
                                // here is when the person is detected just 1 time without repeating (i didn't add it)

                                /*
                                 BT_detected[number_of_added_UID][0] = UID_value;
                                 BT_detected[number_of_added_UID][1] = the_time;
                                 BT_detected[number_of_added_UID][2] = the_time; // because it is not repeated so we don't need to put the next time
                                 BT_detected[number_of_added_UID][3] = the_date;
                                 BT_detected[number_of_added_UID][4] = repeated;
                                 BT_detected[number_of_added_UID][5] = 'no';
                                 number_of_added_UID++;
                                 */
                            }// end else

                        }// end if (exist == 'NO')


                    } // end of the loop var y=0; y<number_of_UID; y++

                }// end of if (i != number_of_devices-1)


                else // this node is the last node

                if (i == number_of_devices - 1)

                {
                    //window.alert('ddd');
                    //	document.write('dddddddbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbddddd');

                    var repeated = devices[i].attributes.getNamedItem("repeat").nodeValue; // find the repeating attribute for each node
                    var Date_time = devices[i].attributes.getNamedItem("scanTime").nodeValue; // find the time and date attribute for each node
                    var newDate_time = '20' + Date_time; // to make it look like 2009-02-16
                    var splitting = newDate_time.split(" "); // split when there is a space
                    var the_date = splitting[0]; // get the date
                    var the_time = splitting[1]; // get the time
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    var reviewed = devices[i].getElementsByTagName("reviewed")[0].firstChild.nodeValue;
                    /////////////////////////////////////////
                    /////////////////////////////////////////
                    /////////////////////////////////////////
                    // Ending time needs to be counted based on the repeat attribute

                    var UIDs = devices[i].getElementsByTagName("UID");
                    var Date_time = devices[i].attributes.getNamedItem("scanTime").nodeValue; // find the time and date attribute for each node
                    var newDate_time = '20' + Date_time; // to make it look like 2009-02-16
                    var splitting = newDate_time.split(" "); // split when there is a space
                    var the_date = splitting[0]; // get the date
                    var the_time = splitting[1]; // get the time
                    var number_of_UID = UIDs.length; // find how many UIDs inside each node of devices


                    var previous_UIDs = devices[i - 1].getElementsByTagName("UID");
                    var previous_Date_time = devices[i - 1].attributes.getNamedItem("scanTime").nodeValue; // find the time and date attribute for each node
                    var previous_newDate_time = '20' + previous_Date_time; // to make it look like 2009-02-16
                    var previous_splitting = previous_newDate_time.split(" "); // split when there is a space
                    var previous_the_date = previous_splitting[0]; // get the date
                    var previous_the_time = previous_splitting[1]; // get the time
                    var number_of_previous_UID = previous_UIDs.length; // find how many UIDs inside each node of devices
                    //window.alert(number_of_UID);

                    for (var y = 0; y < number_of_UID; y++) // make a loop inside each devices node to read the UIDs values
                    {
                        var exist = 'no';
                        var UID_value = UIDs[y].firstChild.nodeValue;
                        //window.alert(number_of_added_UID);

                        //window.alert(UID_value);
                        for (var z = 0; z < number_of_previous_UID; z++)
                        {
                            var previous_UID_value = previous_UIDs[z].firstChild.nodeValue;
                            if (UID_value == previous_UID_value && the_date == previous_the_date)
                            {
                                exist = 'yes';

                                break;
                            }
                        }
                        // when finding the ending time, we should update the array based on it... for the moment, im just skipping it

                        ///*
                        if (exist == 'no' && repeated != 0) // ********************************************************************
                        {
                            //window.alert(UID_value);
                            // now we can add the data to the first row of the array of UIDs
                            BT_detected[number_of_added_UID][0] = UID_value;
                            BT_detected[number_of_added_UID][1] = the_time;
                            BT_detected[number_of_added_UID][2] = count_time_BT(the_time, repeated); // here is the ending time
                            BT_detected[number_of_added_UID][3] = the_date;
                            BT_detected[number_of_added_UID][4] = repeated;
                            BT_detected[number_of_added_UID][5] = 'no';
                            BT_detected[number_of_added_UID][6] = '';
                            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                            BT_detected[number_of_added_UID][7] = reviewed;

                            number_of_added_UID++;
//		window.alert(number_of_added_UID);


                        } // end of if (exist == 'no'


                    } //end of for (var y=0; y<number_of_UID; y++)



                } // end of if (i == number_of_devices-1)


            } // end var i=0; i<number_of_devices; i++)

        }// end of if (number_of_devices != 0 && number_of_devices != 1)


        /////////////////////////////////////////
        /////////////////////////////////////////
        /////////////////////////////////////////


//window.alert(number_of_added_UID);


        if (number_of_devices == 1)
        {
            var repeated = devices[0].attributes.getNamedItem("repeat").nodeValue; // find the repeating attribute for each node
            var Date_time = devices[0].attributes.getNamedItem("scanTime").nodeValue; // find the time and date attribute for each node
            var newDate_time = '20' + Date_time; // to make it look like 2009-02-16
            var splitting = newDate_time.split(" "); // split when there is a space
            var the_date = splitting[0]; // get the date
            var the_time = splitting[1]; // get the time
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            var reviewed = devices[0].getElementsByTagName("reviewed")[0].firstChild.nodeValue;
            /////////////////////////////////////////
            /////////////////////////////////////////
            /////////////////////////////////////////
            // Ending time needs to be counted based on the repeat attribute

            var UIDs = devices[0].getElementsByTagName("UID");
            var number_of_UID = UIDs.length; // find how many UIDs inside each node of devices
            if (repeated != 0) // ********************************************************************
            {
                //window.alert(number_of_UID);

                for (var y = 0; y < number_of_UID; y++) // make a loop inside each devices node to read the UIDs values
                {
                    var UID_value = UIDs[y].firstChild.nodeValue; // this is the value of UID

                    // now we can add the data to the first row of the array of UIDs
                    BT_detected[number_of_added_UID][0] = UID_value;
                    BT_detected[number_of_added_UID][1] = the_time;
                    BT_detected[number_of_added_UID][2] = count_time_BT(the_time, repeated); // here is the ending time
                    BT_detected[number_of_added_UID][3] = the_date;
                    BT_detected[number_of_added_UID][4] = repeated;
                    BT_detected[number_of_added_UID][5] = 'no';
                    BT_detected[number_of_added_UID][6] = '';
                    BT_detected[number_of_added_UID][7] = reviewed;
                    number_of_added_UID++;

                } //end of for (var y=0; y<number_of_UID; y++)

            } // end if (repeated !=0)

        } // end of if (number_of_devices == 1)



// now we have a multi-dimentional array that contains the UIDs, we need to check if those UIDs are for know persons

//window.alert(number_of_added_UID);

        /////////////////////////////////////////
        /////////////////////////////////////////
        /////////////////////////////////////////

// we compare every UID in the array with known UIDs in BT_persons.xml

        xmlDoc1 = document.implementation.createDocument("", "", null);
        var xmlFile1 = known_bt_source;

        xmlDoc1.load(xmlFile1);
        xmlDoc1.onload = Process_activity17_main;

    } //end of the function proces activity

    function Process_activity17_main()
    {

        var devices1 = xmlDoc1.getElementsByTagName("file"); // get all the nodes with the name devices
        var known_devices = devices1.length; // get the number of the nodes

//window.alert(known_devices);
        for (var i = 0; i < number_of_added_UID; i++)
        {
            var current_UID = BT_detected[i][0];


            for (var y = 0; y < known_devices; y++)
            {
                var known_UID = devices1[y].getElementsByTagName("UID")[0].firstChild.nodeValue;

                var known_name = devices1[y].getElementsByTagName("name")[0].firstChild.nodeValue;
                var known_path = destination + devices1[y].getElementsByTagName("path")[0].firstChild.nodeValue;
                //document.writeln(known_name);
                if (current_UID == known_UID)
                {

                    BT_detected[i][0] = known_name; // replace the UID with the name of the person
                    BT_detected[i][5] = 'yes'; // mark as a known person
                    BT_detected[i][6] = known_path; // put the path of the image

                    break;

                }

            }// end of (var y=0; y<number_of_devices1; y++)

        }// end of (var i=0; i<number_of_added_UID; i++)

        /*
         var j = "These are all the UID's known and unknown";
         j= j.bold();
         document.writeln(j);
         document.writeln("<br>");
         document.writeln("<br>");

         for (var b=0; b<number_of_added_UID; b++) // this is to display the saved UIDs
         {
         for (var j=0; j<8; j++)
         {document.writeln(BT_detected[b][j]);}
         document.writeln("<br>");
         }// end of (var b=0; b<number_of_added_UID; b++)




         document.writeln("<br>");
         document.writeln("<br>");
         document.writeln("<br>");

         */
/////////////////////
/////////////////////

// now we will move the known detected persons to known_BT_detected array
//window.alert(number_of_added_UID);
        known_index = 0;
//window.alert(number_of_added_UID);

        for (var b = 0; b < number_of_added_UID; b++) // this is to display the saved UIDs
        {
            if (BT_detected[b][5] != undefined)
            {
                //window.alert(BT_detected[b][5]);
                if (BT_detected[b][5] == 'yes')
                {
                    known_BT_detected[known_index][0] = BT_detected[b][0];
                    known_BT_detected[known_index][1] = BT_detected[b][1];
                    known_BT_detected[known_index][2] = BT_detected[b][2];
                    known_BT_detected[known_index][3] = BT_detected[b][3];
                    known_BT_detected[known_index][4] = BT_detected[b][4];
                    known_BT_detected[known_index][5] = BT_detected[b][5];
                    known_BT_detected[known_index][6] = BT_detected[b][6];
                    known_BT_detected[known_index][7] = BT_detected[b][7];
                    known_index = known_index + 1;

                }

            }
            if (known_index > known_UID_index - 2)
                break;

        }

//window.alert(known_index);
// the next loop is to make the ending time of each meeting of person 1 second less so the ending time of perosn will not be the same like the staring time of the next
        for (var b = 0; b < known_index; b++)
        {

            known_BT_detected[b][2] = Add_seconds(known_BT_detected[b][2], -1);

        }
//window.alert(known_index);

        /*
         var j = "These are all the known persons detected";
         j= j.bold();
         document.writeln(j);
         document.writeln("<br>");
         document.writeln("<br>");


         // printing the previous array
         for (var b=0; b<known_index; b++) // this is to display the saved UIDs
         {
         for (var j=0; j<8; j++)
         {document.writeln(known_BT_detected[b][j]);}
         document.writeln("<br>");
         }// end of (var b=0; b<number_of_added_UID; b++)
         */




/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

// now dealing with GPS

//window.alert(number_of_added_positions);
        xmlDoc2 = document.implementation.createDocument("", "", null);
        var xmlFile2 = gps_source;
        xmlDoc2.load(xmlFile2);
        xmlDoc2.onload = Process_activity5_main;

    } // end of the function process activity

    function Process_activity5_main()
    {


        number_of_added_positions = 0; // this is a counter to count how many detected positions are added to the array



//window.alert(known_index);


        var positions = xmlDoc2.getElementsByTagName("pos"); // get all the nodes with the name devices
        var number_of_positions = positions.length; // get the number of the nodes

        //window.alert(number_of_positions);

///////////////////////////////
//////////////////////////////
// this is to control the number of the BT so it does not exceed the limit
        if (number_of_positions >= num_GPS_index - 1)
            number_of_positions = num_GPS_index - 1;
// this loop is to save all the position from GPSlog.xml file (detected positions) in positions_detected array

        for (var i = 0; i < number_of_positions; i++)
        {
            var Date_time = positions[i].attributes.getNamedItem("posTime").nodeValue; // find the repeating attribute for each node
            var newDate_time = '20' + Date_time; // to make it look like 2009-02-16
            var splitting = newDate_time.split(" "); // split when there is a space
            var the_date = splitting[0]; // get the date
            var the_time = splitting[1]; // get the time
            var y = positions[i].getElementsByTagName("y")[0].firstChild.nodeValue;
            var x = positions[i].getElementsByTagName("x")[0].firstChild.nodeValue;
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            var reviewed = positions[i].getElementsByTagName("reviewed")[0].firstChild.nodeValue;

            // var satellites = positions[i].getElementsByTagName("satellites")[0].firstChild.nodeValue;
            // if (satellites > satellites_threshold) { // make sure that the number of satellites is satisfying

            positions_detected[number_of_added_positions][0] = parseFloat(x);
            positions_detected[number_of_added_positions][1] = parseFloat(y);
            positions_detected[number_of_added_positions][2] = the_time; // here is the ending time
            positions_detected[number_of_added_positions][3] = the_date;
            positions_detected[number_of_added_positions][4] = 'no'; // known?
            positions_detected[number_of_added_positions][5] = 'n/a'; // name of the place
            positions_detected[number_of_added_positions][6] = ''; // name of the place
            positions_detected[number_of_added_positions][7] = reviewed; // name of the place


            number_of_added_positions++;
            // }

        }    // end of for (var i=0; i<number_of_positions ; i++)


//////////////////////////////
//////////////////////////////


        xmlDoc3 = document.implementation.createDocument("", "", null);
        var xmlFile3 = known_places_source;
        xmlDoc3.load(xmlFile3);
        xmlDoc3.onload = Process_activity18_main;

    } // end of the function process activity

    function Process_activity18_main()
    {


        var places = xmlDoc3.getElementsByTagName("file"); // get all the nodes with the name file
        var number_of_places = places.length; // get the number of the nodes
//window.alert(number_of_added_positions);

        var help_array = new Array(20); // this array will separate the coordinates of a known place and save them for adding in the_place array
        known_place_index = 0; // this is a variable will incease 1 everytime we add a new coordinate for a place
        for (var i = 0; i < number_of_places; i++) // go through all the known places
        {

            var coordinates = places[i].getElementsByTagName("GPS")[0].firstChild.nodeValue; // get the coordinates for a place
            //window.alert(coordinates.length);

            var help_array = coordinates.split(","); // split according to ,
            //window.alert(help_splitting.length);


            for (var z = 0; z < help_array.length - 1; z += 2) // go through all the coordinates after splitting
            {
                the_place[known_place_index][0] = parseFloat(help_array[z + 1]); // save the X as number
                the_place[known_place_index][1] = parseFloat(help_array[z]); // save the Y as number

                known_place_index++;
                // the next if is to control the size of the array
                //*************************************************************************************************************
                if (known_place_index >= num_places_index - 1)
                    break;
                //var r = parseFloat(the_place[known_place_index][0]) + parseFloat(the_place[known_place_index][1]);
                //window.alert(r);
                //window.alert (the_place[known_place_index][0]);
                //window.alert (the_place[known_place_index][1]);


            } // end of for (var z=0; z< help_array.length-1; z+=2 )

// the array the_place[][] contains the coordinates of a known place as x,y in each line with known_place_index lines (coordinates), so we can compare every new detected position is it is inside that polygon
// all new detected positions are saved in the array positions_detected[][] as x, y, time, date, known?, name of the place if it is known... number_of_added_positions is the numer of the lines


            for (var b = 0; b < number_of_added_positions; b++) // take each position and check if it is in a known place
            {

                var lat = positions_detected[b][0];
                var lon = positions_detected[b][1];
                var the_length = known_place_index;
                var the_name = places[i].getElementsByTagName("name")[0].firstChild.nodeValue;
                var path_place = places[i].getElementsByTagName("path")[0].firstChild.nodeValue;
                //window.alert(places[b].getElementsByTagName("name")[0].firstChild.nodeValue);


                if (pointInPolygon(the_place, the_length, lat, lon))
                {
                    positions_detected[b][4] = 'yes';
                    positions_detected[b][5] = the_name;
                    positions_detected[b][6] = destination + path_place;

                    //window.alert(the_name);
                } // end of if (pointInPolygon(the_place,the_length,lat,lon) == 'yes')




            } // end of for (var b=0; b<number_of_added_positions ;b++)




            known_place_index = 0; // this is to start again with a new place

        } // end of for (var i=0; i<number_of_places ; i++ )

///////////////////////////////////////
///////////////////////////////////////

// the array positions_detected has all the detected positions with the name of the place if the point is inside a known polygon
// the length of the array is number_of_added_positions
// now we need to move the array to another array called detected_places which contains the name of place with the start and end time
// the structue of detected_places array is name, start time, end time, date, path of the image, and if it is reviewed before


//window.alert(known_index);


//var index = -1; // this is the index which will refer to the added place // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ =-1


// check the first position and set index according to the name value
        var first_time = positions_detected[0][2];
        var first_date = positions_detected[0][3];
        var first_name = positions_detected[0][5]; // this is the name of the first position (might be a name or n/a)
        var first_path = positions_detected[0][6];
        var first_reviewed = positions_detected[0][7];

        if (first_name != undefined)
        {
            if (first_name == 'n/a')
            {
                index = -1; // it is -1 so it will be 0 for the first adding
            }
            else
            {
                places_detected[0][0] = first_name;
                places_detected[0][1] = first_time;
                places_detected[0][2] = count_time_GPS(first_time, 1); // this is just to add the GPS_interval to the start time
                places_detected[0][3] = first_date;
                places_detected[0][4] = first_path;
                places_detected[0][5] = first_reviewed;
                index = 0;

            }
        }

//window.alert('ssss');
//////////////
//////////////

// now we can start the loop to compare and combine positions which are in the same place and have the same date

        var i = 1; // this is the variable which will go through all the position, we start with 1 because the first position was checked before
//window.alert(number_of_added_positions);
        while (i < number_of_added_positions)
        {

            var the_time = positions_detected[i][2];
            var the_date = positions_detected[i][3];
            var the_name = positions_detected[i][5];
            var path_place = positions_detected[i][6];
            var reviewed = positions_detected[i][7];


            if (the_name != 'n/a')
            {
                previous_name = positions_detected[i - 1][5];
                previous_date = positions_detected[i - 1][3];


                if (the_name == previous_name && the_date == previous_date)
                {

                    places_detected[index][2] = the_time;

                }// end if (the_name == previous_name)
                else
                {

                    index = index + 1;
                    places_detected[index][0] = the_name;
                    places_detected[index][1] = the_time;
                    places_detected[index][2] = count_time_GPS(the_time, 1); // this is just to add the GPS_interval to the start time
                    places_detected[index][3] = the_date;
                    places_detected[index][4] = path_place;
                    places_detected[index][5] = reviewed;


                } // end else



            } // end if (the_name != 'n/a')

//*************************************************************************************************************
            if (index >= num_places_index - 1)
                break;

            i = i + 1;

        }// end while


        number_of_places = index + 1;

//window.alert(number_of_places);

//window.alert(number_of_places);

        /*


         // printing the array that contains the new detected positions

         document.writeln("<br>");
         document.writeln("<br>");
         document.writeln("<br>");


         var j = "These are all the positions known and unknown";
         j= j.bold();
         document.writeln(j);
         document.writeln("<br>");
         document.writeln("<br>");

         for (var b=0; b<number_of_added_positions; b++) // this is to display the saved UIDs
         {
         for (var j=0; j<8; j++)
         {document.writeln(positions_detected[b][j]);}
         document.writeln("<br>");
         }// end of (var b=0; b<number_of_added_UID; b++)

         document.writeln("<br>");
         document.writeln("<br>");
         document.writeln("<br>");



         var j = "These are all known places detected";
         j= j.bold();
         document.writeln(j);
         document.writeln("<br>");
         document.writeln("<br>");

         for (var b=0; b<number_of_places; b++) // this is to display the saved UIDs
         {
         for (var j=0; j<6; j++)
         {document.writeln(places_detected[b][j]);}
         document.writeln("<br>");
         }// end of (var b=0; b<number_of_added_UID; b++)
         //
         //
         document.writeln("<br>");
         document.writeln("<br>");
         document.writeln("<br>");
         */

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////



// now it is time to create activities based on persons and places
// the visited places are in places_detected[][] array with number_of_places
// the met persons are in BT_detected[][] with number_of_added_UID


        activity_index = 0;
        var place_index;
        var person_index;




        /*
         // this first if is to check if the starting time of the place is bigger than the starting time of the person
         if (known_index!=0 && number_of_places!=0)
         {


         for (place_index=0; place_index < number_of_places; place_index++) // go over all places
         {

         var ST_place = places_detected[place_index][1]; // start time for being in a place
         var ET_place = places_detected[place_index][2]; // end time for being in a place
         var place_date = places_detected[place_index][3];

         for (person_index=0; person_index < known_index; person_index++) // go over all persons
         {


         var ST_person = known_BT_detected[person_index][1]; // start time for meeting the person
         var ET_person = known_BT_detected[person_index][2]; // end time for meeting the person
         var person_date = known_BT_detected[person_index][3]; // the date of meeting
         //window.alert(date_ST_person);
         //window.alert(date_ET_person);

         //window.alert(date_ST_place);
         //window.alert(date_ET_place);
         //document.writeln(place_index);
         if (person_date == place_date) // the date should be the same
         {

         var difference_ST1 = compare_time(ST_place,ST_person); // the difference between starintg time of the place with starting time of the person and the same for the rest
         var difference_ST2 = compare_time(ST_place,ET_person);
         var difference_ET1 = compare_time(ET_place,ST_person);
         var difference_ET2 = compare_time(ET_place,ET_person);

         if ((difference_ST1 <0 && difference_ST2 >0)||(difference_ET1<0 && difference_ET2>0))
         {
         //window.alert(person_index);


         if (known_BT_detected[person_index][5] != undefined)
         {
         if (known_BT_detected[person_index][5] == 'yes') // the person is known
         {
         activities[activity_index][0] = places_detected[place_index][0] + ' ' + 'med' + ' ' + known_BT_detected[person_index][0];
         activities[activity_index][1] = ST_place;
         activities[activity_index][2] = ET_place;
         activities[activity_index][3] = known_BT_detected[person_index][3];
         activities[activity_index][4] = places_detected[place_index][0]; // this is the place
         activities[activity_index][5] = known_BT_detected[person_index][0]; // this is the person
         activities[activity_index][6] = places_detected[place_index][4]; // this is the path of the place image
         activities[activity_index][7] = known_BT_detected[person_index][6];// this is the path of the person image
         //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         activities[activity_index][8] = places_detected[place_index][5];// this will show if the activity is reviewed or not
         activity_index = activity_index + 1;
         //document.write('ET_person > ET_place');
         } // end od if the person is known
         }// end if (known_BT_detected[person_index][5] != undefined)


         }// end for the big if

         }// end if (person_date == place_date)


         }// for (person_index=0; person_index < known_index; person_index++)



         } // end for (place_index=0; place_index < number_of_places; place_index++)


         } // end if (known_index!=0 && number_of_places!=0)



         */










//window.alert(number_of_added_UID);
//window.alert(number_of_places);
//window.alert(known_index);
//window.alert(number_of_places);
        if (known_index != 0 && number_of_places != 0)
        {

            for (person_index = 0; person_index < known_index; person_index++) // go over all persons
            {


                var ST_person = known_BT_detected[person_index][1]; // start time for meeting the person

                //window.alert(ST_person);

                var ET_person = known_BT_detected[person_index][2]; // end time for meeting the person
                var person_date = known_BT_detected[person_index][3]; // the date of meeting
                //window.alert(date_ST_person);
                //window.alert(date_ET_person);


                for (place_index = 0; place_index < number_of_places; place_index++) // go over all places
                {

                    var ST_place = places_detected[place_index][1]; // start time for being in a place
                    var ET_place = places_detected[place_index][2]; // end time for being in a place
                    var place_date = places_detected[place_index][3];

                    //window.alert(date_ST_place);
                    //window.alert(date_ET_place);
                    //document.writeln(place_index);
                    if (person_date == place_date) // the date should be the same
                    {

                        var difference = compare_time(ET_person, ST_place);
                        if (difference > 0) // If (ET_person < ST_place)
                        {
                            //Add info of the person  "With a Person? as activity and Break // don't check more places
                            //document.writeln('haloya');

                            if (known_BT_detected[person_index][5] != undefined) //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                            {
                                if (known_BT_detected[person_index][5] == 'yes') // the person is known
                                {
                                    activities[activity_index][0] = 'With' + ' ' + known_BT_detected[person_index][0];
                                    activities[activity_index][1] = ST_person;
                                    activities[activity_index][2] = ET_person;
                                    activities[activity_index][3] = known_BT_detected[person_index][3];
                                    activities[activity_index][4] = 'n/a'; // there is no place
                                    activities[activity_index][5] = known_BT_detected[person_index][0];
                                    activities[activity_index][6] = 'n/a';
                                    activities[activity_index][7] = known_BT_detected[person_index][6];
                                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                    activities[activity_index][8] = known_BT_detected[person_index][7]; // this will decide if the activity already reviewed


                                    activity_index = activity_index + 1;


                                    //document.write('there is one');
                                } // end od if the person is known
                            } // if (known_BT_detected[person_index][5] != undefined)

                            break;


                        }// end if (difference >0)

                        else // **
                        {
                            var difference = compare_time(ST_person, ET_place);
                            if (difference < 0) // if ST_person > ET_place
                            {
                                // move to the next place and continue
                                //document.write('ST_person > ET_place');
                                continue;

                            } // end If (ST_person > ET_place)

                            else // ** **
                            {
                                var difference = compare_time(ET_person, ET_place);
                                if (difference < 0) // if (ET_person > ET_place)
                                {

                                    //Add info "With a Person in a Place? as activity, Move to the next place;  Continue; // don't continue the rest of the instruction in this loop
                                    if (known_BT_detected[person_index][5] != undefined)
                                    {
                                        if (known_BT_detected[person_index][5] == 'yes') // the person is known
                                        {
                                            activities[activity_index][0] = places_detected[place_index][0] + ' ' + 'with' + ' ' + known_BT_detected[person_index][0];
                                            activities[activity_index][1] = ST_place;
                                            activities[activity_index][2] = ET_place;
                                            activities[activity_index][3] = known_BT_detected[person_index][3];
                                            activities[activity_index][4] = places_detected[place_index][0]; // this is the place
                                            activities[activity_index][5] = known_BT_detected[person_index][0]; // this is the person
                                            activities[activity_index][6] = places_detected[place_index][4]; // this is the path of the place image
                                            activities[activity_index][7] = known_BT_detected[person_index][6];// this is the path of the person image
                                            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                            activities[activity_index][8] = places_detected[place_index][5];// this will show if the activity is reviewed or not
                                            activity_index = activity_index + 1;
                                            //document.write('ET_person > ET_place');
                                        } // end od if the person is known
                                    }// end if (known_BT_detected[person_index][5] != undefined)

                                    // this next if happen when the meeting person time covering the whole place and not continuing for the next place. In this case we need to check and break
                                    if (place_index != number_of_places - 1) // this is to check that it is not the last place
                                    {
                                        if (ET_person < places_detected[place_index + 1][1]) // so we don't need to check more places
                                        {
                                            break;
                                        }
                                    }

                                    continue; // move to the next place


                                } // end If (ET_person > ET_place)

                                else // ** ** **
                                {
                                    //Add info "With a Person in a Place? as activity; and Break;
                                    if (known_BT_detected[person_index][5] != undefined)
                                    {
                                        if (known_BT_detected[person_index][5] == 'yes') // the person is known
                                        {
                                            activities[activity_index][0] = places_detected[place_index][0] + ' ' + 'with' + ' ' + known_BT_detected[person_index][0];
                                            activities[activity_index][1] = ST_place;
                                            activities[activity_index][2] = ET_place;
                                            activities[activity_index][3] = known_BT_detected[person_index][3];
                                            activities[activity_index][4] = places_detected[place_index][0]; // this is the place
                                            activities[activity_index][5] = known_BT_detected[person_index][0]; // this is the person
                                            activities[activity_index][6] = places_detected[place_index][4]; // this is the path of the place image
                                            activities[activity_index][7] = known_BT_detected[person_index][6];// this is the path of the person image
                                            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                            activities[activity_index][8] = places_detected[place_index][5];// this will show if the activity is reviewed or not
                                            activity_index = activity_index + 1;
                                            //document.write('the last option');
                                        } // end od if the person is known
                                    } // end if (known_BT_detected[person_index][5] != undefined)

                                    break;

                                } //end else ** ** **

                            } // end of the else **  **



                        } // end else **

                    } // if (person_date == place_date)

                }// end the inside for
//**********************************************************************************************************************
// this is to control the size of the array.. i put it -10 because there are other insertion next
                if (activity_index >= activity_help_index - 10)
                    break;

            } //end for (person_index=0; person_index < known_index; person_index++)




//////////////////
//////////////////


//Adding the activities "With a Person? where there were a detection of persons after the last detected place.

//Take the ET_place for the last detected place (all the detected persons after that place will be added as an activity "with a Person?)
//Take the ST_person of the first person and start comparing

//document.writeln(index);


//var ET_place = places_detected[number_of_places-1][2] ; // the ending time for the last place




// now we need to find all the activities with person where there were no detection for a place in the same day. we set ET_place as '' then we go and check if there were detection for places in that day. if there were detection, the value will change in the code to places_detected[j][2] (which is the end time of the place).... otherwise ET_place will be '' and the comparing in if will be always true and the activity will be added... this is will be implemented for all activities 'with a person' when there were no detection of a place... the detected persons before the first detection of a place will be covered in the previous loop up and we don't need to worry about it.. just persons after the last detection

            var ET_place = '';

            places_detected.sort(sortMultiDimensional); // this is to sort the array based on the second element (strat time)
            places_detected.sort(sortMultiDimensional_final); // this is to sort the array again based on the third element (date)


//window.alert(known_index);
            for (var i = 0; i < known_index; i++)
            {

                var ST_person = known_BT_detected[i][1];
                var ET_person = known_BT_detected[i][2];
                var date_perosn = known_BT_detected[i][3];

                for (var j = 0; j < number_of_places; j++)
                {
                    if (places_detected[j][3] == date_perosn) // compare if there is date.. if there are more than 1 date, ET_place will be overwritten
                    {
                        ET_place = places_detected[j][2];
                    }
                }

                //window.alert(ET_place);
                var already_added = 'no'; // to check if this activity already added before so we don't need to add again

                for (var y = 0; y < activity_index; y++)
                {
                    if (known_BT_detected[i][1] == activities[y][1] && known_BT_detected[i][0] == activities[y][5]) // the start date and the path are the same
                    {
                        already_added = 'yes';
                    }
                }


                var difference = compare_time(ST_person, ET_place);
                //window.alert(difference);

                if (difference < 0 && already_added == 'no') //if (ST_person > ET_place) && the activity is not added yet
                {
                    if (known_BT_detected[i][5] == 'yes') // the person is known
                    {
                        activities[activity_index][0] = 'With ' + ' ' + known_BT_detected[i][0];
                        activities[activity_index][1] = ST_person;
                        activities[activity_index][2] = ET_person;
                        activities[activity_index][3] = known_BT_detected[i][3];
                        activities[activity_index][4] = 'n/a'; // this is the place
                        activities[activity_index][5] = known_BT_detected[i][0]; // this is the person
                        activities[activity_index][6] = 'n/a'; // this is the path of the place image
                        activities[activity_index][7] = known_BT_detected[i][6];// this is the path of the person image
                        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                        activities[activity_index][8] = known_BT_detected[i][7];// this will show if the activity is reviewed or not
                        activity_index = activity_index + 1;
                    } // end od if the person is known

                }// end if (ST_person > ET_place)

                ET_place = '';
//**********************************************************************************************************************
// this is to control the size of the array.. i put it -6 because there are other insertion next
                if (activity_index >= activity_help_index - 10)
                    break;

            }// end for (var i=0; i<number_of_added_UID ; i++ )


//////////////////////////////
/////////////////////////////
// Adding the activities 'In a Place' where there were no detection of any person.

            for (place_index = 0; place_index < number_of_places; place_index++) // go over all places
            {

                var ST_place = places_detected[place_index][1]; // start time for being in a place
                var ET_place = places_detected[place_index][2]; // end time for being in a place
                var place_date = places_detected[place_index][3];

                for (person_index = 0; person_index < known_index; person_index++) // go over all persons
                {

                    var ST_person = known_BT_detected[person_index][1]; // start time for meeting the person
                    var ET_person = known_BT_detected[person_index][2]; // end time for meeting the person
                    var person_date = known_BT_detected[person_index][3];

                    var difference = compare_time(ET_place, ST_person);
                    if (difference > 0 && place_date == person_date) //ET_place < ST_person
                    {
                        activities[activity_index][0] = places_detected[place_index][0];
                        activities[activity_index][1] = ST_place;
                        activities[activity_index][2] = ET_place;
                        activities[activity_index][3] = places_detected[place_index][3]; // the date
                        activities[activity_index][4] = places_detected[place_index][0]; // this is the place
                        activities[activity_index][5] = 'n/a'; // this is the person
                        activities[activity_index][6] = places_detected[place_index][4]; // this is the path of the place image
                        activities[activity_index][7] = 'n/a';// this is the path of the person image
                        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                        activities[activity_index][8] = places_detected[place_index][5];// this will show if the activity is reviewed or not
                        activity_index = activity_index + 1;

                        break;
                    }// end ET_place < ST_person

                    else // **
                    {

                        var difference = compare_time(ST_place, ET_person);
                        if (difference < 0) //ST_place > ET_person
                        {
                            continue;
                        }
                        else
                        {
                            break;
                        } // end else
                    } // end else **


//**********************************************************************************************************************
// this is to control the size of the array.. i put it -6 because there are other insertion next
                    if (activity_index >= activity_help_index - 10)
                        break;


                }// end for the person
//**********************************************************************************************************************
// this is to control the size of the array.. i put it -6 because there are other insertion next
                if (activity_index >= activity_help_index - 10)
                    break;

            }//end for the places


///////////////////////////////////
///////////////////////////////////

//The previous loop will miss the places which are detected separately after the last detection a person.
//For that, we need to add the activities "In a Place? where there were places detected after the last detection of a person


            known_BT_detected.sort(sortMultiDimensional_person); // this is to sort the array based on ET_person so the last line will be the max

//var ET_person = known_BT_detected[known_index-1][2] ; // the ending time for the last place
//window.alert(ET_person);


            var ET_person = '';
            for (place_index = 0; place_index < number_of_places; place_index++) // go over all places
            {

                var ST_place = places_detected[place_index][1]; // start time for being in a place
                var ET_place = places_detected[place_index][2]; // end time for being in a place
                var date_place = places_detected[place_index][3];

                for (var j = 0; j < known_index; j++)
                {
                    if (known_BT_detected[j][3] == date_place) // compare if there is date.. if there are more than 1 date, ET_place will be overwritten
                    {
                        ET_person = known_BT_detected[j][2];
                    }
                }

                var already_added = 'no'; // to check if this activity already added before so we don't need to add again
                for (var i = 0; i < activity_index; i++)
                {
                    if (places_detected[place_index][1] == activities[i][1] && places_detected[place_index][0] == activities[i][4]) // the start time and the name are the same
                    {
                        already_added = 'yes';
                    }
                }

                var difference = compare_time(ST_place, ET_person);

                if (difference < 0 && already_added == 'no') //ST_place > ET_person && the activity was not added before
                {

                    activities[activity_index][0] = places_detected[place_index][0];
                    activities[activity_index][1] = ST_place;
                    activities[activity_index][2] = ET_place;
                    activities[activity_index][3] = places_detected[place_index][3]; // the date
                    activities[activity_index][4] = places_detected[place_index][0]; // this is the place
                    activities[activity_index][5] = 'n/a'; // this is the person
                    activities[activity_index][6] = places_detected[place_index][4]; // this is the path of the place image
                    activities[activity_index][7] = 'n/a';// this is the path of the person image
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    activities[activity_index][8] = places_detected[place_index][5];// this will show if the activity is reviewed or not
                    activity_index = activity_index + 1;


                } // end ST_place > ET_person
                ET_person = '';

                //**********************************************************************************************************************
// this is to control the size of the array.. i put it -6 because there are other insertion next
                if (activity_index >= activity_help_index - 10)
                    break;
            } // end for the places


        } // end if (known_index!=0 && number_of_places!=0)



        if (known_index == 0 && number_of_places == 0)
        {
//	document.write('there is no person and no place');


        } // end if (known_index==0 && number_of_places==0)

        else // **
        {

            if (known_index == 0) // this means that there are njo people
            {

                for (place_index = 0; place_index < number_of_places; place_index++) // go over all places
                {

                    var ST_place = places_detected[place_index][1]; // start time for being in a place
                    var ET_place = places_detected[place_index][2]; // end time for being in a place
                    activities[activity_index][0] = places_detected[place_index][0];
                    activities[activity_index][1] = ST_place;
                    activities[activity_index][2] = ET_place;
                    activities[activity_index][3] = places_detected[place_index][3]; // the date
                    activities[activity_index][4] = places_detected[place_index][0]; // this is the place
                    activities[activity_index][5] = 'n/a'; // this is the person
                    activities[activity_index][6] = places_detected[place_index][4]; // this is the path of the place image
                    activities[activity_index][7] = 'n/a';// this is the path of the person image
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    activities[activity_index][8] = places_detected[place_index][5];// this will show if the activity is reviewed or not
                    activity_index = activity_index + 1;

                    //**********************************************************************************************************************
// this is to control the size of the array.. i put it -6 because there are other insertion next
                    if (activity_index >= activity_help_index - 10)
                        break;

                } // end for the places


            }// end if (known_index==0)


            else //** **
            {
                if (number_of_places == 0) // this means there are no places)
                {


                    for (var i = 0; i < known_index; i++)
                    {

                        var ST_person = known_BT_detected[i][1];
                        var ET_person = known_BT_detected[i][2];
                        activities[activity_index][0] = 'With' + ' ' + known_BT_detected[i][0];
                        activities[activity_index][1] = ST_person;
                        activities[activity_index][2] = ET_person;
                        activities[activity_index][3] = known_BT_detected[i][3];
                        activities[activity_index][4] = 'n/a'; // this is the place
                        activities[activity_index][5] = known_BT_detected[i][0]; // this is the person
                        activities[activity_index][6] = 'n/a'; // this is the path of the place image
                        activities[activity_index][7] = known_BT_detected[i][6];// this is the path of the person image
                        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                        activities[activity_index][8] = known_BT_detected[i][7];// this will show if the activity is reviewed or not
                        activity_index = activity_index + 1;

//**********************************************************************************************************************
// this is to control the size of the array.. i put it -6 because there are other insertion next
                        if (activity_index >= activity_help_index - 10)
                            break;
                    }// end for (var i=0; i<known_index ; i++ )


                } // end if (number_of_places==0

            } // end else ** **

        } //end else **


        /*
         ////////////////////////////////
         ///////////////////////////////
         //printing
         //
         var j = "These are all the activities before filtering";
         j= j.bold();
         document.writeln(j);
         document.writeln("<br>");
         document.writeln("<br>");

         for (var b=0; b<activity_index; b++) // this is to display the saved UIDs
         {
         for (var j=0; j<9; j++)
         {document.writeln(activities[b][j]);}
         document.writeln("<br>");
         }// end of (var b=0; b<known_index; b++)
         //
         ///////////////////////////////////////
         ///////////////////////////////////////
         //
         document.writeln("<br>");
         document.writeln("<br>");
         document.writeln("<br>");

         */


        activities.sort(sortMultiDimensional);




// now we need to delete the activities which were repeated
// repeating activities could happen when you are with a person in a place and the system detect his prescense many times in the same place (BT come and go) so the activity in place with that person will be repeated many times in the array activities so we need to filter that and keep just one

        for (var b = 0; b < activity_index; b++) // this is to display the saved UIDs
        {
            var j = b + 1;
            while (j < activity_index)
            {
                if (activities[b][0] == activities[j][0] && activities[b][1] == activities[j][1] && activities[b][2] == activities[j][2] && activities[b][3] == activities[j][3])
                {

                    for (var k = j; k < activity_index - 1; k++)
                    {
                        for (var m = 0; m < 9; m++)
                        {
                            activities[k][m] = activities[k + 1][m];
                        }
                    }

                    activity_index = activity_index - 1;
                    continue;
                }// end if

                j++;
            }// end while
        }// end main for

//window.alert(number_of_places);
//window.alert(known_index);
//window.alert(activity_index);



/////////////////////////////
/////////////////////////////
//now we need to cluster again so one activity can be in one place with  different perosns
// each line will be name (In place, With person, In place with Persons), start, end, date, name_of_place, name1, namber_of_persons, name2....name12, number_of_images, image1_name, image1_time... image180 (maybe)




        clustering_index = 0; // this is the index for clustering_activities[][]

        if (activity_index != 0)
        {



// move the first line

            clustering_activities[0][0] = activities[0][0]; // the name of the activity
            clustering_activities[0][1] = activities[0][1]; // start time
            clustering_activities[0][2] = activities[0][2]; // end time
            clustering_activities[0][3] = activities[0][3]; // the date
            clustering_activities[0][4] = activities[0][4]; // this is the place
            clustering_activities[0][5] = activities[0][5]; // this is the person
            clustering_activities[0][2499] = activities[0][8]; // this is to show if it is reviewed before or not

            clustering_activities[0][18] = 0; // no images so far related to that activity
            if (activities[0][5] != 'n/a') // there is 1 person
            {
                clustering_activities[0][6] = 1;
            }
            else
            {
                clustering_activities[0][6] = 0;
            }


            var the_index; // this index will go over activities array

            for (the_index = 1; the_index < activity_index; the_index++) // go over all activities
            {

                var activity_name = activities[the_index][0]; // get the name of the activity
                var ST_activity = activities[the_index][1]; // start time of the activity
                var ET_activity = activities[the_index][2]; // end time of the activity
                var activity_date = activities[the_index][3]; // the date
                var activity_place = activities[the_index][4];// the name of the place
                var activity_person = activities[the_index][5]; //the name of the person
                var activity_review = activities[the_index][8];


                if (activity_place != 'n/a' && activity_person != 'n/a') //this means that the activity is In place With person
                {

                    if (clustering_activities[clustering_index][4] != 'n/a' && clustering_activities[clustering_index][5] != 'n/a' && clustering_activities[clustering_index][1] == ST_activity && clustering_activities[clustering_index][0] != activity_name && clustering_activities[clustering_index][3] == activity_date) // in this case we need to change the name of the activity
                    {
                        clustering_activities[clustering_index][0] = clustering_activities[clustering_index][0] + ',' + ' ' + activity_person; // add the name of the person to the previous name
                        var persons_number = clustering_activities[clustering_index][6]; // get how many people already added
                        var help = persons_number + 6; // define the index to add the new person
                        clustering_activities[clustering_index][help] = activity_person; // add the name of the new person
                        clustering_activities[clustering_index][6] = clustering_activities[clustering_index][6] + 1; // add the number of persons by one
                    } // end if
                    else // so just add the activity as it is
                    { // ** **
                        if (clustering_activities[clustering_index][0] != activity_name) // this is to aviod adding the same activity again
                        {

                            clustering_index = clustering_index + 1;
                            clustering_activities[clustering_index][0] = activity_name; // the name of the activity
                            clustering_activities[clustering_index][1] = ST_activity; // start time
                            clustering_activities[clustering_index][2] = ET_activity; // end time
                            clustering_activities[clustering_index][3] = activity_date; // the date
                            clustering_activities[clustering_index][4] = activity_place; // this is the place
                            clustering_activities[clustering_index][5] = activity_person; // this is the person
                            clustering_activities[clustering_index][2499] = activity_review; // this is to show if it is reviewed before or not

                            clustering_activities[clustering_index][18] = 0;
                            if (activity_person != 'n/a') // there is 1 person
                            {
                                clustering_activities[clustering_index][6] = 1;
                            }
                            else // ** ** **
                                clustering_activities[clustering_index][6] = 0;

                            // end else ** ** **

                        }// end if (clustering_activities[clustering_index][0]!= activity_name)

                    } //end else ** **
                } // end the main if

                else // *** *** *** // so just add the activity as it is
                {
                    clustering_index = clustering_index + 1;
                    clustering_activities[clustering_index][0] = activity_name; // the name of the activity
                    clustering_activities[clustering_index][1] = ST_activity; // start time
                    clustering_activities[clustering_index][2] = ET_activity; // end time
                    clustering_activities[clustering_index][3] = activity_date; // the date
                    clustering_activities[clustering_index][4] = activity_place; // this is the place
                    clustering_activities[clustering_index][5] = activity_person; // this is the person
                    clustering_activities[clustering_index][2499] = activity_review; // this is to show if it is reviewed before or not

                    clustering_activities[clustering_index][18] = 0;

                    if (activity_person != 'n/a') // there is 1 person
                    {
                        clustering_activities[clustering_index][6] = 1;
                    }
                    else // ** ** **
                    {
                        clustering_activities[clustering_index][6] = 0;

                    }// end else ** ** **

                } // end else *** *** ***


//**********************************************************************************************************************
// this is to control the size of the array.. i put it -6 because there are other insertion next
                if (clustering_index >= num_activities_index - 5)
                    break;

            }// end for (place_index=0; place_index < number_of_places; place_index++)





///////////////////////////////
////////////////////////////////
// now we need to sort the array based on time





            clustering_activities.sort(sortMultiDimensional); // this is to sort the array based on the second element (strat time)
            clustering_activities.sort(sortMultiDimensional_final);

            ++clustering_index; // we have to add 1 to clustering_index



// now the final result is in the array "clustering_activities" but we still need to add images, the number of images is in [18] then the images will be there 'the path'


            /*
             ////////////////////////////////
             ///////////////////////////////
             //printing

             var j = "These are all the activities";
             j= j.bold();
             document.writeln(j);
             document.writeln("<br>");
             document.writeln("<br>");

             for (var b=0; b<activity_index; b++) // this is to display the saved UIDs
             {
             for (var j=0; j<9; j++)
             {document.writeln(activities[b][j]);}
             document.writeln("<br>");
             }// end of (var b=0; b<known_index; b++)
             //
             ///////////////////////////////////////
             ///////////////////////////////////////
             //
             document.writeln("<br>");
             document.writeln("<br>");
             document.writeln("<br>");
             //////////////////////////////////////
             //////////////////////////////////////



             var j = "These are all the activities after clustering";
             j= j.bold();
             document.writeln(j);
             document.writeln("<br>");
             document.writeln("<br>");


             for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
             {
             for (var j=0; j<2500; j++)
             {document.writeln(clustering_activities[b][j]);}
             document.writeln("<br>");
             document.writeln("<br>");
             }// end of (var b=0; b<number_of_added_UID; b++)

             */

        } // end if (activity_index!=0)

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////


// now we need to look inside clustering activities array to cluster when there are some persons together

//ST_P1 = clustering_activities[1][1];
//ST_P1_added = Add_seconds(ST_P1,-1);
//window.alert(ST_P1);
//window.alert(ST_P1_added);


        final_index = 0;
        while (final_index < clustering_index)
        {
            if (clustering_activities[final_index][4] == 'n/a')
            {
                var ST_P1 = clustering_activities[final_index][1]; // get the start time
                var ET_P1 = clustering_activities[final_index][2]; // get the end time
                var person_date = clustering_activities[final_index][3];
                var the_help = final_index + 1;
                for (var i = the_help; i < clustering_index; i++)
                {

                    if (clustering_activities[i][4] == 'n/a' && clustering_activities[i][3] == person_date)
                    {

                        var ST_P2 = clustering_activities[i][1]; // get the start time
                        var ET_P2 = clustering_activities[i][2]; // get the end time
                        var difference = compare_time(ST_P2, ET_P1);

                        if (difference >= 0) //  if (ST_P2 <= ET_P1)
                        {

                            var difference = compare_time(ST_P2, ST_P1);
                            if (difference <= 0) //  if (ST_P2 >= ST_P1)
                            {
                                if (difference <= max_person) // 999999
                                {
                                    var difference = compare_time(ET_P2, ET_P1);
                                    if (difference >= 0) //if (ET_P2 < ET_P1)
                                    {
                                        if (difference <= max_person)// 8888888
                                                // this means that both persons should be combined in 1 activity because thr lod shows that the difference between starting and ending times are smaller than the given vale which decide when u spend time with a person
                                                        // P1 1_________________1
                                                                // P2  1_______________1   somthing like that
                                                                {
                                                                    var exist = 'no'; // this variable to check if the person is already added to activity so we don't need to add it again'
                                                                    if (clustering_activities[final_index][5] == clustering_activities[i][5])
                                                                    {
                                                                        exist = 'yes';
                                                                    }// end if
                                                                    else
                                                                    {
                                                                        var index_help = clustering_activities[final_index][6];
                                                                        for (var u = 1; u < index_help; u++)
                                                                        {
                                                                            if (clustering_activities[final_index][6 + u] == clustering_activities[i][5])
                                                                            {
                                                                                exist = 'yes';
                                                                            }

                                                                        }// end for
                                                                    }//end else

                                                                    //  update the activity name and add the person to the main activity if it doesn't excist'
                                                                    if (exist == 'no')
                                                                    {

                                                                        clustering_activities[final_index][0] = clustering_activities[final_index][0] + ', ' + clustering_activities[i][5];
                                                                        var index_help = clustering_activities[final_index][6] + 6; // to find where to add this new person
                                                                        clustering_activities[final_index][index_help] = clustering_activities[i][5];
                                                                        clustering_activities[final_index][6] = clustering_activities[final_index][6] + 1;


                                                                        // we need to delete the second activity so we move all activities 1 up
                                                                        for (var y = i; y < clustering_index - 1; y++)
                                                                        {
                                                                            for (var b = 0; b < 2500; b++)
                                                                            {
                                                                                clustering_activities[y][b] = clustering_activities[y + 1][b];
                                                                            }
                                                                        }
                                                                        clustering_index = clustering_index - 1; // because we deleted 1 row from the array
                                                                        //final_index = final_index -1; // because we deleted 1 line so we need to check from the same place or we don't need to add 1

                                                                    }// end  if (exist == 'no')

                                                                }// end if (difference <= max_person) 888888

                                                        else // else @@@@ this means that the difference by the ending time is greater than the givin value and we need to combine P2 within P1 and create new activity for P1
                                                                // P1 1_______________1
                                                                        // P2  1_______1
                                                                        {
                                                                            // we will take a copy of the data for P1

                                                                            var array_help = new Array(800);
                                                                            for (var k = 0; k < 2500; k++)
                                                                            {
                                                                                array_help[k] = clustering_activities[final_index][k];
                                                                            }

                                                                            // first update the activity name and add the person to the main activity

                                                                            var exist = 'no'; // this variable to check if the person is already added to activity so we don't need to add it again'
                                                                            if (clustering_activities[final_index][5] == clustering_activities[i][5])
                                                                            {
                                                                                exist = 'yes';
                                                                            }// end if
                                                                            else
                                                                            {
                                                                                var index_help = clustering_activities[final_index][6];
                                                                                for (var u = 1; u < index_help; u++)
                                                                                {
                                                                                    if (clustering_activities[final_index][6 + u] == clustering_activities[i][5])
                                                                                    {
                                                                                        exist = 'yes';
                                                                                    }

                                                                                }// end for
                                                                            }//end else



                                                                            if (exist == 'no')
                                                                            {

                                                                                clustering_activities[final_index][0] = clustering_activities[final_index][0] + ', ' + clustering_activities[i][5];
                                                                                var index_help = clustering_activities[final_index][6] + 6; // to find where to add this new person
                                                                                clustering_activities[final_index][index_help] = clustering_activities[i][5];
                                                                                clustering_activities[final_index][6] = clustering_activities[final_index][6] + 1;
                                                                                //clustering_activities[i][1] = Add_seconds(ET_P2,1);
                                                                                clustering_activities[final_index][2] = Add_seconds(ET_P2, -1);

                                                                                // now update activity2 to make it like 1 using the copy starting with ET_P2 and ending with


                                                                                for (var k = 0; k < 2500; k++)
                                                                                {
                                                                                    clustering_activities[i][k] = array_help[k];
                                                                                }
                                                                                clustering_activities[i][1] = ET_P2; // this is to start this activity 1 second after the previous one
                                                                                clustering_activities[i][2] = ET_P1;

                                                                            } // end if (exist == 'no')
                                                                            else // which means that the person already added and we need just to delete P2 without doing any changes
                                                                            {
                                                                                for (var y = i; y < clustering_index - 1; y++)
                                                                                {
                                                                                    for (var b = 0; b < 2500; b++)
                                                                                    {
                                                                                        clustering_activities[y][b] = clustering_activities[y + 1][b];
                                                                                    }
                                                                                }
                                                                                clustering_index = clustering_index - 1; // because we deleted 1 row from the array
                                                                                final_index = final_index - 1; // because we deleted 1 line so we need to check from the same place or we don't need to add 1
                                                                            }// end for inner else

                                                                        }// end else  @@@@

                                                            }//end if (ET_P2 < ET_P1)

                                                    else // **
                                                    {
                                                        var difference = compare_time(ET_P1, ET_P2);
                                                        if (difference <= max_person) //if (ET_P2 > ET_P1) 777777
                                                                // this means that the difference in time is less than the givin value and P1 andP2 should be combined together and 1 activity should be deleted
                                                                        // P1 1_________1
                                                                                // P2  1__________1 somthing like that

                                                                                {

                                                                                    // first update the activity name and add the person to the main activity
                                                                                    var exist = 'no'; // this variable to check if the person is already added to activity so we don't need to add it again'
                                                                                    if (clustering_activities[final_index][5] == clustering_activities[i][5])
                                                                                    {
                                                                                        exist = 'yes';
                                                                                    }// end if
                                                                                    else
                                                                                    {
                                                                                        var index_help = clustering_activities[final_index][6];
                                                                                        for (var u = 1; u < index_help; u++)
                                                                                        {
                                                                                            if (clustering_activities[final_index][6 + u] == clustering_activities[i][5])
                                                                                            {
                                                                                                exist = 'yes';
                                                                                            }

                                                                                        }// end for
                                                                                    }//end else

                                                                                    if (exist == 'no')
                                                                                    {
                                                                                        clustering_activities[final_index][0] = clustering_activities[final_index][0] + ', ' + clustering_activities[i][5];
                                                                                        var index_help = clustering_activities[final_index][6] + 6; // to find where to add this new person
                                                                                        clustering_activities[final_index][index_help] = clustering_activities[i][5];
                                                                                        clustering_activities[final_index][6] = clustering_activities[final_index][6] + 1;
                                                                                        clustering_activities[final_index][2] = clustering_activities[i][2];

                                                                                        // we need to delete the second activity so we move all activities 1 up
                                                                                        for (var y = i; y < clustering_index - 1; y++)
                                                                                        {
                                                                                            for (var b = 0; b < 2500; b++)
                                                                                            {
                                                                                                clustering_activities[y][b] = clustering_activities[y + 1][b];
                                                                                            }
                                                                                        }
                                                                                        clustering_index = clustering_index - 1; // because we deleted 1 row from the array
                                                                                        final_index = final_index - 1; // because we deleted 1 line so we need to check from the same place or we don't need to add 1

                                                                                    } // end  if (exist == 'no')

                                                                                    else // this means that the person already added before and we need just to delete the activity P2

                                                                                    {
                                                                                        for (var y = i; y < clustering_index - 1; y++)
                                                                                        {
                                                                                            for (var b = 0; b < 2500; b++)
                                                                                            {
                                                                                                clustering_activities[y][b] = clustering_activities[y + 1][b];
                                                                                            }
                                                                                        }
                                                                                        clustering_index = clustering_index - 1; // because we deleted 1 row from the array
                                                                                        final_index = final_index - 1; // because we deleted 1 line so we need to check from the same place or we don't need to add 1
                                                                                    }// end else

                                                                                }// end if if (ET_P2 > ET_P1) 777777
                                                                        else // ** ** this means that the differece by ending time is greater than the givin value and we need to add P2 to P1 and change P2 information
                                                                                // P1 1_________1
                                                                                        // P2  1______________1
                                                                                        {


                                                                                            var exist = 'no'; // this variable to check if the person is already added to activity so we don't need to add it again'
                                                                                            if (clustering_activities[final_index][5] == clustering_activities[i][5])
                                                                                            {
                                                                                                exist = 'yes';
                                                                                            }// end if
                                                                                            else
                                                                                            {
                                                                                                var index_help = clustering_activities[final_index][6];
                                                                                                for (var u = 1; u < index_help; u++)
                                                                                                {
                                                                                                    if (clustering_activities[final_index][6 + u] == clustering_activities[i][5])
                                                                                                    {
                                                                                                        exist = 'yes';
                                                                                                    }

                                                                                                }// end for
                                                                                            }//end else



                                                                                            if (exist == 'no')
                                                                                            {
                                                                                                clustering_activities[final_index][0] = clustering_activities[final_index][0] + ', ' + clustering_activities[i][5];
                                                                                                clustering_activities[final_index][2] = Add_seconds(ET_P1, -1);
                                                                                                var index_help = clustering_activities[final_index][6] + 6; // to find where to add this new person
                                                                                                clustering_activities[final_index][index_help] = clustering_activities[i][5];
                                                                                                clustering_activities[final_index][6] = clustering_activities[final_index][6] + 1;

                                                                                                // change the starting time of the other activity to ET_P1
                                                                                                clustering_activities[i][1] = ET_P1;
                                                                                                //clustering_activities[i][1] = Add_seconds(ET_P1,1); // this is to start this activity 1 second after the previous one

                                                                                            } // end  if (exist == 'no')
                                                                                            else // which means that the person is already added so we need just to update the information of P2
                                                                                            {
                                                                                                clustering_activities[final_index][2] = Add_seconds(ET_P1, -1);
                                                                                                clustering_activities[i][1] = ET_P1;
                                                                                                //clustering_activities[i][1] = Add_seconds(ET_P1,1);
                                                                                            }
                                                                                        }// end else ** **

                                                                            } // end else **


                                                                }// end if 999999

                                                        else // **** **** which means that the difference in the staring time is greater than the given value
                                                                // P1 1_________1
                                                                        // P2      1_____...whatever
                                                                        {

                                                                            var difference = compare_time(ET_P2, ET_P1);
                                                                            if (difference >= 0) //if (ET_P2 < ET_P1)
                                                                            {
                                                                                if (difference <= max_person) // in this case we add P1 to activity2 and we change ET_P1 to ST_P2
                                                                                        // P1 1_______________1
                                                                                                // P2       1________1 somthing like that
                                                                                                {
                                                                                                    var exist = 'no'; // this variable to check if the person is already added to activity so we don't need to add it again'
                                                                                                    if (clustering_activities[i][5] == clustering_activities[final_index][5])
                                                                                                    {
                                                                                                        exist = 'yes';
                                                                                                    }// end if
                                                                                                    else
                                                                                                    {
                                                                                                        var index_help = clustering_activities[i][6];
                                                                                                        for (var u = 1; u < index_help; u++)
                                                                                                        {
                                                                                                            if (clustering_activities[i][6 + u] == clustering_activities[final_index][5])
                                                                                                            {
                                                                                                                exist = 'yes';
                                                                                                            }

                                                                                                        }// end for
                                                                                                    }//end else

                                                                                                    if (exist == 'no')
                                                                                                    {
                                                                                                        clustering_activities[i][0] = clustering_activities[i][0] + ', ' + clustering_activities[final_index][5];
                                                                                                        var index_help = clustering_activities[i][6] + 6; // to find where to add this new person
                                                                                                        clustering_activities[i][index_help] = clustering_activities[final_index][5];
                                                                                                        clustering_activities[i][6] = clustering_activities[i][6] + 1;

                                                                                                        // change the starting time of the other activity to ET_P1

                                                                                                        clustering_activities[final_index][2] = Add_seconds(ST_P2, -1); // this is to end this activity 1 second before the next start
                                                                                                    }// end if (exist == 'no')
                                                                                                    else // just change the time for P1 because it was already added to P2
                                                                                                    {
                                                                                                        clustering_activities[final_index][2] = Add_seconds(ST_P2, -1);
                                                                                                    } //end else


                                                                                                } // end if (difference <= max_person)

                                                                                        else// %%% ths means that the difference is greater than the given value. the situation will look like
                                                                                                // P1 1__________________________________1
                                                                                                        // P2          1_____________1
                                                                                                        {

                                                                                                            var exist = 'no'; // this variable to check if the person is already added to activity so we don't need to add it again'
                                                                                                            if (clustering_activities[i][5] == clustering_activities[final_index][5])
                                                                                                            {
                                                                                                                exist = 'yes';
                                                                                                            }// end if
                                                                                                            else
                                                                                                            {
                                                                                                                var index_help = clustering_activities[i][6];
                                                                                                                for (var u = 1; u < index_help; u++)
                                                                                                                {
                                                                                                                    if (clustering_activities[i][6 + u] == clustering_activities[final_index][5])
                                                                                                                    {
                                                                                                                        exist = 'yes';
                                                                                                                    }

                                                                                                                }// end for
                                                                                                            }//end else

                                                                                                            if (exist == 'no')
                                                                                                            {
                                                                                                                // add P1 to activity 2
                                                                                                                clustering_activities[i][0] = clustering_activities[i][0] + ', ' + clustering_activities[final_index][5];
                                                                                                                clustering_activities[i][2] = Add_seconds(ET_P2, -1);
                                                                                                                var index_help = clustering_activities[i][6] + 6; // to find where to add this new person
                                                                                                                clustering_activities[i][index_help] = clustering_activities[final_index][5];
                                                                                                                clustering_activities[i][6] = clustering_activities[i][6] + 1;

                                                                                                                // add new activity to the end of the array with ET_P2 and ET_P1
                                                                                                                for (var t = 0; t < 2500; t++)
                                                                                                                {
                                                                                                                    clustering_activities[clustering_index][t] = clustering_activities[final_index][t];
                                                                                                                }
                                                                                                                //clustering_activities[clustering_index][1] = Add_seconds(ET_P2,-1);
                                                                                                                clustering_activities[clustering_index][1] = ET_P2;
                                                                                                                clustering_activities[clustering_index][2] = ET_P1;
                                                                                                                clustering_index = clustering_index + 1;


                                                                                                                // change ET_P1 to ST_P2
                                                                                                                clustering_activities[final_index][2] = Add_seconds(ST_P2, -1);

                                                                                                            }// end if (exist == 'no')
                                                                                                            else // which means it already exsit (P1 in P2) so we just need to do the change in P1 and add new activity but we need to change the end end time with minus 1 second for the already existing activity
                                                                                                            {
                                                                                                                // change the ending time
                                                                                                                clustering_activities[i][2] = Add_seconds(ET_P2, -1);

                                                                                                                // add new activity to the end of the array with ET_P2 and ET_P1
                                                                                                                for (var t = 0; t < 2500; t++)
                                                                                                                {
                                                                                                                    clustering_activities[clustering_index][t] = clustering_activities[final_index][t];
                                                                                                                }
                                                                                                                //clustering_activities[clustering_index][1] = Add_seconds(ET_P2,-1);
                                                                                                                clustering_activities[clustering_index][1] = ET_P2;
                                                                                                                clustering_activities[clustering_index][2] = ET_P1;
                                                                                                                clustering_index = clustering_index + 1;

                                                                                                                // change ET_P1 to ST_P2
                                                                                                                clustering_activities[final_index][2] = Add_seconds(ST_P2, -1);

                                                                                                            }// end else

                                                                                                        }// end else %%%

                                                                                            } // end if (difference >= 0) //if (ET_P2 < ET_P1)

                                                                                    else // @@ which means that P2 end after P1
                                                                                    {
                                                                                        var difference = compare_time(ET_P1, ET_P2);
                                                                                        if (difference <= max_person) //if (ET_P2 - ET_P1 <= max)
                                                                                                // P1 1_______________1
                                                                                                        // P2        1_________1 somthing like that
                                                                                                        {

                                                                                                            var exist = 'no'; // this variable to check if the person is already added to activity so we don't need to add it again'
                                                                                                            if (clustering_activities[i][5] == clustering_activities[final_index][5])
                                                                                                            {
                                                                                                                exist = 'yes';
                                                                                                            }// end if
                                                                                                            else
                                                                                                            {
                                                                                                                var index_help = clustering_activities[i][6];
                                                                                                                for (var u = 1; u < index_help; u++)
                                                                                                                {
                                                                                                                    if (clustering_activities[i][6 + u] == clustering_activities[final_index][5])
                                                                                                                    {
                                                                                                                        exist = 'yes';
                                                                                                                    }
                                                                                                                }// end for
                                                                                                            }//end else

                                                                                                            if (exist == 'no')
                                                                                                            {
                                                                                                                // add P1 to activity 2
                                                                                                                clustering_activities[i][0] = clustering_activities[i][0] + ', ' + clustering_activities[final_index][5];
                                                                                                                var index_help = clustering_activities[i][6] + 6; // to find where to add this new person
                                                                                                                clustering_activities[i][index_help] = clustering_activities[final_index][5];
                                                                                                                clustering_activities[i][6] = clustering_activities[i][6] + 1;

                                                                                                                // change ET_P1 to ST_P2
                                                                                                                clustering_activities[final_index][2] = Add_seconds(ST_P2, -1);
                                                                                                            } // end   if (exist == 'no')

                                                                                                            else// which means that the person is already added and we need just to make changes in P1
                                                                                                            {
                                                                                                                clustering_activities[final_index][2] = Add_seconds(ST_P2, -1);
                                                                                                            }// end else

                                                                                                        } // end if (ET_P2 - ET_P1 <= max) which means the the ending time of P2 is much more than the given value comparing to P1
                                                                                                else // $$$$
                                                                                                        //P1 1_______________1
                                                                                                                //P2        1_________________1 somthing like that
                                                                                                                {

                                                                                                                    var exist = 'no'; // this variable to check if the person is already added to activity so we don't need to add it again'
                                                                                                                    if (clustering_activities[i][5] == clustering_activities[final_index][5])
                                                                                                                    {
                                                                                                                        exist = 'yes';
                                                                                                                    }// end if
                                                                                                                    else
                                                                                                                    {
                                                                                                                        var index_help = clustering_activities[i][6];
                                                                                                                        for (var u = 1; u < index_help; u++)
                                                                                                                        {
                                                                                                                            if (clustering_activities[i][6 + u] == clustering_activities[final_index][5])
                                                                                                                            {
                                                                                                                                exist = 'yes';
                                                                                                                            }

                                                                                                                        }// end for
                                                                                                                    }//end else

                                                                                                                    if (exist == 'no')
                                                                                                                    {
                                                                                                                        // do 3 changes
                                                                                                                        clustering_activities[final_index][2] = Add_seconds(ST_P2, -1);
                                                                                                                        //clustering_activities[i][1] = Add_seconds(ET_P1,1);
                                                                                                                        clustering_activities[i][1] = ET_P1;

                                                                                                                        clustering_activities[clustering_index][0] = 'with' + ' ' + clustering_activities[final_index][5] + ', ' + clustering_activities[i][5];
                                                                                                                        clustering_activities[clustering_index][1] = ST_P2;
                                                                                                                        clustering_activities[clustering_index][2] = Add_seconds(ET_P1, -1);
                                                                                                                        clustering_activities[clustering_index][3] = clustering_activities[final_index][3];
                                                                                                                        clustering_activities[clustering_index][4] = 'n/a';
                                                                                                                        clustering_activities[clustering_index][5] = clustering_activities[final_index][5];
                                                                                                                        clustering_activities[clustering_index][6] = 2;
                                                                                                                        clustering_activities[clustering_index][7] = clustering_activities[i][5];

                                                                                                                        clustering_index = clustering_index + 1;

                                                                                                                    }// end if (exist == 'no')
                                                                                                                    else // which means its already added so we need to do some changes
                                                                                                                    {
                                                                                                                        clustering_activities[final_index][2] = Add_seconds(ST_P2, -1);
                                                                                                                        clustering_activities[i][2] = Add_seconds(ET_P1, -1);

                                                                                                                        clustering_activities[clustering_index][0] = 'with' + ' ' + clustering_activities[i][5];
                                                                                                                        //clustering_activities[clustering_index][1]= Add_seconds(ET_P1,1);
                                                                                                                        clustering_activities[clustering_index][1] = ET_P1;
                                                                                                                        clustering_activities[clustering_index][2] = ET_P2;
                                                                                                                        clustering_activities[clustering_index][3] = clustering_activities[i][3];
                                                                                                                        clustering_activities[clustering_index][4] = 'n/a';
                                                                                                                        clustering_activities[clustering_index][5] = clustering_activities[i][5];
                                                                                                                        clustering_activities[clustering_index][6] = 1;


                                                                                                                        clustering_index = clustering_index + 1;

                                                                                                                    }



                                                                                                                }// end else $$$$

                                                                                                    }// end else@@

                                                                                        }// end else **** ****





                                                                            }//  end if (ST_P2 >= ST_P1)

                                                                        }// end if (ST_P2 < ET_P1)
                                                            }// end  if (clustering_activities[i][4] == 'n/a' && clustering_activities[i][3] == person_date)

                                                            //window.alert(clustering_activities[i][0]);
                                                        }//end for (var i= the_help; i<clustering_index; i++ )

                                            } // end if (clustering_activities[final_index][4] == 'n/a')

                                    final_index++;
//**********************************************************************************************************************
// this is to control the size of the array.. i put it -6 because there are other insertion next
                                    if (clustering_index >= num_activities_index - 5)
                                        break;
                                }// end while (final_index < clustering_index)



                        /*
                         document.writeln(j);
                         document.writeln("<br>");
                         document.writeln("<br>");


                         var j = "These are all the activities after clustering";
                         j= j.bold();
                         document.writeln(j);
                         document.writeln("<br>");
                         document.writeln("<br>");


                         for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
                         {
                         for (var j=0; j<2500; j++)
                         {document.writeln(clustering_activities[b][j]);}
                         document.writeln("<br>");
                         document.writeln("<br>");
                         }// end of (var b=0; b<number_of_added_UID; b++)



                         */

//window.alert(clustering_index);


                        clustering_activities.sort(sortMultiDimensional); // this is to sort the array based on the second element (strat time)
                        clustering_activities.sort(sortMultiDimensional_final);

// now the final result is in the array "clustering_activities" but we still need to add images, the number of images is in [18] then the images will be there 'the path'
                        /*


                         var j = "These are all the activities after clustering with persons";
                         j= j.bold();
                         document.writeln(j);
                         document.writeln("<br>");
                         document.writeln("<br>");


                         for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
                         {
                         for (var j=0; j<2500; j++)
                         {document.writeln(clustering_activities[b][j]);}
                         document.writeln("<br>");
                         document.writeln("<br>");
                         }// end of (var b=0; b<number_of_added_UID; b++)

                         */
///////////////////////////////////////
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////


// this next loop is to fix a bug in the algorithm... when testing, some activities were repeated with the same starting and ending time but with different persons... because of lack of time, im putting extra loop to check if there are some activities with exactly same time and date, and if there is, the system will check the number of persons in each one and delete the one which it has less persons

//////////////////
//////////////////
/////////////////
/////////////////
/////////////////                BUG DOWN
///////////////////
/////////////////
//////////////////
////////////////

                        for (var b = 0; b < clustering_index - 1; b++) // this is to display the saved UIDs
                        {

                            if (clustering_activities[b][1] == clustering_activities[b + 1][1] && clustering_activities[b][2] == clustering_activities[b + 1][2] && clustering_activities[b][3] == clustering_activities[b + 1][3]) // that means that they are the same with different names
                            {


                                if (clustering_activities[b + 1][6] <= clustering_activities[b][6]) // check which one has more persons
                                {

                                    for (var i = b; i < clustering_index - 1; i++)
                                    {

                                        for (var j = 0; j < 2500; j++)
                                        {
                                            clustering_activities[i][j] = clustering_activities[i + 1][j];
                                        }// end second for

                                    }// end first for


                                }// end if (clustering_activities[b+1][6] >= clustering_activities[b][6])
                                else // we need to delete the second one because the first has more persons
                                {
                                    for (var i = b + 1; i < clustering_index - 1; i++)
                                    {

                                        for (var j = 0; j < 2500; j++)
                                        {
                                            clustering_activities[i][j] = clustering_activities[i + 1][j];
                                        }// end second for

                                    }// end first for

                                }// end else



                                clustering_index--;
                            } // end main if

                        } // end of the main for

//////////////////
//////////////////
/////////////////
/////////////////
/////////////////                BUG UP
///////////////////
/////////////////
//////////////////
////////////////

//window.alert(clustering_index);

// this next loop is also to fix a bug in the algorithm... when testing, some activities were repeated with the same ending time but with different persons... because of lack of time, im putting extra loop to check if there are some activities with exactly same time and date, and if there is, the system will check the number of persons in each one and delete the one which it has less persons

                        for (var b = 0; b < clustering_index - 1; b++) // this is to display the saved UIDs
                        {

                            if (clustering_activities[b][2] == clustering_activities[b + 1][2] && clustering_activities[b][3] == clustering_activities[b + 1][3]) // that means that they are the same with different names
                            {


                                if (clustering_activities[b + 1][6] <= clustering_activities[b][6]) // check which one has more persons
                                {

                                    for (var i = b; i < clustering_index - 1; i++)
                                    {

                                        for (var j = 0; j < 2500; j++)
                                        {
                                            clustering_activities[i][j] = clustering_activities[i + 1][j];
                                        }// end second for

                                    }// end first for


                                }// end if (clustering_activities[b+1][6] >= clustering_activities[b][6])
                                else // we need to delete the second one because the first has more persons
                                {
                                    for (var i = b + 1; i < clustering_index - 1; i++)
                                    {

                                        for (var j = 0; j < 2500; j++)
                                        {
                                            clustering_activities[i][j] = clustering_activities[i + 1][j];
                                        }// end second for

                                    }// end first for

                                }// end else



                                clustering_index--;
                            } // end main if

                        } // end of the main for


                        /*
                         for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
                         {
                         for (var j=0; j<100; j++)
                         {document.writeln(clustering_activities[b][j]);}
                         document.writeln("<br>");
                         document.writeln("<br>");
                         }// end of (var b=0; b<number_of_added_UID; b++)

                         */

// this next loop is also to fix a bug in the algorithm... when testing, some activities were divided into 2 different activities while they suppose to be one since they have the same name... the idea is to test the ending time of the activity and compare it to the time of the next one... if the title is the same and the difference is just 1 second, we should compine hem together


//window.alert(clustering_index);

                        for (var b = 0; b < clustering_index - 1; b++) // this is to display the saved UIDs
                        {
                            var help = Add_seconds(clustering_activities[b][2], 1); // add second to the ending time of the current activity
                            if (clustering_activities[b][0] == clustering_activities[b + 1][0] && help == clustering_activities[b + 1][1] && clustering_activities[b][3] == clustering_activities[b + 1][3]) // that means that they are the same with different names
                            {

                                //window.alert('bingoooooooo');
                                clustering_activities[b][2] = clustering_activities[b + 1][2]; // make the ending time of the first activity the same as the ending time of the next activity (combine them)
                                // now delete activity 2

                                for (var y = b + 1; y < clustering_index - 1; y++) // this is to display the saved UIDs
                                {
                                    for (var j = 0; j < 2500; j++)
                                    {
                                        clustering_activities[y][j] = clustering_activities[y + 1][j];
                                    }

                                }
                                clustering_index = clustering_index - 1;
                                b--;

                            }//end if
                        }// end main for


// this next loop is alsooooooooo to fix a bug in the algorithm... when testing, some activities might have ending time 1 second before starting time and this will happen when there is a detected person in the end of the BT logs with 0 repeating.. it is easy to fix in the algorithm but it is easier to add a loop and check so i don't mess up the code :)

                        /*
                         for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
                         {
                         var difference = Time_differenc(clustering_activities[b][1],clustering_activities[b][2]); // add second to the ending time of the current activity
                         //window.alert(difference);
                         if (difference <= 0) // which means that ending time is less than starting time
                         {

                         if (b == clustering_index-1) // which means that it is the last activity
                         {
                         clustering_index --;
                         }
                         else
                         {
                         }

                         }// end if (difference <= 0)

                         /*
                         if (clustering_activities[b][0] == clustering_activities[b+1][0] && help == clustering_activities[b+1][1] && clustering_activities[b][3] == clustering_activities[b+1][3]) // that means that they are the same with different names
                         {

                         //window.alert('bingoooooooo');
                         clustering_activities[b][2] = clustering_activities[b+1][2]; // make the ending time of the first activity the same as the ending time of the next activity (combine them)
                         // now delete activity 2

                         for (var y=b+1; y<clustering_index-1; y++) // this is to display the saved UIDs
                         {
                         for (var j=0; j<2500; j++)
                         {
                         clustering_activities[y][j] = clustering_activities[y+1][j];
                         }

                         }
                         clustering_index= clustering_index-1;
                         b--;

                         }//end if

                         }// end main for



                         */

// now i want to keep the previous array but change the name of persons an places to the image path

                        for (var b = 0; b < clustering_index; b++) // this is to display the saved UIDs
                        {
                            for (var j = 1; j < 2500; j++) // we start from 1 so we don't need to look for the first member which is the name of the activity... i do that because the name of the activity might be a name of a place, so the loop doesn't change it to the path image
                            {
                                for (var place_scan = 0; place_scan < number_of_places; place_scan++)
                                {
                                    if (places_detected[place_scan][0] == clustering_activities[b][j])
                                    {
                                        clustering_activities[b][j] = places_detected[place_scan][4];
                                    }
                                }

                                for (var person_scan = 0; person_scan < number_of_added_UID; person_scan++)
                                {
                                    if (BT_detected[person_scan][0] == clustering_activities[b][j])
                                    {
                                        clustering_activities[b][j] = BT_detected[person_scan][6];
                                    }
                                }

                            }
                        }// end of the main for

//////////////





                        /*
                         if (clustering_index!=0)
                         {
                         if (clustering_activities[clustering_index-1][2499] == 'yes')
                         {

                         clustering_index --;

                         }
                         }

                         */

                        /*

                         //this part is a try to combine activities when they are in the same day and have the same name and the difference between them is short (maybe 2 minutes)

                         for (var b=0; b<clustering_index-2; b++) // this is to display the saved UIDs
                         {

                         var ending_time_current = clustering_activities[b][2];
                         var starting_time_next = clustering_activities[b+1][1];
                         var difference = Time_differenc(ending_time_current,starting_time_next);
                         var name_current = clustering_activities[b][0];
                         var name_next = clustering_activities[b+1][0];
                         var date_current = clustering_activities[b][3];
                         var date_next = clustering_activities[b+1][3];
                         //window.alert(difference);
                         if (name_current == name_next && date_current == date_next && difference < 900) // that means that they are the same with different names
                         {

                         clustering_activities[b][2] = clustering_activities[b+1][2]; // make the ending time of the first one as ending time of the second one (combine them)
                         //window.alert(clustering_activities[b][2]);
                         for (var i=b+1; i< clustering_index-1; i++)
                         {
                         for (var j=0; j<2500 ; j++ )
                         {

                         clustering_activities[i][j] = clustering_activities[i+1][j];

                         }// end second for

                         }// end first for

                         clustering_index--;
                         b--;
                         } // end main if

                         } // end of the main for


                         */
// the next loop is to check the time between 2 activities. If the time is greater than a value, a new activity with unknown place and unknow persons will be created. the value is defined in variables.xml in "shortest_desired_activity_time_gaps". if the time is less, the gap will be merged with the previous activity

//////////////////
//////////////////
/////////////////
/////////////////
/////////////////                BUG DOWN
///////////////////
/////////////////
//////////////////
////////////////

                        if (clustering_index > 1) // this means that there are at least 2 activities and we can look for filling the gaps
                        {


                            for (var b = 0; b < clustering_index - 1; b++) // this is to display the saved UIDs
                            {

                                if (clustering_activities[b][3] == clustering_activities[b + 1][3]) //chech if they are in the same day
                                {

                                    var ending_time = clustering_activities[b][2]; // find ending time of the current activity
                                    var next_starting_time = clustering_activities[b + 1][1]; // find starting time of the next activity
                                    var difference = Time_differenc(ending_time, next_starting_time);
                                    //window.alert(difference);

                                    if (difference > shortest_desired_activity_time_gaps) // this means that we should create an activity between those 2 activities otherwise, we merge this gap with previous one
                                    {
                                        for (var i = clustering_index; i > b + 1; i--)
                                        {
                                            for (var j = 0; j < 2500; j++)
                                            {
                                                clustering_activities[i][j] = clustering_activities[i - 1][j];
                                            }
                                        }// end for (var i=b+1;i<clustering_index;i++)

                                        clustering_activities[b + 1][0] = 'Images ';
                                        clustering_activities[b + 1][1] = Add_seconds(ending_time, 1);
                                        clustering_activities[b + 1][2] = Add_seconds(next_starting_time, -1);
                                        clustering_activities[b + 1][3] = clustering_activities[b][3];
                                        clustering_activities[b + 1][4] = 'n/a'; // there is no place
                                        clustering_activities[b + 1][5] = 'n/a';
                                        clustering_activities[b + 1][6] = 0;
                                        clustering_activities[b + 1][2499] = 'no'; // i should work on it
                                        //clustering_activities[b+1][7] = known_BT_detected[person_index][6];
                                        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                        //clustering_activities[b+1][8] = known_BT_detected[person_index][7]; // this will decide if the activity already reviewed

                                        clustering_index++;
                                    }// end if (difference > time_for_new_activity)

                                    else //in this case, the gap is short, so we just merge it with the previous activity
                                    {

                                        var new_ending_time = Add_seconds(next_starting_time, -1); // find the new ending time of the activity based on starting time of the next one
                                        clustering_activities[b][2] = new_ending_time;
                                        //window.alert(clustering_index);

                                    } // end else

                                }// end if (clustering_activities[b][3] == clustering_activities[b+1][3])

//**********************************************************************************************************************
// this is to control the size of the array.. i put it -6 because there are other insertion next
                                if (clustering_index >= num_activities_index - 5)
                                    break;
                            }// end of the main for


                        } // end of the main if


//////////////////
//////////////////
/////////////////
/////////////////
/////////////////                BUG UP
///////////////////
/////////////////
//////////////////
////////////////

////////////////////////
////////////////////////
////////////////////////
//window.alert(clustering_index);

// the next loop is to check if 2 activities in a row has the same context (same subject). If the time is less than 7 minutes (or whatever) the both activities will be mergerd


                        if (clustering_index > 1) // this means that there are at least 2 activities to compare
                        {


                            for (var b = 0; b < clustering_index - 1; b++)
                            {

                                if (clustering_activities[b][3] == clustering_activities[b + 1][3] && clustering_activities[b][0] == clustering_activities[b + 1][0]) //chech if they are in the same day and have the same date
                                {

                                    var ending_time = clustering_activities[b][2]; // find ending time of the current activity
                                    var next_starting_time = clustering_activities[b + 1][1]; // find starting time of the next activity
                                    var difference = Time_differenc(ending_time, next_starting_time);
                                    //window.alert(difference);

                                    if (difference < shortest_desired_activity_time_gaps) // this means that we should merge them since they are the same activity with a small gap
                                    {
                                        clustering_activities[b][2] = clustering_activities[b + 1][2]; // expand the time of the first activity to cover the next one

                                        // delete the second activity
                                        for (var i = b + 1; i < clustering_index - 1; i++)
                                        {
                                            for (var j = 0; j < 2500; j++)
                                            {
                                                clustering_activities[i][j] = clustering_activities[i + 1][j];
                                            }
                                        }// end for (var i=b+1;i<clustering_index;i++)


                                        clustering_index--;
                                    }// end if (difference > time_for_new_activity)


                                }// end if (clustering_activities[b][3] == clustering_activities[b+1][3] && clustering_activities[b][0] == clustering_activities[b+1][0])


                            }// end of the main for


                        } // end of the main if















                        /*
                         for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
                         {
                         for (var j=0; j<2500; j++)
                         {document.writeln(clustering_activities[b][j]);}
                         document.writeln("<br>");
                         document.writeln("<br>");
                         }// end of (var b=0; b<number_of_added_UID; b++)
                         */

//window.alert(clustering_index);
//if (clustering_index!= 0) // if there are no activities, we don't need to check any image so all the next block doesn't need to be excuted
//{
                        xmlDoc4 = document.implementation.createDocument("", "", null); // this is for the images

                        var xmlFile4 = mediaimage_source; // this is for the images

                        xmlDoc4.load(xmlFile4); // this is for the images
                        xmlDoc4.onload = Process_activity6_main;
//}// end if


                    } // end of the function Process_activity2

                    function Process_activity6_main()
                    {


///////////////////////////////////////
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
// now its time for looking for images
// all the activities are saved in the array clustering_activities ... based on start and end time for each activity, we need to cluster images
// each line in images_groups will be activity_index, number_of_images_in_group, group_number, image1_name, image1_time, image2_name...., .... image98



                        var all_images = xmlDoc4.getElementsByTagName("image"); // get all the nodes with the name devices
                        var number_of_images = all_images.length; // get the number of the nodes

//**********************************************************************************************************************
// this is to control the size of the array.. i put it -6 because there are other insertion next
                        if (number_of_images >= num_images_index - 100)
                            number_of_images = num_images_index - 100;

                        number_of_days = 0;

// first i will create an array that has all the dataes of the days with the time of the first image and the time of the last image... the idea is to use this array to compare the activities to wach others and fill in all the gaps before, after and between the activities... the array called images_on_day[][] and the number of days (number of its rows) is  number_of_days (it is defined as global variable in index.php)

                        if (number_of_images > 1)
                        {

                            // initialize the date and the time
                            images_on_day[0][0] = all_images[0].getElementsByTagName("date")[0].firstChild.nodeValue;
                            images_on_day[0][1] = all_images[0].getElementsByTagName("time")[0].firstChild.nodeValue;

                            var i = 1;
                            //var index_of_day = 0;
                            var date_to_compare_with = images_on_day[0][0];


                            while (i < number_of_images - 1)
                            {

                                if (all_images[i].getElementsByTagName("date")[0].firstChild.nodeValue == date_to_compare_with) // this means that this image is taken in the same day and it is not important
                                {
                                    i++;
                                    continue;
                                }

                                images_on_day[number_of_days][2] = all_images[i - 1].getElementsByTagName("time")[0].firstChild.nodeValue; // put the time of the last taken image

                                // initialize again for the next day
                                images_on_day[number_of_days + 1][0] = all_images[i].getElementsByTagName("date")[0].firstChild.nodeValue;
                                images_on_day[number_of_days + 1][1] = all_images[i].getElementsByTagName("time")[0].firstChild.nodeValue;

                                date_to_compare_with = images_on_day[number_of_days + 1][0];
                                number_of_days++;

                                //**********************************************************************************************************************
// this is to control the size of the array.. i put it because there are other insertion next
                                if (number_of_days >= activity_help_index - 5)
                                    number_of_days = activity_help_index - 5;

                            }// end while

// the last value is undefined so we need to set it manually
                            images_on_day[number_of_days][2] = all_images[number_of_images - 1].getElementsByTagName("time")[0].firstChild.nodeValue;

                        }// end if (number_of_images > 1)







                        /*
                         for (var b=0; b<=number_of_days; b++) // this is to display the saved UIDs
                         {
                         for (var j=0; j<3; j++)
                         {document.writeln(images_on_day[b][j]);}
                         document.writeln("<br>");
                         document.writeln("<br>");
                         }// end of (var b=0; b<number_of_added_UID; b++)


                         document.writeln("<br>");
                         document.writeln("<br>");
                         */








                        unknow_activities_index = 0;
                        var i = 0; // this is the counter which will scan all the images


                        if (number_of_images > 1)
                        {

                            while (i < number_of_images - 1)
                            {
                                //here are the starting and ending time of the activity to add
                                var the_new_starting_time = 0;
                                var the_new_ending_time = 0;
                                var image_time = all_images[i].getElementsByTagName("time")[0].firstChild.nodeValue;
                                var image_date = all_images[i].getElementsByTagName("date")[0].firstChild.nodeValue;

                                // now put the starting time of the activity as the time of the first image
                                the_new_starting_time = image_time;
                                // now scan the images and compare the date and see if it is the same just go to the next one, otherwise set the ending time of the activity as the time of the image (it should be the last image taken in that day)
                                for (var b = i + 1; b < number_of_images; b++)
                                {

                                    var image_date_next = all_images[b].getElementsByTagName("date")[0].firstChild.nodeValue; // get the date to compare
                                    if (image_date_next == image_date)
                                    {
                                        continue; // don't do the rest of the loop, just jump to the next image
                                    }

                                    break;

                                } // end for (var b=i+1; b<number_of_images ;b++ )


                                the_new_ending_time = all_images[b - 1].getElementsByTagName("time")[0].firstChild.nodeValue; // because b is pointing to the image with another date

                                images_no_activity[unknow_activities_index][0] = the_new_starting_time;
                                images_no_activity[unknow_activities_index][1] = the_new_ending_time;
                                images_no_activity[unknow_activities_index][2] = image_date;
                                unknow_activities_index++;

                                i = b; // so we will continue checking from the current position
                                //**********************************************************************************************************************
// this is to control the size of the array.. i put it because there are other insertion next
                                if (unknow_activities_index >= num_activities_index - 5)
                                    unknow_activities_index = num_activities_index - 5;
                                //window.alert(the_new_starting_time);
                                //window.alert(the_new_ending_time);


                            }// end main while

                        } // end if (number_of_images > 1)

//window.alert(unknow_activities_index);

                        /*for (var b=0; b<unknow_activities_index; b++) // this is to display the saved UIDs
                         {
                         for (var j=0; j<3; j++)
                         {document.writeln(images_no_activity[b][j]);}
                         document.writeln("<br>");
                         document.writeln("<br>");
                         }// end of (var b=0; b<number_of_added_UID; b++)


                         document.writeln("<br>");
                         document.writeln("<br>");*/
// now we need to compare the array images_no_activity with clustering_activities to add the days where there were no detected activities

                        if (clustering_index == 0) // this means that we need to move all the array of unknown activities
                        {

                            for (var i = 0; i < unknow_activities_index; i++)
                            {

                                clustering_activities[i][0] = 'Images ';
                                clustering_activities[i][1] = images_no_activity[i][0]
                                clustering_activities[i][2] = images_no_activity[i][1]
                                clustering_activities[i][3] = images_no_activity[i][2]
                                clustering_activities[i][4] = 'n/a'; // there is no place
                                clustering_activities[i][5] = 'n/a';
                                clustering_activities[i][6] = 0;
                                clustering_activities[i][2499] = 'no'; // i should work on it
                                //clustering_activities[b+1][7] = known_BT_detected[person_index][6];
                                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                //clustering_activities[b+1][8] = known_BT_detected[person_index][7]; // this will decide if the activity already reviewed

                                clustering_index++;
                                //**********************************************************************************************************************
// this is to control the size of the array.. i put it because there are other insertion next
                                if (clustering_index >= num_activities_index - 5)
                                    clustering_index = num_activities_index - 5;
                            }// end for (var i=0; i<unknow_activities_index; i++)

                        }// end if (clustering_index == 0)
                        else // this means that there is at least 1 activity
                        {

                            for (var i = 0; i < unknow_activities_index; i++)
                            {

                                // compare the date of the unknown activity of the day with the last detected activity. If the date is gretaer, just add it to the end of the array


                                if (images_no_activity[i][2] > clustering_activities[clustering_index - 1][3])
                                {

                                    //window.alert(images_no_activity[i][2]);
                                    clustering_activities[clustering_index][0] = 'Images ';
                                    clustering_activities[clustering_index][1] = images_no_activity[i][0];
                                    clustering_activities[clustering_index][2] = images_no_activity[i][1];
                                    clustering_activities[clustering_index][3] = images_no_activity[i][2];
                                    clustering_activities[clustering_index][4] = 'n/a'; // there is no place
                                    clustering_activities[clustering_index][5] = 'n/a';
                                    clustering_activities[clustering_index][6] = 0;
                                    clustering_activities[clustering_index][2499] = 'no'; // i should work on it
                                    //clustering_activities[b+1][7] = known_BT_detected[person_index][6];
                                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                    //clustering_activities[b+1][8] = known_BT_detected[person_index][7]; // this will decide if the activity already reviewed

                                    clustering_index++;
                                    //**********************************************************************************************************************
// this is to control the size of the array.. i put it because there are other insertion next
                                    if (clustering_index >= num_activities_index - 5)
                                        clustering_index = num_activities_index - 5;
                                } // end if (images_no_activity[i][2] > clustering_activities[clustering_index-1][3])


                            }// end for (var i=0; i<unknow_activities_index; i++)

// all the activities that happen after the last detected activity are added, now we need to scan to see if there are some activities that happen before or in between
                            clustering_activities.sort(sortMultiDimensional); // this is to sort the array based on the second element (strat time)
                            clustering_activities.sort(sortMultiDimensional_final);


                            /*var j = "These are all the activities after clustering and adding images with name and time";
                             j= j.bold();
                             document.writeln(j);
                             document.writeln("<br>");
                             document.writeln("<br>");


                             for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
                             {
                             for (var j=0; j<2500; j++)
                             {document.writeln(clustering_activities[b][j]);}
                             document.writeln("<br>");
                             document.writeln("<br>");
                             }// end of (var b=0; b<number_of_added_UID; b++)


                             document.writeln("<br>");
                             document.writeln("<br>");*/
//window.alert(unknow_activities_index);
                            for (var i = 0; i < unknow_activities_index; i++) // pass over the unknown activities
                            {
//window.alert(images_no_activity[i][0]);
                                for (var b = 0; b < clustering_index; b++) // pass over the detected activities
                                {

                                    if (images_no_activity[i][2] == clustering_activities[b][3])
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        if (images_no_activity[i][2] < clustering_activities[b][3])
                                        {

                                            for (var k = clustering_index; k > b; k--)
                                            {
                                                for (var j = 0; j < 2500; j++)
                                                {
                                                    clustering_activities[k][j] = clustering_activities[k - 1][j];
                                                }
                                            }// end for (var i=b+1;i<clustering_index;i++)

                                            clustering_index++;

                                            clustering_activities[b][0] = 'Images ';
                                            clustering_activities[b][1] = images_no_activity[i][0];
                                            clustering_activities[b][2] = images_no_activity[i][1];
                                            clustering_activities[b][3] = images_no_activity[i][2];
                                            clustering_activities[b][4] = 'n/a'; // there is no place
                                            clustering_activities[b][5] = 'n/a';
                                            clustering_activities[b][6] = 0;
                                            clustering_activities[b][2499] = 'no'; // i should work on it
                                            //clustering_activities[b+1][7] = known_BT_detected[person_index][6];
                                            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                            //clustering_activities[b+1][8] = known_BT_detected[person_index][7]; // this will decide if the activity already reviewed


                                            //**********************************************************************************************************************
// this is to control the size of the array.. i put it because there are other insertion next
                                            if (clustering_index >= num_activities_index - 5)
                                                clustering_index = num_activities_index - 5;

                                            b = b - 1;

                                        }// end if (images_no_activity[i][2] < clustering_activities[b][3])

                                    } // end else

                                }// end for (var b=0; b<clustering_index ; b++)

                            }// end for (var i=0; i<unknow_activities_index; i++)

                            /*var j = "These are all the activities after clustering and adding images with name and time";
                             j= j.bold();
                             document.writeln(j);
                             document.writeln("<br>");
                             document.writeln("<br>");


                             for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
                             {
                             for (var j=0; j<2500; j++)
                             {document.writeln(clustering_activities[b][j]);}
                             document.writeln("<br>");
                             document.writeln("<br>");
                             }// end of (var b=0; b<number_of_added_UID; b++)


                             document.writeln("<br>");
                             document.writeln("<br>");*/





                        }// end of the main else

//window.alert('eee');
                        clustering_activities.sort(sortMultiDimensional); // this is to sort the array based on the second element (strat time)
                        clustering_activities.sort(sortMultiDimensional_final);

///////////////////
///////////////////
///////////////////
                        /*var j = "These are all the activities after clustering and adding images with name and time";
                         j= j.bold();
                         document.writeln(j);
                         document.writeln("<br>");
                         document.writeln("<br>");


                         for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
                         {
                         for (var j=0; j<2500; j++)
                         {document.writeln(clustering_activities[b][j]);}
                         document.writeln("<br>");
                         document.writeln("<br>");
                         }// end of (var b=0; b<number_of_added_UID; b++)


                         document.writeln("<br>");
                         document.writeln("<br>");
                         */
// this next loop is to create an activity for the images taken before the first detected activity for each day

//window.alert(clustering_activities[10][3]);

                        var index_for_days = 0; // this is the counter which will scan all the images

//window.alert(clustering_index);
                        while (index_for_days <= number_of_days) // for each day we will insert all the images before the start of it

                        {

// the next 2 variables are to save the strting and ending time of the activity which is intended to be added
                            var the_new_starting_time = 0;
                            var the_new_ending_time = 0;


                            if (number_of_images > 1) // we need to have at least one image to check
                            {

// this next loop check to find the first image taken in the same date of the first activity and before the starting time of that activity
                                for (var i = 0; i < clustering_index; i++)
                                {

                                    var today_date = images_on_day[index_for_days][0]; // get the date of the day from the array
                                    var today_start_time = images_on_day[index_for_days][1]; // get the strting time
                                    var first_activity_starting_time = clustering_activities[i][1]; //this is the starting time of the first activity
                                    var first_activity_starting_date = clustering_activities[i][3];
                                    if (index_for_days == 2)
                                    {
                                        //window.alert('sfsdfsdfs');
                                        //window.alert(i);
                                    }
                                    //var image_time = all_images[i].getElementsByTagName("time")[0].firstChild.nodeValue;
                                    //var image_date = all_images[i].getElementsByTagName("date")[0].firstChild.nodeValue;
                                    var difference = Time_differenc(today_start_time, first_activity_starting_time);
                                    //window.alert(difference);
                                    if (first_activity_starting_date == today_date)
                                    {
                                        if (difference > 0)
                                        {
                                            the_new_starting_time = today_start_time;
                                            //window.alert(the_new_starting_time);
                                            the_new_ending_time = Add_seconds(first_activity_starting_time, -1);
                                            //window.alert(the_new_starting_time);
                                            //window.alert(the_new_ending_time);
                                            break;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }



                                } // end for (var i=0; i<number_of_images; i++ )

                            } // end if (number_of_images>0)


// now the starting and ending time of activity we want to add are stored in the_new_starting_time and the_new_ending_time

// first check if the_new_starting_time != 0 which meanse that there is an image taken before the first activity
// then add the activity to the array if it is not 0

                            if (the_new_starting_time != 0)
                            {
                                // shift the activity forward 1 activity
                                for (var i = clustering_index; i > 0; i--)
                                {
                                    for (var j = 0; j < 2500; j++)
                                    {
                                        clustering_activities[i][j] = clustering_activities[i - 1][j];
                                    }
                                }// end for (var i=b+1;i<clustering_index;i++)


                                clustering_index++;

// now add the activity to the begining of the array
                                clustering_activities[0][0] = 'Images ';
                                clustering_activities[0][1] = the_new_starting_time;
                                clustering_activities[0][2] = the_new_ending_time;
                                clustering_activities[0][3] = today_date;
                                clustering_activities[0][4] = 'n/a'; // there is no place
                                clustering_activities[0][5] = 'n/a';
                                clustering_activities[0][6] = 0;
                                clustering_activities[0][2499] = 'no';
//window.alert('dsds');
//clustering_activities.sort(sortMultiDimensional); // this is to sort the array based on the second element (strat time)
//clustering_activities.sort(sortMultiDimensional_final);

                            }// end if (the_new_starting_time != 0)


//window.alert(index_for_days);
                            index_for_days++;
                        } // end main while


                        clustering_activities.sort(sortMultiDimensional); // this is to sort the array based on the second element (strat time)
                        clustering_activities.sort(sortMultiDimensional_final);
///////////////////////////////////
//////////////////////////////////
//////////////////////////////////
//////////////////////////////////


// this next loop is to create an activity for the images taken after the last detected activity

                        var index_for_days = number_of_days; // this is the counter which will scan all the images


                        while (index_for_days >= 0) // for each day we will insert all the images before the start of it

                        {

// the next 2 variables are to save the strting and ending time of the activity which is intended to be added
                            var the_new_starting_time = 0;
                            var the_new_ending_time = 0;


                            if (number_of_images > 1) // we need to have at least one image to check
                            {

// this next loop check to find the first image taken in the same date of the first activity and before the starting time of that activity
                                for (var i = clustering_index - 1; i >= 0; i--)
                                {

                                    var today_date = images_on_day[index_for_days][0]; // get the date of the day from the array
                                    var today_start_time = images_on_day[index_for_days][2]; // get the strting time
                                    var first_activity_starting_time = clustering_activities[i][2]; //this is the starting time of the first activity
                                    var first_activity_starting_date = clustering_activities[i][3];
                                    //var image_time = all_images[i].getElementsByTagName("time")[0].firstChild.nodeValue;
                                    //var image_date = all_images[i].getElementsByTagName("date")[0].firstChild.nodeValue;
                                    var difference = Time_differenc(today_start_time, first_activity_starting_time);
                                    if (first_activity_starting_date == today_date)
                                    {
                                        if (difference < 0)
                                        {
                                            the_new_starting_time = Add_seconds(first_activity_starting_time, 1);
                                            the_new_ending_time = today_start_time;
                                            //window.alert(the_new_starting_time);
                                            //window.alert(the_new_ending_time);
                                            break;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }



                                } // end for (var i=0; i<number_of_images; i++ )

                            } // end if (number_of_images>0)


// now the starting and ending time of activity we want to add are stored in the_new_starting_time and the_new_ending_time

// first check if the_new_starting_time != 0 which meanse that there is an image taken before the first activity
// then add the activity to the array if it is not 0

                            if (the_new_starting_time != 0)
                            {
                                // shift the activity forward 1 activity
                                /*	for (var i=clustering_index; i> 0 ;i--)
                                 {
                                 for (var j=0; j<2500; j++)
                                 {
                                 clustering_activities[i][j] = clustering_activities[i-1][j];
                                 }
                                 }// end for (var i=b+1;i<clustering_index;i++)*/




// now add the activity to the begining of the array
                                clustering_activities[clustering_index][0] = 'Images ';
                                clustering_activities[clustering_index][1] = the_new_starting_time;
                                clustering_activities[clustering_index][2] = the_new_ending_time;
                                clustering_activities[clustering_index][3] = today_date;
                                clustering_activities[clustering_index][4] = 'n/a'; // there is no place
                                clustering_activities[clustering_index][5] = 'n/a';
                                clustering_activities[clustering_index][6] = 0;
                                clustering_activities[clustering_index][2499] = 'no';
                                clustering_index++;
//**********************************************************************************************************************
// this is to control the size of the array.. i put it because there are other insertion next
                                if (clustering_index >= num_activities_index - 5)
                                    clustering_index = num_activities_index - 5;
                            }// end if (the_new_starting_time != 0)



                            index_for_days--;
                        } // end main while




                        clustering_activities.sort(sortMultiDimensional); // this is to sort the array based on the second element (strat time)
                        clustering_activities.sort(sortMultiDimensional_final);






                        /*









                         var last_activity_ending_time = clustering_activities[clustering_index-1][2]; //this is the starting time of the first activity
                         var last_activity_ending_date = clustering_activities[clustering_index-1][3];


                         // the next 2 variables are to save the strting and ending time of the activity which is intended to be added
                         var the_new_starting_time = 0;
                         var	the_new_ending_time = 0;


                         if (number_of_images>0) // we need to have at least one image to check
                         {

                         // this next loop check to find the first image taken in the same date of the first activity and before the starting time of that activity
                         for (var i=number_of_images-1; i>0; i-- )
                         {

                         var image_time = all_images[i].getElementsByTagName("time")[0].firstChild.nodeValue;
                         var image_date = all_images[i].getElementsByTagName("date")[0].firstChild.nodeValue;
                         var difference = Time_differenc(image_time,last_activity_ending_time);

                         if (last_activity_ending_date == image_date && difference < 0)
                         {

                         the_new_starting_time = Add_seconds(last_activity_ending_time,1);
                         the_new_ending_time = image_time;
                         //window.alert(the_new_starting_time);
                         //window.alert(the_new_ending_time);
                         break;
                         }



                         } // end for (var i=0; i<number_of_images; i++ )

                         } // end if (number_of_images>0)


                         // now the starting and ending time of activity we want to add are stored in the_new_starting_time and the_new_ending_time

                         // first check if the_new_starting_time != 0 which meanse that there is an image taken after the last activity
                         // then add the activity to the array if it is not 0

                         if (the_new_starting_time != 0)
                         {


                         // now add the activity to the begining of the array
                         clustering_activities[clustering_index][0] = ' ';
                         clustering_activities[clustering_index][1] = the_new_starting_time;
                         clustering_activities[clustering_index][2] = the_new_ending_time;
                         clustering_activities[clustering_index][3] = image_date;
                         clustering_activities[clustering_index][4] = 'n/a'; // there is no place
                         clustering_activities[clustering_index][5] = 'n/a';
                         clustering_activities[clustering_index][6] = 0;
                         clustering_activities[clustering_index][2499] = 'no';

                         clustering_index++;


                         }// end if (the_new_starting_time != 0)



                         */




                        var index = 0; // this index will move over the activities in clustering_activities array which has clustering_index lines
                        array_index = 0; // this index is the one which will move over images_groups array

                        while (index < clustering_index)
                        {

                            ST_activity = clustering_activities[index][1]; // find the start time
                            ET_activity = clustering_activities[index][2]; // find the end time
                            date_activity = clustering_activities[index][3]; // we might need it for later development
                            //document.writeln(ET_activity);

                            for (var i = 0; i < number_of_images; i++)
                            {
                                var image_time = all_images[i].getElementsByTagName("time")[0].firstChild.nodeValue; // the time
                                var image_name = all_images[i].getElementsByTagName("name")[0].firstChild.nodeValue; // the name
                                var image_date = all_images[i].getElementsByTagName("date")[0].firstChild.nodeValue; // the date




                                var S_difference = compare_time(image_time, ST_activity);
                                var E_difference = compare_time(image_time, ET_activity);

                                ///// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                ///// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  here we need to add the date

                                Group_id = all_images[i].attributes.getNamedItem("groupID").nodeValue; // group id for comparing the similarty
                                //window.alert(Group_id);
                                //document.writeln(image_name);

                                if (S_difference <= 0 && E_difference >= 0 && image_date == date_activity) // the image is taken during the activity
                                        ///// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                                                ///// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  here we need to add the date inside if
                                                {

                                                    //document.writeln(index);
                                                    //document.writeln(image_time);

                                                    if (array_index == 0)
                                                    {

                                                        images_groups[array_index][0] = index;
                                                        images_groups[array_index][1] = 1;
                                                        images_groups[array_index][2] = Group_id;
                                                        images_groups[array_index][3] = image_name;
                                                        images_groups[array_index][4] = image_time;
                                                        array_index = array_index + 1;

                                                    }// end if (array_index == 0)

                                                    else //**
                                                    {
                                                        if (Group_id == images_groups[array_index - 1][2]) // in this case we need to update the line without changing the array_index
                                                        {

                                                            images_groups[array_index - 1][1] = images_groups[array_index - 1][1] + 1; // there is one more image in this line
                                                            var the_number = images_groups[array_index - 1][1] * 2 + 1;
                                                            images_groups[array_index - 1][the_number] = image_name; // to add the name of the image
                                                            images_groups[array_index - 1][the_number + 1] = image_time; // to add the name of the image

                                                        }// end if (Group_id == images_groups[array_index-1][2])

                                                        else // ** **
                                                        {

                                                            images_groups[array_index][0] = index;
                                                            images_groups[array_index][1] = 1;
                                                            images_groups[array_index][2] = Group_id;
                                                            images_groups[array_index][3] = image_name;
                                                            images_groups[array_index][4] = image_time;
                                                            array_index = array_index + 1;

                                                        } // end else ** **



                                                    } //end else**



                                                } // end if (image_time >= ST_activity && image_time <= ET_activity)

//**********************************************************************************************************************
// this is to control the size of the array.. i put it because there are other insertion next
                                        if (array_index >= num_images_index - 100)
                                            array_index = num_images_index - 100;


                                    }// end for (var i=0; i<number_of_images; i++ )


                            index = index + 1;
//document.write('sdsdsdsd');
                        }// end main while


// printing images_groups

                        /*
                         var j = "These are all images organized in groups with activities ID";
                         j= j.bold();
                         document.writeln("<br>");
                         document.writeln(j);
                         document.writeln("<br>");
                         document.writeln("<br>");

                         for (var b=0; b<array_index; b++)
                         {
                         for (var j=0; j<12; j++)
                         {document.writeln(images_groups[b][j]);}
                         document.writeln("<br>");
                         }// end of (var b=0; b<known_index; b++)



                         document.writeln("<br>");
                         document.writeln("<br>");


                         */

///////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////


// now we need to cluster  images_groups array again so each line will contain the activity_index, number_of_images, image1_name, image1_time ..... image200 (maybe)... the clustering will be based on images_groups with array_index lines. If there is a group with number of similar images, we will pick up just the middle one.
// the result will be in array called final_images with clustering_index lines (the number of the activities)

// first set all the indexes with 0 images
                        for (var i = 0; i < clustering_index; i++)
                        {
                            final_images[i][0] = i; // add the index of the activity
                            final_images[i][1] = 0; // put 0 as number of images

                        }// end for


// now we need to add images to activities

                        for (var i = 0; i < clustering_index; i++)
                        {

                            for (var y = 0; y < array_index; y++)
                            {

                                var the_activiy_index = images_groups[y][0];
                                if (the_activiy_index == i)
                                {

                                    var the_images_number = images_groups[y][1];

                                    if (the_images_number == 1) // there is only one image
                                    {

                                        final_images[i][1] = final_images[i][1] + 1; // add the number of images by 1
                                        var insert_index = 2 * final_images[i][1]; // this is the index to add
                                        final_images[i][insert_index] = images_groups[y][3]; // this is the name of the image
                                        final_images[i][insert_index + 1] = images_groups[y][4]; // this is the time of the image

                                    }// end if (the_images_number == 1)

                                    else // there are more than one images (there are similrity)
                                    {

                                        var image_to_add; // we need to find the index based on the number of images
                                        if (the_images_number % 2 == 0)
                                        {
                                            image_to_add = the_images_number + 1;
                                        }
                                        else
                                        {
                                            image_to_add = the_images_number + 2;
                                        }
                                        final_images[i][1] = final_images[i][1] + 1; // add the number of images by 1
                                        var insert_index = 2 * final_images[i][1]; // this is the index to add
                                        final_images[i][insert_index] = images_groups[y][image_to_add]; // this is the name of the image
                                        final_images[i][insert_index + 1] = images_groups[y][image_to_add + 1]; // this is the time of the image


                                    }// end else


                                }// end if (the_activiy_index == i)


                            }// end for (var y=0; y<array_index; y++)


                        }// end for (var i=0; i<clustering_index; i++

//************************************************************************************************************************
// the next loop is control that an activity doesn't have more than 1200 images (for example)
                        for (var b = 0; b < clustering_index; b++)
                        {
                            if (final_images[b][1] > num_img_activity)
                                final_images[b][1] = num_img_activity;

                        }// end of (var b=0; b<known_index; b++)

//window.alert(clustering_index);

                        /*

                         var j = "Here are all the activities indexs with the images related to them after removing similar images";
                         j= j.bold();

                         document.writeln(j);
                         document.writeln("<br>");
                         document.writeln("<br>");

                         for (var b=0; b<clustering_index; b++)
                         {
                         for (var j=0; j<12; j++)
                         {document.writeln(final_images[b][j]);}
                         document.writeln("<br>");
                         }// end of (var b=0; b<known_index; b++)



                         document.writeln("<br>");
                         document.writeln("<br>");
                         document.writeln("<br>");
                         */

///////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////


// now we need to combine final_images[][] array (clustering_index length) with clustering_activities[][] array which has the same length
// the index to start inserting in clustering_activities is [19] where we can put the name of the first image, then the time of it and so on. after finishing inserting we have to change the index [18] to the number of images in that activity


                        for (var i = 0; i < clustering_index; i++)
                        {

                            var images_to_insert = final_images[i][1];

                            clustering_activities[i][18] = images_to_insert;
                            images_to_insert = images_to_insert * 2;


                            if (images_to_insert != '0')
                            {
                                //window.alert('bingo');
                                for (var y = 0; y < images_to_insert; y++)
                                {

                                    clustering_activities[i][19 + y] = final_images[i][2 + y];

                                }// end for (var y=0; y<images_to_insert ; y++ )

                            }// end if

                        }// end for (var i=0; i<clustering_index; i++)



// printing the final result

                        /*
                         var j = "These are all the activities after clustering and adding images with name and time";
                         j= j.bold();
                         document.writeln(j);
                         document.writeln("<br>");
                         document.writeln("<br>");


                         for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
                         {
                         for (var j=0; j<2500; j++)
                         {document.writeln(clustering_activities[b][j]);}
                         document.writeln("<br>");
                         document.writeln("<br>");
                         }// end of (var b=0; b<number_of_added_UID; b++)


                         document.writeln("<br>");
                         document.writeln("<br>");
                         */

///////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

//now we have all the results in the array clustering_activities. to know which activities are reviewed and which are not, i will scan all the array and take the staring time of the activity when there were just persons (there is no conflict with places since 2 places will not be at the same time). for each activity, i will look inside BT log and compare the staring time with the time of the log itself. if the time is the same or greater, i will quite the loop and look at the review node in the log.. it will slso chack if there are only images so the algorithem searches in the images.xml file instead



                        xmlDoc = document.implementation.createDocument("", "", null);
                        var xmlFile = bt_source;
                        xmlDoc.load(xmlFile);
                        xmlDoc.onload = Process_activity8_main;

                    } // end od the main function

                    function Process_activity8_main()

                    {



                        for (var b = 0; b < clustering_index; b++) // this is to display the saved UIDs
                        {
//window.alert(clustering_activities[b][0]);
                            if (clustering_activities[b][4] == 'n/a') // that means that there is no place and there are just persons
                            {

                                var activity_starting_time = clustering_activities[b][1];
                                var activity_date = clustering_activities[b][3];

                                // now open the BT log and look for the right log

                                var devices = xmlDoc.getElementsByTagName("Devices"); // get all the nodes with the name devices
                                var number_of_devices = devices.length; // get the number of the nodes
                                //window.alert(number_of_devices);

                                for (var i = 0; i < number_of_devices; i++) // this loop is to go over all the devices node. For each node, we will check how many times repeated and then get the date time //and UIDs
                                {
                                    //window.alert(number_of_devices);
                                    var repeated = devices[i].attributes.getNamedItem("repeat").nodeValue; // find the repeating attribute for each node
                                    var Date_time = devices[i].attributes.getNamedItem("scanTime").nodeValue; // find the time and date attribute for each node
                                    var newDate_time = '20' + Date_time; // to make it look like 2009-02-16
                                    var splitting = newDate_time.split(" "); // split when there is a space
                                    var the_date = splitting[0]; // get the date
                                    var the_time = splitting[1]; // get the time
                                    var reviewed = devices[i].getElementsByTagName("reviewed")[0].firstChild.nodeValue;
                                    var difference = Time_differenc(activity_starting_time, the_time); // compare the starting time of the activity with the time of the log
                                    //window.alert(activity_starting_time + ' ' +difference + ' '+the_time);
                                    if (difference >= 0 && activity_date == the_date)
                                    {
                                        //window.alert(the_time);
                                        break;
                                    }

                                    //var UIDs = devices[i].getElementsByTagName("UID");
                                    //var number_of_UID = UIDs.length; // find how many UIDs inside each node of devices

                                }// end for (var i=0; i<number_of_devices; i++)
                                //window.alert(the_date);
                                //window.alert(activity_date);
                                //window.alert(reviewed);
                                //reviewed = devices[i+1].getElementsByTagName("reviewed")[0].firstChild.nodeValue;
                                clustering_activities[b][2499] = reviewed;

                            }// end if (clustering_activities[b][4] == 'n/a')

                        }// end for (var b=0; b<clustering_index-1; b++)

                        /*
                         for (var i=0; i<clustering_index; i++ )
                         {
                         window.alert(clustering_activities[i][2499]);
                         }
                         */
// now we have all the results in the array clustering_activities, we need to keep just the un_reviewed activities and delelte the reviewd activities

                        xmlDoc2 = document.implementation.createDocument("", "", null);
                        var xmlFile2 = mediaimage_source;
                        xmlDoc2.load(xmlFile2);
                        xmlDoc2.onload = Process_activity1118_main;
                    }

                    function Process_activity1118_main()

                    {

                        for (var b = 0; b < clustering_index; b++) // this is to display the saved UIDs
                        {

                            if (clustering_activities[b][4] == 'n/a' && clustering_activities[b][5] == 'n/a') // that means that there is no place and there are just persons
                            {

                                var activity_starting_time = clustering_activities[b][1];
                                var activity_date = clustering_activities[b][3];

                                // now open the BT log and look for the right log

                                var images = xmlDoc2.getElementsByTagName("image"); // get all the nodes with the name devices
                                var number_of_images = images.length; // get the number of the nodes
                                //window.alert(number_of_pos);

                                for (var i = 0; i < number_of_images; i++) // this loop is to go over all the devices node. For each node, we will check how many times repeated and then get the date time //and UIDs
                                {
                                    //window.alert(number_of_devices);
                                    //var repeated = devices[i].attributes.getNamedItem("repeat").nodeValue; // find the repeating attribute for each node
                                    //var Date_time = positions[i].attributes.getNamedItem("posTime").nodeValue; // find the time and date attribute for each node
//			var newDate_time = '20' + Date_time; // to make it look like 2009-02-16
//			var splitting = newDate_time.split(" "); // split when there is a space
//			var the_date = splitting[0]; // get the date
//			var the_time = splitting[1]; // get the time

                                    var the_date = images[i].getElementsByTagName("date")[0].firstChild.nodeValue; // get the date
                                    var the_time = images[i].getElementsByTagName("time")[0].firstChild.nodeValue; // get the time
                                    var reviewed = images[i].getElementsByTagName("reviewed")[0].firstChild.nodeValue;
                                    var difference = Time_differenc(activity_starting_time, the_time); // compare the starting time of the activity with the time of the log
                                    //window.alert(activity_starting_time + ' ' +difference + ' '+the_time);
                                    if (difference >= 0 && activity_date == the_date)
                                    {
                                        break;
                                    }

                                    //var UIDs = devices[i].getElementsByTagName("UID");
                                    //var number_of_UID = UIDs.length; // find how many UIDs inside each node of devices

                                }// end for (var i=0; i<number_of_devices; i++)

                                //window.alert(reviewed);
                                clustering_activities[b][2499] = reviewed;

                            }// end if (clustering_activities[b][4] == 'n/a')

                        }// end for (var b=0; b<clustering_index-1; b++)

                        xmlDoc2 = document.implementation.createDocument("", "", null);
                        var xmlFile2 = gps_source;
                        xmlDoc2.load(xmlFile2);
                        xmlDoc2.onload = Process_activity118_main;

                    } // end od the main function

                    function Process_activity118_main()

                    {


                        for (var b = 0; b < clustering_index; b++) // this is to display the saved UIDs
                        {

                            if (clustering_activities[b][4] != 'n/a') // that means that there is no place and there are just persons
                            {

                                var activity_starting_time = clustering_activities[b][1];
                                var activity_date = clustering_activities[b][3];

                                // now open the BT log and look for the right log

                                var positions = xmlDoc2.getElementsByTagName("pos"); // get all the nodes with the name devices
                                var number_of_pos = positions.length; // get the number of the nodes
                                //window.alert(number_of_pos);

                                for (var i = 0; i < number_of_pos; i++) // this loop is to go over all the devices node. For each node, we will check how many times repeated and then get the date time //and UIDs
                                {
                                    //window.alert(number_of_devices);
                                    //var repeated = devices[i].attributes.getNamedItem("repeat").nodeValue; // find the repeating attribute for each node
                                    var Date_time = positions[i].attributes.getNamedItem("posTime").nodeValue; // find the time and date attribute for each node
                                    var newDate_time = '20' + Date_time; // to make it look like 2009-02-16
                                    var splitting = newDate_time.split(" "); // split when there is a space
                                    var the_date = splitting[0]; // get the date
                                    var the_time = splitting[1]; // get the time
                                    var reviewed = positions[i].getElementsByTagName("reviewed")[0].firstChild.nodeValue;
                                    var difference = Time_differenc(activity_starting_time, the_time); // compare the starting time of the activity with the time of the log
                                    //window.alert(activity_starting_time + ' ' +difference + ' '+the_time);
                                    if (difference >= 0 && activity_date == the_date)
                                    {
                                        break;
                                    }

                                    //var UIDs = devices[i].getElementsByTagName("UID");
                                    //var number_of_UID = UIDs.length; // find how many UIDs inside each node of devices

                                }// end for (var i=0; i<number_of_devices; i++)

                                //window.alert(reviewed);
                                clustering_activities[b][2499] = reviewed;

                            }// end if (clustering_activities[b][4] == 'n/a')

                        }// end for (var b=0; b<clustering_index-1; b++)


// in some cases, the algorithm shows by mistake activity where the ending time and the staring time are equal or with 1 second difference... we need to delete those activiies
                        for (var b = 0; b < clustering_index; b++) // this is to display the saved UIDs
                        {
                            start_time = clustering_activities[b][1];
                            end_time = clustering_activities[b][2];

                            var difference = Time_differenc(start_time, end_time); // compare the starting time of the activity with the time of the log
                            //window.alert(difference);

                            if (difference <= 0) // which means that the activity should be deleted
                            {
                                if (b == clustering_index - 1) // which means it is the last activity and we just need to decrease the index
                                {
                                    clustering_index--;
                                }
                                else
                                {

                                    for (var i = b; i < clustering_index - 1; i++)
                                    {

                                        for (var j = 0; j < 2500; j++)
                                        {

                                            clustering_activities[i][j] = clustering_activities[i + 1][j];

                                        }// end second for

                                    }// end first for

                                    clustering_index--;
                                    b--;


                                }// end else

                            }// end if (difference <= 0)


                        } // end of the main for


////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

                        /*
                         var j = "These are all the activities after clustering and adding images with name and time";
                         j= j.bold();
                         document.writeln(j);
                         document.writeln("<br>");
                         document.writeln("<br>");


                         for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
                         {
                         for (var j=0; j<2500; j++)
                         {document.writeln(clustering_activities[b][j]);}
                         document.writeln("<br>");
                         document.writeln("<br>");
                         }// end of (var b=0; b<number_of_added_UID; b++)


                         document.writeln("<br>");
                         document.writeln("<br>");

                         */


// the next loop is to check if an activity is unknown and it has less than 2 ('min_image_unknown')images, we delete it


                        for (var b = 0; b < clustering_index; b++) // this is to display the saved UIDs
                        {

                            var name_of_activity = clustering_activities[b][0];
                            var num_of_images = clustering_activities[b][18];
                            if (name_of_activity == ' ' && num_of_images < min_image_unknown)
                            {
                                for (var i = b; i < clustering_index - 1; i++)
                                {

                                    for (var j = 0; j < 2500; j++)
                                    {

                                        clustering_activities[i][j] = clustering_activities[i + 1][j];

                                    }// end second for

                                }// end first for

                                clustering_index--;
                                b--;

                            }// end if (name_of_activity == ' ' && num_of_images < min_image_unknown)


                        }// end for (var b=0; b<clustering_index-1; b++)


//////////////////////////////
/////////////////////////////
////////////////////////////
/////////////////////////////

// now we want to check if there is no person and no place and there are no images, we delete the activity

                        for (var b = 0; b < clustering_index - 1; b++) // this is to display the saved UIDs
                        {

                            if (clustering_activities[b][4] == 'n/a' && clustering_activities[b][5] == 'n/a' && clustering_activities[b][18] == 0) // that means that they are the same with different names
                            {

                                for (var i = b; i < clustering_index - 1; i++)
                                {

                                    for (var j = 0; j < 2500; j++)
                                    {

                                        clustering_activities[i][j] = clustering_activities[i + 1][j];

                                    }// end second for

                                }// end first for

                                clustering_index--;
                                b--;
                            } // end main if

                        } // end of the main for


/////////////////////////////
/////////////////////////////
//////


// this next loop is also to fix a bug in the algorithm... when testing, some activities were divided into 2 different activities while they suppose to be one since they have the same name... the idea is to test the ending time of the activity and compare it to the time of the next one... if the title is the same and the difference is just 1 second, we should compine hem together


//window.alert(clustering_index);

                        for (var b = 0; b < clustering_index - 1; b++) // this is to display the saved UIDs
                        {
                            var help = Add_seconds(clustering_activities[b][2], 1); // add second to the ending time of the current activity
                            if (clustering_activities[b][0] == clustering_activities[b + 1][0] && help == clustering_activities[b + 1][1] && clustering_activities[b][3] == clustering_activities[b + 1][3]) // that means that they are the same with different names
                            {

                                //window.alert('bingoooooooo');
                                clustering_activities[b][2] = clustering_activities[b + 1][2]; // make the ending time of the first activity the same as the ending time of the next activity (combine them)
                                // now delete activity 2

                                for (var y = b + 1; y < clustering_index - 1; y++) // this is to display the saved UIDs
                                {
                                    for (var j = 0; j < 2500; j++)
                                    {
                                        clustering_activities[y][j] = clustering_activities[y + 1][j];
                                    }

                                }
                                clustering_index = clustering_index - 1;
                                b--;

                            }//end if
                        }// end main for

////////////////////////////////////////
//////////////////////////////////////////
///////////////////////////////////////////
                        if (clustering_index > 1) // this means that there are at least 2 activities to compare
                        {


                            for (var b = 0; b < clustering_index - 1; b++)
                            {

                                if (clustering_activities[b][3] == clustering_activities[b + 1][3] && clustering_activities[b][0] == clustering_activities[b + 1][0]) //chech if they are in the same day and have the same date
                                {

                                    var ending_time = clustering_activities[b][2]; // find ending time of the current activity
                                    var next_starting_time = clustering_activities[b + 1][1]; // find starting time of the next activity
                                    var difference = Time_differenc(ending_time, next_starting_time);
                                    //window.alert(difference);

                                    if (difference < shortest_desired_activity_time_gaps) // this means that we should merge them since they are the same activity with a small gap
                                    {
                                        clustering_activities[b][2] = clustering_activities[b + 1][2]; // expand the time of the first activity to cover the next one

                                        // delete the second activity
                                        for (var i = b + 1; i < clustering_index - 1; i++)
                                        {
                                            for (var j = 0; j < 2500; j++)
                                            {
                                                clustering_activities[i][j] = clustering_activities[i + 1][j];
                                            }
                                        }// end for (var i=b+1;i<clustering_index;i++)


                                        clustering_index--;
                                    }// end if (difference > time_for_new_activity)


                                }// end if (clustering_activities[b][3] == clustering_activities[b+1][3] && clustering_activities[b][0] == clustering_activities[b+1][0])


                            }// end of the main for


                        } // end of the main if








// this next loop is also to fix a bug in the algorithm... when testing, some activities were divided into 2 different activities while they suppose to be one since they have the same name... the idea is to test the ending time of the activity and compare it to the time of the next one... if the title is the same and the difference is just 1 second, we should compine hem together


//window.alert(clustering_index);

                        for (var b = 0; b < clustering_index - 1; b++) // this is to display the saved UIDs
                        {
                            var help = Add_seconds(clustering_activities[b][2], 1); // add second to the ending time of the current activity
                            if (clustering_activities[b][0] == clustering_activities[b + 1][0] && help == clustering_activities[b + 1][1] && clustering_activities[b][3] == clustering_activities[b + 1][3]) // that means that they are the same with different names
                            {

                                //window.alert('bingoooooooo');
                                clustering_activities[b][2] = clustering_activities[b + 1][2]; // make the ending time of the first activity the same as the ending time of the next activity (combine them)
                                // now delete activity 2

                                for (var y = b + 1; y < clustering_index - 1; y++) // this is to display the saved UIDs
                                {
                                    for (var j = 0; j < 2500; j++)
                                    {
                                        clustering_activities[y][j] = clustering_activities[y + 1][j];
                                    }

                                }
                                clustering_index = clustering_index - 1;
                                b--;

                            }//end if
                        }// end main for








//////////////////////////
//////////////////////////









// keep just the unreviewed activities

                        for (var b = 0; b < clustering_index - 1; b++) // this is to display the saved UIDs
                        {

                            if (clustering_activities[b][2499] == 'yes') // that means that they are the same with different names
                            {

                                for (var i = b; i < clustering_index - 1; i++)
                                {

                                    for (var j = 0; j < 2500; j++)
                                    {

                                        clustering_activities[i][j] = clustering_activities[i + 1][j];

                                    }// end second for

                                }// end first for

                                clustering_index--;
                                b--;
                            } // end main if

                        } // end of the main for


                        if (clustering_index != 0)
                        {
                            if (clustering_activities[clustering_index - 1][2499] == 'yes')
                            {

                                clustering_index--;

                            }
                        }

//pausecomp(3000);

//////////////////////////////
/////////////////////////////
////////////////////////////
/////////////////////////////
// now we want to keep just the activities of the last 3 days (no need to show activities for reviewing before that)

// the next variable will hold the date of 2 days ago regardless of the chosen date by the user
                        var two_days_ago_date = '<?php echo date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"))); ?>';
//window.alert(two_days_ago_date);

                        clustering_index_old = 0;

                        for (var b = 0; b < clustering_index; b++) // this is to display the saved UIDs
                        {

                            var activity_date = clustering_activities[b][3];

                            if (activity_date < two_days_ago_date)
                            {
                                // since this activity happen before 3 days ago, we will add it to clustering_activities_old array 					           and remove it from clustering_activities array

                                // first add it to clustering_activities_old array
                                for (var j = 0; j < 2500; j++)
                                {

                                    clustering_activities_old[clustering_index_old][j] = clustering_activities[b][j];

                                }// end second for

                                clustering_index_old++;
                                for (var i = b; i < clustering_index - 1; i++)
                                {

                                    for (var j = 0; j < 2500; j++)
                                    {

                                        clustering_activities[i][j] = clustering_activities[i + 1][j];

                                    }// end second for

                                }// end first for

                                clustering_index--;
                                b--;
                            } // end main if

                        } // end of the main for


//pausecomp(1000);


//window.alert(clustering_activities[5][0]);
//window.alert(clustering_index_old);
                        if (clustering_index < 1 && clustering_index_old > 0) // which means that there are no activities in the last 3 days and we just need to save the old activities
                        {
//window.alert('dddddddddddd');
                            var string_to_be_sent_to_server_old = replaceCommaWithTilt(clustering_activities_old);
                            window.document.detect.the_clustering_activity_old_save.value = string_to_be_sent_to_server_old;
                            window.document.detect.the_clustering_index_old_Not_save.value = clustering_index;
                            window.document.detect.the_clustering_index_old_save.value = clustering_index_old;
                            //Flag to check if there are only old activities (more than 3 days old)
                            window.document.detect.only_old.value = true;

                            //TOD: submit?
                            window.document.forms['detect'].submit();

                        }
                        else
                        if (false /*image_source == 'http://127.0.0.1/xampp/review/images/Add/reviewed.png'*/) // which means there are some activities in the last 3 days and the call is to load the activities on the main interface (we know from the passed image)
                        {
                            createImageLinks();
                        }
                        else // which means that the call is to start displaying activities (the user clicked on 1 image to start reviewing) and we need to apss old activities to save and start displaying yhe activities
                        {
                            var string_to_be_sent_to_server = replaceCommaWithTilt(clustering_activities);
                            var string_to_be_sent_to_server_old = replaceCommaWithTilt(clustering_activities_old);

//window.alert(image_source);
// this is the array with old activities then the index of it
                            window.document.detect.the_clustering_activity_old.value = string_to_be_sent_to_server_old;
                            window.document.detect.the_clustering_index_old.value = clustering_index_old;

// this is the array with the activities in the last 3 days and the index of it
                            window.document.detect.the_clustering_activity.value = string_to_be_sent_to_server;
                            window.document.detect.the_clustering_index.value = clustering_index;
                            window.document.detect.image_source.value = image_source;
                            //Flag to check if there are only old activities (more than 3 days old)
                            window.document.detect.only_old.value = false;

                            //TODO: submit?
                            window.document.forms['detect'].submit();
                        }




// combine activities when they have the same name and the difference between ending time of the first one and the strting time of the second one less than givin value

                        /*
                         for (var b=0; b<clustering_index-2; b++) // this is to display the saved UIDs
                         {

                         var ending_time_current = clustering_activities[b][2];
                         var starting_time_next = clustering_activities[b+1][1];
                         var difference = Time_differenc(ending_time_current,starting_time_next);
                         var name_current = clustering_activities[b][0];
                         var name_next = clustering_activities[b+1][0];

                         if (name_current == name_next && difference < 900) // that means that they are the same with different names
                         {

                         for (var i=b; i< clustering_index-1; i++)
                         {

                         for (var j=0; j<2500 ; j++ )
                         {

                         clustering_activities[i][j] = clustering_activities[i+1][j];

                         }// end second for

                         }// end first for

                         clustering_index--;
                         b--;
                         } // end main if

                         } // end of the main for

                         */



                        /*
                         var j = "These are all the activities after clustering and adding images with name and time";
                         j= j.bold();
                         document.writeln(j);
                         document.writeln("<br>");
                         document.writeln("<br>");


                         for (var b=0; b<clustering_index; b++) // this is to display the saved UIDs
                         {
                         for (var j=0; j<2500; j++)
                         {document.writeln(clustering_activities[b][j]);}
                         document.writeln("<br>");
                         document.writeln("<br>");
                         }// end of (var b=0; b<number_of_added_UID; b++)


                         document.writeln("<br>");
                         document.writeln("<br>");

                         */







//var array_to_pass =  replaceCommaWithTilt(clustering_activities);



// the final result is the array clustering_activities with this info:
// the number of activities is clustering_index.
// clustering_activities[clustering_index][0] is the name of the activity
// clustering_activities[clustering_index][1] is the start time of the activity
// clustering_activities[clustering_index][2] is the end time of the activity
// clustering_activities[clustering_index][3] is the date of the activity
// clustering_activities[clustering_index][4] is the place image path of the activity (might be n/a)
// clustering_activities[clustering_index][5] is the first person image path in the activity (might be n/a)
// clustering_activities[clustering_index][6] is the number of persons involved in the activity
// clustering_activities[clustering_index][7] --> clustering_activities[clustering_index][17] are the images of persons involved (they can be 12 Max)
// clustering_activities[clustering_index][18] is the number of images
// clustering_activities[clustering_index][19] --> clustering_activities[clustering_index][300] are the name of images and the time in sequence. This means that the odd indexes 19, 21, 23...299 are names (the path), and even indexes 20, 22, 24 ....300 are times.

//return clustering_index;
//window.alert(clustering_index);

//document.write(clustering_activities[0][0]);
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

// now we need to put the names with the representative image in an array to post it in the main today_reviewed
// if there are no images the image of the place will be there, if there is no place, we take the picture of the first person

                        /*
                         for (var i=0; i<clustering_index; i++)
                         {


                         names_activities[i][0]= clustering_activities[i][0]; // getting the name
                         names_activities[i][1]= clustering_activities[i][3]; // getting the date


                         var num_images = clustering_activities[i][18]; // number of images taken during the activity
                         if (num_images == '0')
                         {
                         if (clustering_activities[i][4] == 'n/a') // this means there is no place
                         {
                         names_activities[i][2]= clustering_activities[i][5]; // set the image of a person as representative
                         }// end if (clustering_activities[i][4] == 'n/a')
                         else
                         {
                         names_activities[i][2]= clustering_activities[i][4]; // set the image of the place as representative
                         }
                         }// end if (num_images == 0)

                         else
                         {
                         if (num_images == '1')
                         {
                         names_activities[i][2]= clustering_activities[i][19]; // set the only image representative

                         }
                         else
                         {
                         //var mid_image = parseInt (num_images / 2);
                         //var mid_image_index =19 + mid_image * 2;
                         //names_activities[i][2]= clustering_activities[i][mid_image_index]; // set the middle image


                         }// end else

                         }// end else



                         }// end for (var i=0; i<clustering_index; i++)
                         */
//window.alert(clustering_index);
                    }//end of the function

</script>
