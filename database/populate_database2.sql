-- Database for MemoryLane Review Client
-- Copyright (c) 2013, Lulea University of Technology (http://www.ltu.se)

--
-- Populate the database with test data
--

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

USE MLReview;

--
-- Populate the media table
--
--  mediaId int(11) NOT NULL AUTO_INCREMENT,
--  src varchar(100)  DEFAULT NULL COMMENT 'Sourcepath to the media',
--  activityId int(11) DEFAULT NULL COMMENT 'Foreign Key (Reference to activityID in Activity table). Many media can belong to one Activity',
--  mediaTime datetime DEFAULT NULL COMMENT 'When the media element is taken',
--  GPSPos point DEFAULT NULL COMMENT 'The GPS position for this media. Only store x and y position.',

INSERT INTO `Media` (`src`, `activityId`) VALUES
('/data/logsync/Logs/old/dsc00496.jpg', 1),
('/data/logsync/Logs/old/dsc00497.jpg', 2),
('/data/logsync/Logs/old/dsc00503.jpg', 3),
('/data/logsync/Logs/old/dsc00464.jpg', 4),
('/data/logsync/Logs/old/dsc00494.jpg', 4),
('/data/logsync/Logs/old/dsc00499.jpg', 4),
('/data/logsync/Logs/old/dsc00501.jpg', 4),
('/data/logsync/Logs/old/dsc00500.jpg', 4),
('/data/logsync/Logs/old/dsc00467.jpg', 5),
('/data/logsync/Logs/old/dsc00469.jpg', 5),
('/data/logsync/Logs/old/dsc00473.jpg', 5),
('/data/logsync/Logs/old/dsc00476.jpg', 5),
('/data/logsync/Logs/old/dsc00465.jpg', 5),
('/data/logsync/Logs/old/dsc00479.jpg', 5),
('/data/logsync/Logs/old/dsc00388.jpg', 6),
('/data/logsync/Logs/old/dsc00389.jpg', 6),
('/data/logsync/Logs/old/dsc00392.jpg', 6),
('/data/logsync/Logs/old/dsc00395.jpg', 6),
('/data/logsync/Logs/old/dsc00396.jpg', 6),
('/data/logsync/Logs/old/dsc00061.jpg', 6),
('/data/logsync/Logs/old/dsc00004.jpg', 7),
('/data/logsync/Logs/old/dsc00005.jpg', 7),
('/data/logsync/Logs/old/dsc00006.jpg', 7),
('/data/logsync/Logs/old/dsc00075.jpg', 7),
('/data/logsync/Logs/old/dsc00076.jpg', 7),
('/data/logsync/Logs/old/dsc00077.jpg', 7),
('/data/logsync/Logs/old/dsc00070.jpg', 7),
('/data/logsync/Logs/old/dsc00072.jpg', 7),
('/data/logsync/Logs/old/dsc00066.jpg', 7);