-- Database for MemoryLane Review Client
-- Copyright (c) 2013, Lulea University of Technology (http://www.ltu.se)

--
-- Populate the database with test data
--

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

USE MLReview;

--
-- Populate the Life Story table
--
--  storyCategoryId int(11) NOT NULL AUTO_INCREMENT,
--  name varchar(100) NOT NULL COMMENT 'The name of this life story category (child, adult, older life etc)',
--  description varchar(255) DEFAULT NULL COMMENT 'Description of this life story category',
INSERT INTO `StoryCategory` (`name`, `description`) VALUES
('Child Life', 'Age 0-15'),
('Adult Life', 'Age 16-65'),
('Older Life', 'Age 66+');

--
-- Populate the Activity table
--
--  activityId int(11) NOT NULL AUTO_INCREMENT,
--  startTime datetime DEFAULT NULL COMMENT 'Start date and/or time of this activity',
--  endTime datetime DEFAULT NULL COMMENT 'End date and/or time of this activity',
--  subject varchar(150) NOT NULL COMMENT 'Go shopping, visiting someone, etc',
--  description varchar(255) DEFAULT NULL COMMENT 'Description of the event',
--  src varchar(100) DEFAULT NULL COMMENT 'Sourcepath to the image representing this activity',
--  storyCategoryId int(11) DEFAULT NULL COMMENT 'Foreign Key (Reference to storyCategoryId in StoryCategory table)',
--  placeId int(11) DEFAULT NULL COMMENT 'Foreign Key (Reference to placeId in Place table)',
INSERT INTO `Activity` (`subject`, `startTime`, `endTime`, `description`, `src`, `storyCategoryId`, `reviewed`) VALUES
('Min bror Ragnar', null, null, '', '/data/logsync/Logs/old/dsc00496.jpg', 1, 1),
('Skolkök i Roknäs', null, null, '', '/data/logsync/Logs/old/dsc00497.jpg', 1, 1),
('Vi syskon framför hemmet i Stormyran', null, null, '', '/data/logsync/Logs/old/dsc00503.jpg', 1, 1),
('Båtutflykt till Fingermanholmen sommaren 1955', null, null, 'En härlig sommardag åkte jag, Bengt, Britta, Olle, Karin och några till med Olles båt ut till Fingermanholmen. Vi badade hela dagen, och avslutade kvällen med att halstra strömming över öppen eld. Till det åt vi bruta med tunnbröd som jag och Britta bakat dagen innan.', '/data/logsync/Logs/old/dsc00501.jpg', 2, 1),
('Semester sommaren 1954', null, null, '', '/data/logsync/Logs/old/dsc00476.jpg', 2, 1),
('Stugan påsken 2010', null, null, 'Alla vi syskon med barn var där! Härligt väder, massor med snö, lite fisk. Blött på isen.', '/data/logsync/Logs/old/dsc00395.jpg', 3, 1),
('Stugan i Kikkejaur juli 2009', null, null, 'Härligt väder men förfärligt med mygg och knott! Fick bra med fisk, stora abborrar som vi sen rökte. ', '/data/logsync/Logs/old/dsc00078.jpg', 3, 1);
--
-- Populate the Person table
--
--  personId int(11) NOT NULL AUTO_INCREMENT,
--  src varchar(100) NOT NULL COMMENT 'Sourcepath to the image representing this place',
--  firstName varchar(100) DEFAULT NULL COMMENT 'The first name of the person',
--  lastName varchar(100) DEFAULT NULL COMMENT 'The last name of the person',
--  personalInfo varchar(255) DEFAULT NULL COMMENT 'Stores some personal information about the person like the relationship to user',
--  phone varchar(40) DEFAULT NULL COMMENT 'The main phone number of the person',

INSERT INTO `Person` (`firstName`, `src`, `lastName`, `personalInfo`, `phone`) VALUES
('Stefan', '/data/uploaded/person/stefan.jpg', '', 'Stefan är en schysst kille', '070-12345, 0920-12345'),
('Basel', '/data/uploaded/person/basel_kikhia.jpg', 'Kikhia', '', ''),
('Eva', '/data/uploaded/person/eva.jpg' , 'Karlsson', '', ''),
('Johan', '/data/uploaded/person/johan_bengtsson.jpg', 'Bengtsson', '', ''),
('Andrey', '/data/uploaded/person/Andrey.jpg', '', '', ''),
('Kåre', '/data/uploaded/person/kjs_polypoly.jpg', 'Synnes', '', ''),
('George', '/data/uploaded/person/Gearge.jpg', '', '', '');

--
-- Populate the Place table
--
--  placeId int(11) NOT NULL AUTO_INCREMENT,
--  src varchar(100) DEFAULT NULL COMMENT 'Sourcepath to the image representing this place',
--  placeName varchar(100) NOT NULL COMMENT 'The name of the place (my house, medical centre, etc)',
--  GPSShape polygon DEFAULT NULL COMMENT 'The GPS coordinates that creates a surface.',
--  description varchar(255) DEFAULT NULL COMMENT 'Description added by the user',
--  address varchar(255) DEFAULT NULL COMMENT 'The address',

INSERT INTO `Place` (`placeName`, `src`, `description`, `address`) VALUES
('Luleå Centrum', '/data/uploaded/place/centrum.jpg', 'Stadens centrum', 'Storgatan 1 and beyond'),
('Ica Supermarket', '/data/uploaded/place/ica.jpg', 'Lokala affären på Porsön', 'Ingen aning'),
('Porsön', '/data/uploaded/place/porson.jpg' , '', ''),
('LTU', '/data/uploaded/place/ltu.jpg', '', ''),
('Willys', '/data/uploaded/place/willys.jpg', '', ''),
('Hertsön', '/data/uploaded/place/hertson.jpg', '', ''),
('Björkskatan', '/data/uploaded/place/Bjorkskatan.jpg', '', ''),
('Bergnäset', '/data/uploaded/place/Bergnaset.jpg', '', ''),
('Boden', '/data/uploaded/place/boden.jpg', '', ''),
('Kallax Flygplats', '/data/uploaded/place/flygplats.jpg', '', ''),
('Luleå', '/data/uploaded/place/lulea.jpg', '', ''),
('Piteå', '/data/uploaded/place/pitea.jpg', '', ''),
('Kalix', '/data/uploaded/place/kalix.jpg', '', ''),
('Storheden', '/data/uploaded/place/storheden.jpg', '', ''),
('Sunderbyn', '/data/uploaded/place/sunderbyn.jpg', '', '');




