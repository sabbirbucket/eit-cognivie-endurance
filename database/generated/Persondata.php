<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Persondata
 *
 * @ORM\Table(name="PersonData")
 * @ORM\Entity
 */
class Persondata
{
    /**
     * @var string
     *
     * @ORM\Column(name="BTName", type="string", length=50, nullable=true)
     */
    private $btName;

    /**
     * @var string
     *
     * @ORM\Column(name="BTuid", type="string", length=50, nullable=true)
     */
    private $btUid;

    /**
     * @var integer
     *
     * @ORM\Column(name="personDataId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personDataId;

    /**
     * @var \Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="personDatas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="personId", referencedColumnName="personId")
     * })
     */
    private $person;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Media", inversedBy="personDatas")
     * @ORM\JoinTable(name="PersonDataMedia",
     *   joinColumns={
     *     @ORM\JoinColumn(name="personDataId", referencedColumnName="personDataId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="mediaId", referencedColumnName="mediaId")
     *   }
     * )
     */
    private $medias;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->medias = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
}
