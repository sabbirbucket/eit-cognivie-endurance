<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Storycategory
 *
 * @ORM\Table(name="StoryCategory")
 * @ORM\Entity
 */
class Storycategory
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="storyCategoryId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $storyCategoryId;


}
