<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Activity
 *
 * @ORM\Table(name="Activity")
 * @ORM\Entity
 */
class Activity
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startTime", type="datetime", nullable=false)
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endTime", type="datetime", nullable=false)
     */
    private $endTime;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=150, nullable=true)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="activityId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $activityId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Media", mappedBy="activity")
     */
    private $medias;

    /**
     * @var \Place
     *
     * @ORM\ManyToOne(targetEntity="Place", inversedBy="activities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="placeId", referencedColumnName="placeId")
     * })
     */
    private $place;

    /**
     * @var \StoryCategory
     *
     * @ORM\ManyToOne(targetEntity="StoryCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="storyCategoryId", referencedColumnName="storyCategoryId")
     * })
     */
    private $storyCategory;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Person", inversedBy="activities")
     * @ORM\JoinTable(name="ActivityPerson",
     *   joinColumns={
     *     @ORM\JoinColumn(name="activityId", referencedColumnName="activityId")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="personId", referencedColumnName="personId")
     *   }
     * )
     */
    private $persons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->medias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->persons = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
}
