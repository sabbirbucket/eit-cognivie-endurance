<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Media
 *
 * @ORM\Table(name="Media")
 * @ORM\Entity
 */
class Media
{
    /**
     * @var string
     *
     * @ORM\Column(name="pathName", type="string", length=100, nullable=false)
     */
    private $pathName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mediaTime", type="datetime", nullable=true)
     */
    private $mediaTime;

    /**
     * @var point
     *
     * @ORM\Column(name="GPSPos", type="point", nullable=true)
     */
    private $gpsPos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activityRepresent", type="boolean", nullable=true)
     */
    private $activityRepresent;

    /**
     * @var integer
     *
     * @ORM\Column(name="mediaId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $mediaId;

    /**
     * @var \Activity
     *
     * @ORM\ManyToOne(targetEntity="Activity", inversedBy="medias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="activityId", referencedColumnName="activityId")
     * })
     */
    private $activity;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="PersonData", mappedBy="medias")
     */
    private $personDatas;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->personDatas = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
}
