-- Database for MemoryLane Review Client
-- Copyright (c) 2013, Lulea University of Technology (http://www.ltu.se)
-- Uses the engine INNODB instead of MyISAM because of built in references

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE DATABASE IF NOT EXISTS MLReview;
USE MLReview;

--
-- Table structure for table Activity
-- Moved the foreign keys last in the script
--

CREATE TABLE IF NOT EXISTS Activity (
  activityId int(11) NOT NULL AUTO_INCREMENT,
  startTime datetime DEFAULT NULL COMMENT 'Start date and/or time of this activity',
  endTime datetime DEFAULT NULL COMMENT 'End date and/or time of this activity',
  subject varchar(150) NOT NULL COMMENT 'Go shopping, visiting someone, etc',
  description varchar(255) DEFAULT NULL COMMENT 'Description of the event',
  src varchar(100) DEFAULT NULL COMMENT 'Sourcepath to the image representing this activity',
  reviewed boolean DEFAULT 1 COMMENT 'If this activity is reviewed by the user or not',
  storyCategoryId int(11) DEFAULT NULL COMMENT 'Foreign Key (Reference to storyCategoryId in StoryCategory table)',
  placeId int(11) DEFAULT NULL COMMENT 'Foreign Key (Reference to placeId in Place table)',
  PRIMARY KEY(activityId)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Table structure for table Media
--

CREATE TABLE IF NOT EXISTS Media (
  mediaId int(11) NOT NULL AUTO_INCREMENT,
  src varchar(100)  DEFAULT NULL COMMENT 'Sourcepath to the media',
  activityId int(11) DEFAULT NULL COMMENT 'Foreign Key (Reference to activityID in Activity table). Many media can belong to one Activity',
  mediaTime datetime DEFAULT NULL COMMENT 'When the media element is taken',
  GPSPos point DEFAULT NULL COMMENT 'The GPS position for this media. Only store x and y position.',
  PRIMARY KEY(mediaId),
  FOREIGN KEY(activityId) REFERENCES Activity(activityId) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Table structure for table Person
--

CREATE TABLE IF NOT EXISTS Person (
  personId int(11) NOT NULL AUTO_INCREMENT,
  src varchar(100) DEFAULT NULL COMMENT 'Sourcepath to the image representing this person',
  firstName varchar(100) NOT NULL COMMENT 'The first name of the person',
  lastName varchar(100) DEFAULT NULL COMMENT 'The last name of the person',
  personalInfo varchar(255) DEFAULT NULL COMMENT 'Stores some personal information about the person like the relationship to user',
  phone varchar(40) DEFAULT NULL COMMENT 'The phone numbers of the person',
  PRIMARY KEY(personId)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Table structure for table Place
--

CREATE TABLE IF NOT EXISTS Place (
  placeId int(11) NOT NULL AUTO_INCREMENT,
  src varchar(100) DEFAULT NULL COMMENT 'Sourcepath to the image representing this place',
  placeName varchar(100) NOT NULL COMMENT 'The name of the place (my house, medical centre, etc)',
  GPSShape polygon DEFAULT NULL COMMENT 'The GPS coordinates that creates a surface.',
  description varchar(255) DEFAULT NULL COMMENT 'Description added by the user',
  address varchar(255) DEFAULT NULL COMMENT 'The address',
  PRIMARY KEY (placeId)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Table structure for table StoryCategory
--

CREATE TABLE IF NOT EXISTS StoryCategory (
  storyCategoryId int(11) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL COMMENT 'The name of this life story category (child, adult, older life etc)',
  description varchar(255) DEFAULT NULL COMMENT 'Description of this life story category',
  PRIMARY KEY (storyCategoryId)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Table structure for table PersonData
--

CREATE TABLE IF NOT EXISTS PersonData (
  personDataId int(11) NOT NULL AUTO_INCREMENT,
  personId int(11) DEFAULT NULL COMMENT 'Foreign Key (Reference to personID in Person table). One Person can have many PersonData.',
  BTName varchar(50) DEFAULT NULL COMMENT 'Name of this bluetooth device',
  BTuid varchar(50) DEFAULT NULL COMMENT 'Uid of this bluetooth device',
  PRIMARY KEY (personDataId),
  FOREIGN KEY(personId) REFERENCES Person(personId) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Table structure for table ActivityPerson
-- This table connects two other tables
--

CREATE TABLE IF NOT EXISTS ActivityPerson (
  activityId int(11) NOT NULL,
  personId int(11) NOT NULL,
  PRIMARY KEY (activityId, personId),
  FOREIGN KEY(activityId) REFERENCES Activity(activityId) ON DELETE CASCADE,
  FOREIGN KEY(personId) REFERENCES Person(personId) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Table structure for table PersonDataMedia
-- This table connects two other tables
--

CREATE TABLE IF NOT EXISTS PersonDataMedia (
  personDataId int(11) NOT NULL,
  mediaId int(11) NOT NULL,
  PRIMARY KEY (personDataId, mediaId),
  FOREIGN KEY(personDataId) REFERENCES PersonData(personDataId) ON DELETE CASCADE,
  FOREIGN KEY(mediaId) REFERENCES Media(mediaId) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1;

-- Some mocking up
ALTER TABLE Activity
ADD FOREIGN KEY(storyCategoryId) REFERENCES StoryCategory(storyCategoryId) ON DELETE SET NULL,
ADD FOREIGN KEY(placeId) REFERENCES Place(placeId) ON DELETE SET NULL
