-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2013 at 02:05 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mlreview`
--
CREATE DATABASE IF NOT EXISTS `mlreview` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mlreview`;


-- --------------------------------------------------------

--
-- Table structure for table `oauth`
--

CREATE TABLE IF NOT EXISTS `oauth` (
  `callbackUrl` varchar(50) NOT NULL,
  `siteUrl` varchar(50) NOT NULL,
  `consumerKey` varchar(50) NOT NULL,
  `consumerSecret` varchar(50) NOT NULL,
  `oauth_token` varchar(50) NOT NULL,
  `tokenSecret` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Keeps the OAuth configuration';

--
-- Dumping data for table `oauth`
--

INSERT INTO `oauth` (`callbackUrl`, `siteUrl`, `consumerKey`, `consumerSecret`, `oauth_token`, `tokenSecret`) VALUES
('http://mlreview.dev/uploadxmldata/callback', 'https://healthlabs.ehv.campus.philips.com/oauth', 'b0827beb5686c89869cebe5ac16e4366', '5d5aff748e72d6d1e917fc922b04473a', '538d677f626fa403924fa760ac6c7753c4aedede', '36f353330341f167e761e90b2bda5651a8459540');

-- --------------------------------------------------------

