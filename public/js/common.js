/*
 * This file is part of the Memory Lane Review Client
 *
 * Copyright (c) 2013, Lulea University of Technology  (http://www.ltu.se)
 */

//Global javascript variables
var infoWindowWidth = screen.availWidth * 0.70;
var infoWindowHeight = screen.availHeight * 0.99;

//Open an information window to the user
function open_info_window(url, title) {
    var iw = window.open(url, title, 'width='
            + infoWindowWidth + ',height=' + infoWindowHeight
            + ',left=20,top=20,linemenubar=yes,scrollbars=yes,resizable=yes' +
            ',modal=yes,location=yes,status=yes,toolbar=yes');
    iw.focus();

}

//Special submit, different purpose in different forms
//Only difference from normal submit is the specialFlag
function special_submit(thisForm) {
    thisForm.elements["specialFlag"].value = 'true';
    thisForm.submit();
}

//Close the current window and refreshes the parent (the calling window)
//Used when closing pop-ups
function refresh_parent() {
    //window.opener.location.href = window.opener.location.href;
    //true=force get from server
    window.opener.location.reload(true);

    if (window.opener.progressWindow) {
        window.opener.progressWindow.close();
    }
    window.close();
}

//Help function to remove item from id array
//array must be on the form array[i][0] === id
function remove_item(array, id) {
    for (var i in array) {
        if (array[i][0] == id) {
            array.splice(i, 1);
            break;
        }
    }
    return array;
}

//Help function see if item exists in array
//array must be on the form array[i][0] === id
function has_item(array, id) {
    for (var i in array) {
        if (array[i][0] == id) {
            return true;
        }
    }
    return false;
}

//Help function to check for empyy values in javascript
//Should work as the PHP "empty"
function empty(variable) {
    var emptyValues = [undef, null, false, 0, '', '0'];
    var undef, i, j, len;

    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (variable === emptyValues[i]) {
            return true;
        }
    }

    if (typeof variable === "object") {
        for (j in variable) {
            return false;
        }
        return true;
    }
    return false;
}


