// JavaScript Document
function startDate(language) {

    var d = new Date();
    var weekdayname = new Array(7);
    var monthname = new Array();
    if (language === "sv_SE") {
        weekdayname[0] = "Söndag";
        weekdayname[1] = "Måndag";
        weekdayname[2] = "Tisdag";
        weekdayname[3] = "Onsdag";
        weekdayname[4] = "Torsdag";
        weekdayname[5] = "Fredag";
        weekdayname[6] = "Lördag";
        monthname[0] = "Januari";
        monthname[1] = "Februari";
        monthname[2] = "Mars";
        monthname[3] = "April";
        monthname[4] = "Maj";
        monthname[5] = "Juni";
        monthname[6] = "Juli";
        monthname[7] = "Augusti";
        monthname[8] = "September";
        monthname[9] = "Oktober";
        monthname[10] = "November";
        monthname[11] = "December";
    } else {
        //default
        weekdayname[0] = "Sunday";
        weekdayname[1] = "Monday";
        weekdayname[2] = "Tuesday";
        weekdayname[3] = "Wednesday";
        weekdayname[4] = "Thursday";
        weekdayname[5] = "Friday";
        weekdayname[6] = "Saturday";
        monthname[0] = "January";
        monthname[1] = "February";
        monthname[2] = "March";
        monthname[3] = "April";
        monthname[4] = "May";
        monthname[5] = "June";
        monthname[6] = "July";
        monthname[7] = "August";
        monthname[8] = "September";
        monthname[9] = "October";
        monthname[10] = "November";
        monthname[11] = "December";
    }

    var weekday = weekdayname[d.getDay()];
    var month = monthname[d.getMonth()];
    $(".weekday").text(weekday);
    $(".month").text(month);
    $(".datenumber").text(d.getDate());
}
