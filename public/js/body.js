// JavaScript Document

/* The page is divided into Header, Content, and Footer parts.
 *
 * This script will make sure that the Content part takes up the entire height - Header size - Footer size
 */


function resize() {
	 var header = $('#header').height();
	 var footer = $('#footer').height();
	 var docHeight = $(window).height();
 
	 var remainingHeight = docHeight - header - footer;
	 $('#content').height(remainingHeight);

	 $('#contentspace').outerHeight(remainingHeight, true);	
}


$(document).ready(function() {
    resize();
});

// for the window resize
$(window).resize(function() {
   resize();
});

function showBackButton(show) {
	if(show) {
		$('#backButton').show();
	} else {
		$('#backButton').hide();
	}
}

